<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $cliente = $data["cliente"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($cliente->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Perfil
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li class="active"><?php echo ($cliente->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Perfil</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/clientes/saveperfil" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>
                <div class="col-lg-4">
                    <label id="l_rlegal">Representante Legal</label>
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" id="nombre" name="nombre" value="<?php echo $cliente->nombre ?>" class="form-control">
                    </div>
                </div>

                <div class="col-lg-4">
                    <label id="l_empresa">Empresa</label>
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-suitcase"></i></span>
                        <input type="text" id="empresa" name="empresa" value="<?php echo $cliente->empresa ?>" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_nit">Nit / Identificaci&oacute;n</label>
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-sort-numeric-desc"></i></span>
                        <input type="text" id="nit" name="nit" value="<?php echo $cliente->nit ?>" class="form-control">
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_direccion">Direcci&oacute;n</label>
                    <div class="input-group margin-bottom-30">
                        <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
                        <input type="text" id="direccion" name="direccion" value="<?php echo $cliente->direccion ?>" class="form-control">
                    </div>
                </div>

                <div class="col-lg-4">
                    <label id="l_tfijo">Telefono Fijo</label>
                    <div class="input-group margin-bottom-30">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input type="text" id="tfijo" name="tfijo" value="<?php echo $cliente->tfijo ?>" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_tcelular">Telefono Celular</label>
                    <div class="input-group margin-bottom-30">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input type="text" id="tcelular" name="tcelular" value="<?php echo $cliente->tcelular ?>" class="form-control">
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_pweb">Pagina Web</label>
                    <div class="input-group margin-bottom-30">
                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                        <input type="text" id="pweb" name="pweb" value="<?php echo $cliente->pweb ?>" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_email">Email</label>
                    <div class="input-group margin-bottom-30">
                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                        <input type="text" id="email" name="email" value="<?php echo $cliente->email ?>" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_ciudad">Ciudad</label>
                    <div class="input-group margin-bottom-30">
                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                        <input type="text" id="ciudad" name="ciudad" value="<?php echo $cliente->ciudad ?>" class="form-control">
                    </div>
                </div>
                <?php
//$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
//$cad = "";
//for($i=0;$i<8;$i++) {
//$cad .= substr($str,rand(0,62),1);
//}
//print $cad;
//print "<br/>";
//print md5($cad);
//
                ?>
                <div class="clearfix"></div><br/>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input id="estado" name="estado" type="hidden" value="1" />
                    <input id="id" name="id" type="hidden" value="<?php echo $cliente->id ?>" />
                </div>
        </div>
    </form>
</div>
<div class="modal fade" id="ModalInput" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-sm vertical-align-center" role="document">
            <div class="modal-content">
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <div id="textError">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_rlegal').html(), true + "\n");
        sErrMsg += validateText($('#empresa').val(), $('#l_empresa').html(), true + "\n");
        sErrMsg += validateText($('#nit').val(), $('#l_nit').html(), true + "\n");
        sErrMsg += validateText($('#ciudad').val(), $('#l_ciudad').html(), true + "\n");
        sErrMsg += validateText($('#tfijo').val(), $('#l_tfijo').html(), true + "\n");
        sErrMsg += validateEmail($('#email').val(), $('#l_email').html(), true + "\n");
        if (sErrMsg !== "")
        {
            $('#ModalInput').modal('show');
            $('#textError').html(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        
        $.post('<?php echo $data['rootUrl']; ?>admin/actualizarperfil/validar', {
            nit: $('#nit').val(),
            //nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('El Cliente ' + $('#nombre').val() + ' ya se encuentra registrado ..');
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>panel/home';
    });

</script>
