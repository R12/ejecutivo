<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $banner = $data["banner"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($banner->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> Banners
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/banners">Banner</a></li>
        <li class="active"><?php echo ($banner->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de la ciudad </li>
    </ol>
</section>
<br/>
<div class="box ">
    <form id="form1" class="form" action="<?= $patch; ?>admin/banners/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>

                <div class="col-lg-4">
                    <label id="l_nombre">Nombre</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $banner->nombre ?>" id="nombre" name="nombre" maxlength="60">
                    </div>
                </div>

                <div class="col-lg-4">
                    <label id="l_video">Imagen</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file" id="imagen" name="imagen" class="input-file"/>
                    </div>
                </div>
                <div class="clearfix"></div><br/>

                <div class="col-lg-4">
                    <label id="l_descripcion">Descripci&oacute;n</label><br/>
                    Utiliza la etiqueta<i> <code><span><</span>br<span>/></span></code> </i>Para saltos de lineas
                    <div class="input-group">
                        <textarea name="descripcion" id="descripcion" maxlength="800" cols="50"><?php echo $banner->descripcion ?></textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $banner->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        //sErrMsg += validateText($('#descripcion').val(), $('#l_descripcion').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/banners/validar', {
            nombre: $("#nombre").val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('El Banner ' + $('#nombre').val() + ' ya se encuentra registrada ..')
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/banners';
    });

</script>
