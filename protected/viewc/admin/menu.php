<?php
    $login = $_SESSION['login'];
?>
<aside class="main-sidebar">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?= $patch ?>global/img/admin/user.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?= $login->nombre ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <?= $login->menu; ?>
        </section>
</aside>
