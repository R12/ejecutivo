<header class="main-header">
    <!-- Logo -->
    <a href="<?= $patch ?>panel/home" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>C</b>EJ</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>CC. EJECUTIVOS</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo $data["rootUrl"] ?>panel/home"> 
                        <i class="fa fa-columns"></i>
                        Panel Administrador
                    </a>
                </li>
                <li>
                    <a target="_blank" href="<?php echo $data["rootUrl"] ?>"> 
                        <i class="fa fa-chrome"></i>
                        Ir a la pagina Web
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= $login->nombre ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $patch ?>global/img/admin/user.png" class="img-circle" alt="User Image">
                            <p><?= $login->nombre ?></p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="<?= $patch ?>panel/logout" class="btn btn-default btn-flat">Salir</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>