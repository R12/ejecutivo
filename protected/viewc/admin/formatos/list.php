<div id="form" style="width:700px;">

    <?php if (isset($_REQUEST['msg'])) { ?>
        <div class="error" style="margin-top: 10px;"><?php echo $_REQUEST['msg'] ?></div>
    <?php } ?>

    <form name="form1" method="post" action="<?php echo $data['rootUrl']; ?>admin/formatos"  class="form" id="form1">
        <h4 class="titleform" style="margin-bottom:0px;">Definici&oacute;n de Formatos</h4>
        <div  id="toolbar">
            <div class="toolbar-list">
                <ul>
                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>admin/formatos/add" id="btn-add" class="link-button">
                            <span class="icon-new" title="Nuevo" >&nbsp;</span>
                            Nuevo
                        </a>
                    </li>

                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>admin/formatos/edit" id="btn-edit" class="link-button" >
                            <span class="icon-edit" title="Editar" >&nbsp;</span>
                            Editar
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>panel/home" id="btn-back" class="link-button">
                            <span class="icon-back" title="Regresar" >&nbsp;</span>
                            Regresar
                        </a>
                    </li>

                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <hr />

        <div id="filter-bar">

            <label style="width:70px" class="filter-by">Filtrar por</label>
            <select name="filtro" id="filtro" class="select">
                <option value="informe" <?php echo $data["filtro"] == 'informe' ? 'selected' : '' ?>>Descripci&oacute;n</option>
                <option value="codigo" <?php echo $data["filtro"] == 'codigo' ? 'selected' : '' ?>>Codigo</option>
            </select>

            <span class="search">
                <input name="texto" type="text" size="30" maxlength="30" class="input-search" id="texto" value="<?php echo $data["texto"] ?>"/>
                <input type="button" class="search-btn" id="btn-find" onclick="submitbutton('index');" />
            </span>
            <input name="mod" type="hidden" id="mod" value="clientes" />
        </div>
        <div id="datagrid">
            <table class="grid" cellspacing="1" width="100%">
                <thead>
                    <tr>
                        <th width="5%">&nbsp;</th>
                        <th width="30%">Descripci&oacute;n</th>
                        <th width="15%">Codigo</th>
                        <th width="15%">Emisi&oacute;n</th>
                        <th width="15%">Versi&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($data["formatos"])>0){
                    $i = 0;
                    foreach ($data["formatos"] as $r):
                        ?>
                        <tr class="row<?php echo $i ?>">
                            <td><input name="item" type="radio" value="<?php echo $r['id']; ?>" /></td>
                            <td><?php echo $r['informe']; ?></td>
                            <td><?php echo $r['codigo'];?></td>
                            <td><?php echo $r['emision']; ?></td>
                            <td><?php echo $r['version']; ?></td>
                        </tr>
                        <?php
                        $i = 1 - $i;
                    endforeach;}else{
                    ?>
                        <tr><td colspan="6">No hay Registros</td></tr>
                        <?php }?>
                </tbody>
            </table>
            <?php if (count($data["formatos"]) > 0) { ?>
                <div id="pagination">
                    <?php echo $data['pager'] ?>
                </div>
            <?php } ?>

        </div>
    </form>
</div>
<script type="text/javascript">

    $('texto').keypress(function(e){
        if (e.keyCode==13)
            $('#form1').submit();
    })

    $('#btn-edit').click(function(e){
        item = $('input[name=item]:checked').attr('value');
        if (!item){
            alert('Debe seleccionar un item');
            e.preventDefault();
        }
        else {
            var action = $(this).attr("href") + "/" + item;
            $(this).attr("href",action);
        }
    });

    $('#btn-find').click(function(){
        $('#form1').submit();
    });

    $('#btn-delete').click(function(e){
        item = $('input[name=item]:checked').attr('value');
        if (!item){
            alert('Debe seleccionar un item');
            e.preventDefault();
        }
        else {
            var action = $(this).attr("href") + "/" + item;
            $(this).attr("href",action);
        }
    });
</script>