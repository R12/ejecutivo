<?php $formato = $data["formato"]; ?>
<div id="form" style="width:700px;">
    <form id="form1" class="form" action="<?php echo $data['rootUrl']; ?>admin/formatos/save" method="post" name="form1">
        <h4 class="titleform">Informaci&oacute;n del Formato</h4>

        <div class="inputem" style="width:20%;float: left;">
            <label style="width:50%" class="required" id="l_codigo">Codigo</label>
            <input style="width: 45%" value="<? echo $formato->codigo ?>" id="codigo" name="codigo" maxlength="20" />
        </div>
        <div class="inputem" style="width:20%;float: left;">
            <label style="width:50%" class="required" id="l_version">Versi&oacute;n</label>
            <input style="width: 45%" value="<? echo $formato->version ?>" id="version" name="version" maxlength="2" />
        </div>
<div class="inputem" style="width:55%;float: left;">
<label style="width:25%" class="required" id="l_emision">Fecha Emisi&oacute;n</label>
<input style="width:30%" value="<? echo $formato->emision ?>" id="emision" name="emision" maxlength="20" />
<span style="font-size: 8px;">(YY-MM-DD)</span>
<img src="<?php echo $data['rootUrl']; ?>global/img/admin/calendar.png" alt="Seleccione fecha" name="calendar1" id="calendar1" width="16" height="16" align="absmiddle" class="datepicker" />
</div>
        <div class="clear"></div>
        <div class="inputem" style="width: 98%">
            <label style="width:10%" class="required" id="l_informe">Informe</label>
            <input style="width:63.5%" value="<? echo $formato->informe ?>" id="informe" name="informe" maxlength="40" />
        </div>
        <div class="inputem" style="width: 100%">
            <label style="width:10%" id="l_nota">Nota</label>
            <textarea id="nota" name="nota" style="width: 85%;height: 100px;" ><? echo $formato->nota ?></textarea>
        </div>
        <div class="button-bar">
            <button class="button right" type="button" id="btn-cancel"><span class="icon-cancel16">Cancelar</span></button>
            <button class="button right" type="button" id="btn-save"><span class="icon-save16">Guardar</span></button>
            <input name="id" type="hidden" id="id" value="<? echo $formato->id; ?>" />
        </div>
    </form>

</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<link href="<?php echo $data['rootUrl'] ?>global/js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/js/calendar/calendar.js"></script>
<script type="text/javascript">
    function validateForm(){

        var sErrMsg = "";
        var flag = true;

        sErrMsg += validateText($('#codigo').val(), $('#l_codigo').html(), true);
        sErrMsg += validateText($('#informe').val(), $('#l_informe').html(), true);
        sErrMsg += validateDate($('#emision').val(), $('#l_emision').html(), true);
        sErrMsg += validateNumber($('#version').val(), $('#l_version').html(), true);

        if(sErrMsg != "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }


      Calendar.setup({
        inputField	 : "emision",
        button		 : "calendar1",
        ifFormat	 : "%Y-%m-%d",
        singleClick	 : true
    });
    function validar(){
        $.post( '<?php echo $data['rootUrl']; ?>admin/formatos/validar', {
            informe: $('#informe').val(),
            id:$("#id").val()
        },
        function( data ) {
            if (data){
                alert('El Formato ya se encuentra registrado ..')
            } else {
                $('#form1').submit();
            }
        }
    );
    }
    $('#btn-save').click(function(){
        if (validateForm()){
            validar();
        }
    })

    $('#btn-cancel').click(function(){
        window.location = '<?php echo $data['rootUrl']; ?>admin/formatos';
    })

</script>