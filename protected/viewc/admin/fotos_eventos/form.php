<?php $ft = $data["fotoevento"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($ft->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Fotos &nbsp;
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/fotoevento">Fotos Eventos</a></li>
        <li class="active"><?php echo ($ft->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?>Fotos Eventos</li>
    </ol>
</section>
<br/>
<div class="box">

    <form id="form1" class="form" action="<?php echo $data['rootUrl']; ?>admin/fotoevento/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend style="text-align: center;"><strong>Informaci&oacute;n Basica <i class="fa fa-pencil"></i></strong></legend>

                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_nombre">Nombre(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $ft->nombre ?>" id="nombre" name="nombre" maxlength="100" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_evento">Evento (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-caret-square-o-down"></i>
                        </div>
                        <select class="form-control select2"  id="id_evento" name="id_evento">
                            <?php foreach ($data["evento"] as $ctg) { ?>
                                <option <?php echo ($ft->id_evento == $ctg->id ? 'selected="selected"' : ''); ?> value="<?php echo $ctg->id; ?>"><?php echo $ctg->nombre; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_foto">Foto(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file"  id="imagen" name="imagen" />
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-8">
                    <label>Descripci&oacute;n</label>
                    <div class="input-group">
                        <textarea cols="50"  id="descripcion" name="descripcion" ><?php echo $ft->descripcion ?></textarea>
                    </div>
                </div>
            </fieldset>
            <div class="clearfix"></div><br/>
            <div class="box-footer col-lg-2 pull-right">
                <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                <input name="id" type="hidden" id="id" value="<?php echo $ft->id; ?>" />
                <input name="nom_img" type="hidden" id="nom_img" value="<?php echo $ft->foto; ?>" />
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        if ($("#id").val() === "") {
            sErrMsg += validateText($('#imagen').val(), $('#l_imagen').html(), true);
        }
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }
        return flag;
    }
    
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/fotoevento/validar', {
            nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('La Foto' + $('#nombre').val() + ' ya se encuentra registrado ..');
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }

    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });

    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/fotoevento';
    });

</script>
