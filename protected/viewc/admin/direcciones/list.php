<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<section class="content-header">
    <h1>
        Definici&oacute;n de Categoria Tiendas
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>panel/home">Inicio</a></li>
        <li class="active">Definici&oacute;n de Categoria Tiendas</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <form name="form1" method="post" action="<?php echo $data['rootUrl']; ?>admin/direcciones"  class="form" id="form1">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-body">
                        <div class="mailbox-controls" style="float:right;">
                            <!-- Check all button -->
                            <div class="btn-group">
                                <a href="<?= $patch; ?>admin/direcciones/add" id="btn-add" class="btn btn-default btn-md"><i class="fa fa-plus-circle"></i><br/><span>Nuevo</span></a>
                                <a href="javascript:void(0);" id="btn-edit" class="btn btn-default btn-md"><i class="fa fa-edit"></i><br/><span>Editar</span></a>
                                <a href="javascript:void(0);" id="btn-delete" class="btn btn-default btn-md"><i class="fa fa-minus-circle"></i><br/><span>Eliminar</spa></a>
                            </div><!-- /.btn-group -->
                        </div>
                        <div class="clearfix"></div>
                        <table id="tabledatas" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Id</th>
                                    <th>Etiqueta</th>
                                    <th>URL</th>
                                    <th>Men&uacute;</th>
                                    <th>Pie de P&aacute;g.</th>
                                    <th>Columna Izq.</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($data['banners'] as $f) {
                                    ?>
                                    <tr>
                                        <td><input class="minimal" name="item" type="radio" value="<?php echo $f->id; ?>" /></td>
                                        <td><?php echo $f->id; ?></td>
                                        <td><?php echo $f->etiqueta; ?></td>
                                        <td><?php echo $f->url; ?></td>
                                        <td><?php echo $f->istop; ?></td>
                                        <td><?php echo $f->isfotter; ?></td>
                                        <td><?php echo $f->iscolunm; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Id</th>
                                    <th>Etiqueta</th>
                                    <th>URL</th>
                                    <th>Men&uacute;</th>
                                    <th>Pie de P&aacute;g.</th>
                                    <th>Columna Izq.</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </form>
    </div>
</section>
<script src="<?= $patch ?>global/admin/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?= $patch ?>global/admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    });

    $(function () {
        $("#tabledatas").DataTable();
    });

    $('#texto').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#form1').submit();
        }
    });

    $('#btn-edit').click(function () {
        if (getItemCheked() == -1) {
            alert('Debe seleccionar un item');
        } else {
            var action = $('#form1').attr("action") + "/edit";
            $('#form1').attr("action", action);
            $('#form1').submit();
        }
    });

    $('#btn-delete').click(function () {
        if (getItemCheked() == -1) {
            alert('Debe seleccionar un item');
        } else {
            if (confirm("Esta seguro de eliminar este registro ..")) {
                var action = $('#form1').attr("action") + "/delete";
                $('#form1').attr("action", action);
                $('#form1').submit();
            }
        }
    });

</script>