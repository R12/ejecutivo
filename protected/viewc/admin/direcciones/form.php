<?php $dirc = $data['direccion']; ?>
<section class="content-header">
    <h1>
        <?php echo ($dirc->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> Direcci&oacute;n
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/eventos">Eventos</a></li>
        <li class="active"><?php echo ($dirc->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Direcci&oacute;n</li>
    </ol>
</section>
<br/>
<div class="box">
   <form id="form1" class="form" action="<?php echo $data['rootUrl']; ?>admin/direcciones/save" method="post" name="form1" enctype="multipart/form-data">
       <div class="box-body">
           <fieldset style="width:97%;">
               <legend style="text-align: center;"><strong>Informaci&oacute;n Basica <i class="fa fa-pencil"></i></strong></legend>
               
               <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_nombre">Etiqueta(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="etiqueta" id="etiqueta" value="<?php echo $dirc->etiqueta; ?>" maxlength="200"/>
                    </div>
                </div>
               <div class="col-lg-4">
                    <label id="l_nombre">URL(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="url" id="url" value="<?php echo $dirc->url; ?>" maxlength="200"/>
                    </div>
                </div>
               <div class="col-lg-4">
                    <label id="l_nombre">Altura(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="alto" id="alto" value="<?php echo $dirc->alto; ?>" maxlength="200"/>
                    </div>
                </div>
               <div class="clearfix"></div><br/>
               <div class="col-lg-3">
                    <label id="l_orden">Orden(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="ordercolumn2" id="ordercolumn2" value="<?php echo $dirc->ordercolumn2; ?>" />
                    </div>
                </div>
               <div class="col-lg-2">
                    <label id="l_nombre">Tiene Submenus?(*)</label>
                    <div class="input-group">
                        
                        <input type="checkbox" name="submenu" id="submenu"  value="S" <?php echo ($dirc->submenu == 'S' ? 'checked="checked"' : '') ?>/>
                    </div>
                </div>
               
               <div class="col-lg-2">
                    <label id="l_orden">Opci&oacute;n del Men&uacute;</label>
                    <div class="input-group">
                        
                        <input type="checkbox" name="istop" id="istop" value="S" <?php echo ($dirc->istop == 'S' ? 'checked="checked"' : '') ?> />
                    </div>
                </div>
               <div class="col-lg-2">
                    <label id="l_orden">Link de Pie de P&aacute;gina</label>
                    <div class="input-group">
                        
                        <input  type="checkbox" name="isfotter" id="isfotter" value="S" <?php echo ($dirc->isfotter == 'S' ? 'checked="checked"' : '') ?> />
                    </div>
                </div>
               <div class="col-lg-3">
                    <label id="l_orden">Opci&oacute;n de la Columna Izq.</label>
                    <div class="input-group">
                        
                        <input  type="checkbox" name="iscolunm" id="iscolunm" value="S" <?php echo ($dirc->iscolunm == 'S' ? 'checked="checked"' : '') ?> />
                    </div>
                </div>
               
               <div class="clearfix"></div><br/>
             
               <div class="col-lg-4">
                    <label id="l_categoria">Ra&iacute;z de la Opci&oacute;n</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-code"></i>
                        </div>
                        <select class="form-control select2"  name="dependencia" id="dependencia" >
                            <option value="0" <?php echo ($dirc->dependencia == '0' ? 'selected="selected"' : ''); ?> >Sin Ra&iacute;z</option>
                            <?php foreach ($data['opciones'] as $ops) { ?>
                                <option value="<?php echo $ops['id']; ?>" <?php echo ($dirc->dependencia == $ops['id'] ? 'selected="selected"' : ''); ?> ><?php echo $ops['etiqueta']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
               
               <div class="col-lg-4">
                   <label id="l_categoria">Posici&oacute;n en Pie de P&aacute;gina</label>
                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa-code"></i>
                       </div>
                       <select class="form-control select2" name="posicionf" id="posicionf" >
                           <option value="0" <?php echo ($dirc->posicionf == '0' ? 'selected="selected"' : ''); ?> >[Seleccione...]</option>
                           <option value="1" <?php echo ($dirc->posicionf == '1' ? 'selected="selected"' : ''); ?> >Primera Columna</option>
                           <option value="2" <?php echo ($dirc->posicionf == '2' ? 'selected="selected"' : ''); ?> >Segunda Columna</option>
                           <option value="3" <?php echo ($dirc->posicionf == '3' ? 'selected="selected"' : ''); ?> >Tercera Columna</option>
                       </select>
                   </div>
               </div>
      
               <div class="col-lg-4">
                   <label id="l_categoria">Posici&oacute;n en la Columna Izquierda</label>
                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa-code"></i>
                       </div>
                       <select class="form-control select2" name="posicionf" id="posicionf" >
                           <option value="0" <?php echo ($dirc->posicionf == '0' ? 'selected="selected"' : ''); ?> >[Seleccione...]</option>
                           <option value="1" <?php echo ($dirc->posicionf == '1' ? 'selected="selected"' : ''); ?> >Primera Columna</option>
                           <option value="2" <?php echo ($dirc->posicionf == '2' ? 'selected="selected"' : ''); ?> >Segunda Columna</option>
                           <option value="3" <?php echo ($dirc->posicionf == '3' ? 'selected="selected"' : ''); ?> >Tercera Columna</option>
                       </select>
                   </div>
               </div>
               
                        <div class="clearfix"></div><br/>
              
               <div class="col-lg-4">
                    <label id="l_orden">Orden2</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input class="form-control pull-right" type="text" name="orderfotter" id="orderfotter" value="<?php echo $dirc->orderfotter; ?>" />
                    </div>
                </div>
               
               <div class="col-lg-4">
                    <label id="l_orden">Orden 3</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input class="form-control pull-right" type="text" name="ordercolumn" id="ordercolumn" value="<?php echo $dirc->ordercolumn; ?>" />
                    </div>
                </div>
               
               <div class="col-lg-4">
                   <label id="l_categoria">Destino del la Direcci&oacute;n</label>
                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa-code"></i>
                       </div>
                       <select class="form-control select2" name="destino" id="destino" >
                           <option value="L" <?php echo ($dirc->destino == 'L' ? 'selected="selected"' : ''); ?> >Local</option>
                           <option value="E" <?php echo ($dirc->destino == 'E' ? 'selected="selected"' : ''); ?> >Externo</option>
                       </select>
                   </div>
               </div>
               <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $dirc->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
               
               
           </fieldset>
       </div>
   </form>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">

    function validateForm(){

        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#etiqueta').val(), "Etiqueta", true);
        sErrMsg += validateText($('#url').val(), "URL", true);
        sErrMsg += validateNumber($('#ordercolumn2').val(),'Orden',true);

        sErrMsg += validateNumber($('#orderfotter').val(),'Orden 2',true);
        sErrMsg += validateNumber($('#ordercolumn').val(),'Orden 3',true);
        sErrMsg += validateNumber($('#alto').val(),'Alto',true);
        if(sErrMsg != "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }

    $('#btn-save').click(function(){
        if (validateForm()){
            if($("#typeaction").val()=='I'){
                validar();
            }else{
                $('#form1').submit();
            }
        }
    })

    function validar(){
        $.post( '<?php echo $data['rootUrl']; ?>admin/direcciones/validar', {
            id:$("#id").val()
        },
        function( data ) {
            if (data){
                alert('El Id '+$("#id").val()+' ya Existe')
            } else {
                $('#form1').submit();
            }
        }
    );
    }

    $('#btn-cancel').click(function(){
        window.location = '<?php echo $data['rootUrl']; ?>admin/direcciones';
    })

</script>
