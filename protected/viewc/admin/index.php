<?php
$patch = $data['rootUrl'];
$login = $_SESSION['login'];
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>CC LOS EJECUTIVOS</title>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="author" content="Web and net">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.4 -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/css/bootstrap.min.css"/>
        <!-- FontAwesome 4.3.0 -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/font-awesome/css/font-awesome.min.css"/>
        <!-- Ionicons 2.0.0 -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/css/ionicons.min.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/css/AdminLTE.css"/>

        <!--<link rel="stylesheet" href="<?//= $patch ?>global/admin/skins/skin-blue.min.css"/>-->
        <link href="<?= $patch ?>global/admin/skins/skin-red.min.css" rel="stylesheet" type="text/css"/>

        <!-- iCheck -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/plugins/iCheck/minimal/blue.css"/>

        <!-- Date Picker -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/plugins/datepicker/datepicker3.css"/>
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/plugins/daterangepicker/daterangepicker-bs3.css"/>



        <link rel="stylesheet" href="<?= $patch ?>global/admin/plugins/select2/select2.min.css"/>

         <!-- Impletacion CSS para el calendario con hora -->


        <!-- Implementacion CSS de las mask (Mascara) -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/js/jquery-loadmask-0.4/jquery.loadmask.css"/>

        <!-- Growl -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/plugins/jquery.growl/css/jquery.growl.css">
        <!-- CSS Personalizados -->

        <!-- jQuery 2.1.4 -->
        <script type='text/javascript'src="<?= $patch ?>global/admin/js/jquery-2.1.4.min.js"></script>

        <!-- Impletacion JS para el calendario con hora -->

        <!-- Implementacion JS de las mask (Mascara) -->
        <script type='text/javascript' src='<?= $patch ?>global/admin/js/jquery-loadmask-0.4/jquery.loadmask.min.js'></script>

        <script type='text/javascript' src='<?= $patch ?>global/admin/plugins/jquery-steps-master/build/jquery.steps.js'></script>
         <link rel="stylesheet" href="<?= $patch ?>global/admin/plugins/jquery-steps-master/demo/css/jquery.steps.css">
         <link rel="stylesheet" href="<?= $patch ?>global/admin/css/personalizado.css"/>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="skin-red sidebar-mini">
      <div class="wrapper">
          <!-- incluyendo la pagina de Header, ubicada en la cabecera -->
          <?php include 'header.php'; ?>
          <!-- Incluyendo La pagina de Menu -->
          <?php include 'menu.php' ?>
          <!-- Contenedor de la pagina -->
          <div class="content-wrapper">
              <section id="section-content" class="content">
                  <?php include $data['content']; ?>
              </section>
          </div><!-- /.Cierre de todo el contenedor de la pagina -->

            <!-- incluyendo la pagina de footer -->
            <?php include 'footer.php' ?>
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->

    </body>

<!-- Bootstrap 3.3.2 JS -->
<script src="<?= $patch ?>global/admin/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?= $patch ?>global/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script type="text/javascript" src="<?= $patch ?>global/admin/plugins/select2/select2.full.min.js"></script>
<script>
    $(function () {
        $(".select2").select2();
        $(".timepicker").timepicker({
          showInputs: false
        });
    });
</script>

<!-- Bootstrap WYSIHTML5 -->
<script type="text/javascript" src="<?= $patch ?>global/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- FastClick -->
<script type="text/javascript" src="<?= $patch ?>global/admin/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= $patch ?>global/admin/js/app.min.js" type="text/javascript"></script>
<link rel="stylesheet" media="all" href="<?= $patch ?>global/admin/plugins/bootstrap-datetimepicker.min.css" />
<!-- Growl -->
<script type="text/javascript" src="<?= $patch ?>global/admin/plugins/jquery.growl/js/jquery.growl.js"></script>
</html>
