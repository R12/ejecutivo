<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $ctgetienda = $data["ctgetienda"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($ctgetienda->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Categoria Tienda
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/ctgtienda">Categoria Tienda</a></li>
        <li class="active"><?php echo ($ctgetienda->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Categoria Tienda</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/ctgtienda/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>

                <div class="col-lg-4">
                    <label id="l_nombre">Nombre</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $ctgetienda->nombre ?>" id="nombre" name="nombre" maxlength="20">
                    </div>
                </div>
                <div class="col-lg-8">
                    <label>Descripci&oacute;n</label>
                    <div class="input-group">
                        <textarea style="width: 50%;" cols="70" id="descripcion" name="descripcion" ><?php echo $ctgetienda->descripcion ?></textarea>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $ctgetienda->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        //sErrMsg += validateText($('#descripcion').val(), $('#l_descripcion').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/ctgtienda/validar', {
            nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('La Categoria ' + $('#nombre').val() + ' ya se encuentra registrada ..')
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/ctgtienda';
    });

</script>
