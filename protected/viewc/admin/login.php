<!DOCTYPE html>
<?php
$patch = $data['rootUrl'];
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CC LOS EJECUTIVOS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/css/bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= $patch ?>global/admin/skins/skin-blue.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>CC LOS EJECUTIVOS</b>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><?php echo (isset($data['title']) ? 'Inicio de Sesi&oacute;n' : 'Panel Administrativo'); ?></p>
                <?php if (isset($data['error'])) {
                    ?>
                    <div class="external-event bg-red ui-draggable ui-draggable-handle error"><?php echo $data['error'] ?></div>
                <?php } ?>
                <form  name="form1" action="<?php echo $data['rootUrl'] . (isset($data['title']) ? 'clientes' : 'admin'); ?>/login" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="usuario" name="email" id="email" maxlength="30" autofocus="autofocus">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input name="password" id="password" type="password" class="form-control" placeholder="Contrase&ntilde;a">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                        </div><!-- /.col -->
                    </div>
                </form>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="<?= $patch ?>global/admin/js/jquery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?= $patch ?>global/admin/js/bootstrap.min.js"></script>
    </body>
</html>
