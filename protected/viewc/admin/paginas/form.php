<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $page = $data['page']; ?>
<section class="content-header">
    <h1>
        <?php echo ($page->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Paginas
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/paginas">Paginas</a></li>
        <li class="active"><?php echo ($page->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Paginas</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/paginas/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>

                <div class="col-lg-4">
                    <label id="l_page">Url</label>
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" id="page" name="page" value="<?php echo $page->page; ?>" class="form-control">
                    </div>
                </div>

                <div class="col-lg-4">
                    <label id="l_titulo">Titulo</label>
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-suitcase"></i></span>
                        <input type="text" id="titulo" name="titulo" value="<?php echo $page->titulo ?>" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary <?php echo ($page->align=="left" ? 'active' : ''); ?>">
                            <input type="radio" name="align" value="L" id="option1"><i class="glyphicon glyphicon-align-left"></i>
                        </label>
                        <label class="btn btn-primary <?php echo ($page->align=="center" ? 'active' : ''); ?>">
                            <input type="radio" name="align" value="C" id="option2"><i class="glyphicon glyphicon-align-center"></i>
                        </label>
                        <label class="btn btn-primary <?php echo ($page->align=="right" ? 'active' : ''); ?>">
                            <input type="radio" name="align" value="R" id="option3"><i class="glyphicon glyphicon-align-right"></i>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_foto">Foto Principal (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file"  id="imagen" name="imagen" />
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                
                <div class="col-lg-4">
                    <label id="l_background">Foto Background (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file"  id="imagen2" name="imagen2" />
                    </div>
                </div>
                
                <div class="clearfix"></div><br/>

                <div class="col-lg-12">
                    <label id="l_descripcion">Contenido</label>
                    <textarea name="descripcion" cols="60" rows="5" style="width: 100%;height:350px;" id="descripcion" ><?php echo $page->descripcion; ?></textarea>
                </div>
                <div class="clearfix"></div><br/>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input id="estado" name="estado" type="hidden" value="1" />
                    <input id="id" name="id" type="hidden" value="<?php echo $page->id ?>" />
                </div>
            </fieldset>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/editor.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#page').val(), $('#l_page').html(), true);
        sErrMsg += validateText($('#titulo').val(), $('#l_titulo').html(), true);
//        sErrMsg += validateText($('#descripcion').val(), $('#l_descripcion').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }

    initEditor("descripcion");

    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/paginas/validar', {
            nombre: $('#page').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('La Pagina ' + $('#page').val() + ' ya se encuentra registrado ..');
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }

    $('#btn-save').click(function () {
        if (validateForm()) {
            $('#form1').submit();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/paginas';
    });

</script>
