<div id="form" style="width:600px;">
    
    <form id="form1" class="form" action="<?php echo $data['rootUrl']; ?>admin/parametros/save" method="post" name="form1">
        <h4 class="titleform">Parametros</h4>
        
         <div class="inputem">
                <label style="width:150px">Correo electr&oacute;nico</label>
                <input name="email" type="text"  id="email" size="60" maxlength="100" value="<?php echo $data['email'] ?>"/>
                <?php if (isset($data["errors"]["email"])) { ?>
                <div class="error-msg" style="margin-left: 150px"><?php echo utf8_decode($data["errors"]["email"]); ?></div>
                <?php } ?>
         </div>
              
        <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
                                  
        <div class="button-bar">
            <button class="button right" type="button" id="btn-cancel"><span class="icon-cancel16">Cancelar</span></button>
            <button class="button right" type="submit" id="btn-save"><span class="icon-save16">Guardar</span></button>
        </div>
            
    </form>
</div>
    
<script type="text/javascript">
            
    $('#btn-cancel').click(function(){
        window.location = '<?php echo $data['rootUrl']; ?>panel/home';
    })
    
</script>

