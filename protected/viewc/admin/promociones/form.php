<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $data['rootUrl']; ?>global/admin/js/jquery-ui/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<link href="<?php echo $data['rootUrl']; ?>global/admin/js/jquery-ui/css/start/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css"/>
<?php $t = $data["promocion"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($t->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> Promociones
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/promocion">Promociones</a></li>
        <li class="active"><?php echo ($t->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Promociones</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/promociones/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>

                <div class="col-lg-4">
                    <label id="l_nombre">Nombre</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $t->nombre ?>" id="nombre" name="nombre" maxlength="30">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_fecha_inicio">Fecha Inicio(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $t->fecha_inicio ?>" id="fecha_inicio" name="fecha_inicio" maxlength="100" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_fecha_fin">Fecha Fin(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $t->fecha_fin ?>" id="fecha_fin" name="fecha_fin" maxlength="100" />
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_categoria">Tiendas</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-users"></i>
                        </div>
                        <select class="form-control select2"  id="id_tienda" name="id_tienda">
                            <option value="0" <?php echo ($t->id_tienda == '0' ? 'selected="selected"' : ''); ?> >Sin Ra&iacute;z</option>
                            <?php foreach ($data['tienda'] as $ops) { ?>
                                <option value="<?php echo $ops['id']; ?>" <?php echo ($t->id_tienda == $ops['id'] ? 'selected="selected"' : ''); ?> ><?php echo $ops['nombre']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_foto">Foto</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file" id="imagen" name="imagen" />
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-8">
                    <label>Descripci&oacute;n</label>
                    <div class="input-group">
                        <textarea cols="52" id="descripcion" name="descripcion" maxlength="200" ><?php echo $t->descripcion ?></textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $t->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<!--<script type="text/javascript" src="<?//php echo $data['rootUrl'] ?>global/admin/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?//php echo $data['rootUrl'] ?>global/admin/js/editor.js"></script>-->
<script type="text/javascript">
//    initEditor("descripcion");
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        //sErrMsg += validateUrl($('#link').val(), $('#l_link').html(), true);
        //sErrMsg += validateText($('#descripcion').val(), $('#l_descripcion').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }

    $(function () {
        $("#fecha_inicio").datepicker({
                    dateFormat: 'yy/mm/dd',
                    minDate: 0 
                });
                
        $("#fecha_fin").datepicker({
            dateFormat: 'yy/mm/dd',
            minDate: 0
        });
    });
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/promociones/validar', {
            nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('La Promocion ' + $('#nombre').val() + ' ya se encuentra registrada ..');
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/promociones';
    });

</script>
