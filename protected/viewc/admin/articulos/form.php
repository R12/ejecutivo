<link rel="stylesheet" type="text/css" href="<?php echo $data['rootUrl']; ?>global/css/ui-lightness/jquery-ui-1.8.18.custom.css" />
<?php
  $articulo = $data['articulo'];
?>
<div id="form" style="width:586px;">
    
    <form id="form1" class="form" action="<?php echo $data['rootUrl']; ?>admin/articulos/save" method="post" name="form1" enctype="multipart/form-data">
        <h4 class="titleform">Informaci&oacute;n del Banners</h4>
          
              <div class="inputem">
                <label style="width:150px">Tipo</label>
                <select name="categoria">
                    <?php foreach ($data['categories'] as $k => $v): ?>
                        <option value="<?php echo $k; ?>" <?php echo ($articulo->categoria == $k?'selected':'') ?>><?php echo $v; ?></option>
                    <?php endforeach; ?>
                    </select>    
                </div>    
    
               <div class="inputem">    
                    <label style="width:150px">Titulo</label>    
                    <input name="titulo" type="text"  id="titulo" size="60"  value="<?php echo $articulo->titulo; ?>"/>
                    <?php if (isset($data["errors"]["titulo"])) { ?>
                       <div class="error-msg" style="margin-left: 150px"><?php echo ($data["errors"]["titulo"]); ?></div>
                    <?php } ?>
                </div>
                <div class="inputem">
                   <label style="width:150px">Introducci&oacute;n</label>
                   <textarea name="descripcion" rows="4" cols="45"><? echo $articulo->descripcion; ?></textarea>
                    <?php if (isset($data["errors"]["descripcion"])) { ?>
                       <div class="error-msg" style="margin-left: 150px"><?php echo ($data["errors"]["descripcion"]); ?></div>
                    <?php } ?>
               </div> 
        
           <div class="inputem">
             <label style="width:150px" class="text-label">Imagen</label>
             <input type="file" name="file"  id="file"/>
           </div>
                    
          <div class="button-bar">
            <button class="button right" type="button" id="btn-cancel"><span class="icon-cancel16">Cancelar</span></button>
            <button class="button right" type="submit" id="btn-save"><span class="icon-save16">Guardar</span></button>
            <input type="hidden" name="id"   value="<?php echo $articulo->id; ?>" />
         </div>
            
    </form>
</div>
    
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/editor.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/js/jquery.ui.datepicker-es.js"></script>
    
<script type="text/javascript">
    
    
    $('#btn-cancel').click(function(){
        window.location = '<?php echo $data['rootUrl']; ?>admin/articulos';
    })
    
</script>

