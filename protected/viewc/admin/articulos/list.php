<div id="form" style="width:570px;">
    <form name="form1" method="post" action="<?php echo $data['rootUrl']; ?>admin/articulos"  class="form" id="form1">
        <h4 class="titleform" style="margin-bottom:0px;">Banners</h4>

        <div  id="toolbar">

            <div class="toolbar-list">
                <ul>
                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>admin/articulos/add" id="btn-add" class="link-button">
                            <span class="icon-new" title="Nuevo" >&nbsp;</span>
                            Nuevo
                        </a>
                    </li>
                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>admin/articulos/edit" id="btn-edit" class="link-button">
                            <span class="icon-edit" title="Editar" >&nbsp;</span>
                            Editar
                        </a>
                    </li>
                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>admin/articulos/delete" id="btn-delete" class="link-button">
                            <span class="icon-delete" title="Eliminar" >&nbsp;</span>
                            Eliminar
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>

                    <li class="btn-toolbar">
                        <a href="<?php echo $data['rootUrl']; ?>panel/home" id="btn-back" class="link-button">
                            <span class="icon-back" title="Regresar" >&nbsp;</span>
                            Regresar
                        </a>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <hr />

        <div id="filter-bar">
            <label style="width:70px" class="filter-by">Filtrar por</label>
            <select name="filtro" class="select">
                <option value="titulo" <?php echo ($this->filtro == 'titulo' ? 'selected' : ''); ?>>Titulo</option>
                <option value="fecha"  <?php echo ($this->filtro == 'fecha' ? 'selected' : ''); ?>>Fecha</option>
            </select>
            <span class="search">
                <input name="texto" type="text" size="30" maxlength="30" class="input-search" id="texto" value="<?php echo $data['texto'] ?>" />
                <input type="button" class="search-btn" id="btn-find" onclick="form1.submit();" />
            </span>
        </div>

        <div id="datagrid">

            <table class="grid" cellspacing="1" width="100%">
                <thead>
                    <tr>
                        <th width="20">&nbsp;</th>
                        <th width="20">#</th>
                        <th width="300">Titulo</th>
                        
                        <th width="100">Imagen</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 0;
                    $articulos = $data["articulos"];
                     if (count($articulos) > 0) {
                    foreach ($articulos as $a):
                        ?>
                        <tr class="row<?php echo $i ?>">
                            <td><input name="item" type="radio" value="<?php echo $a['id']; ?>" /></td>
                            <td><?php echo $a['id']; ?></td>
                            <td><?php echo $a['titulo']; ?></td>
                            <td style="text-align: center;"><a href="<?php echo Doo::conf()->RELATIVE_IMG_DIR.$a['imagen'] ?>" class="link-image"><img style="border: 0;" src="<?php echo $data['rootUrl']; ?>global/img/admin/iPhoto.png" alt="Ver Imagen"/></a></td>
                        </tr>
                        <?php
                        $i = 1 - $i;
                      endforeach;
                     } else {
                     ?>
                      <tr>
                        <td colspan="4">No hay registros que mostrar</td>
                      </tr>
                     <?php } ?>   
                </tbody>
            </table>
            <div id="pagination">
                <?php echo $data['pager'] ?>
            </div>
         </div>  
    </form>
</div>
<script type="text/javascript">
    $('#btn-edit').click(function(e){
        n = $('input[name=item]:checked').attr('value');
        if (!n){
           alert('Debe seleccionar un item');
           e.preventDefault();
        }else {
            var action = $(this).attr("href") + "/?item=" + n;
            $(this).attr("href",action);
        }
    });

    $('#btn-delete').click(function(e){
        n = $('input[name=item]:checked').attr('value');
        if (!n){
          alert('Debe seleccionar un item');
          e.preventDefault();
        }else {
            if (confirm("Esta seguro de eliminar este registro ..")){
               var action = $(this).attr("href") + "/?item=" + n;
               $(this).attr("href",action);
            }
        }
    });

    $('#texto').keypress(function(e){
        if (e.keyCode == 13){
            form1.submit();
        }
    });

</script>
