<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $l = $data["locales"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($l->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Locales
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/locales">Locales</a></li>
        <li class="active"><?php echo ($l->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Locales</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/locales/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>
                <div class="col-lg-4">
                    <label id="l_nombre">Nombre (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $l->nombre ?>" id="nombre" name="nombre" maxlength="20">
                    </div>
                </div> 
                <div class="col-lg-4">
                    <label id="l_piso">Piso (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-hashtag"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $l->piso ?>" id="piso" name="piso" maxlength="3">
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_piso">Descripci&oacute;n</label>
                    <textarea class="form-control" name="descripcion" id="descripcion"><?php echo $l->descripcion; ?></textarea>
                </div>
                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $l->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
            </fieldset>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        sErrMsg += validateNumber($('#piso').val(), $('#l_piso').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/locales/validar', {
            nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('El Local ' + $('#nombre').val() + ' ya se encuentra registrada ..');
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/locales';
    });

</script>
