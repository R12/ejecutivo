<section class="content-header">
    <h1>
        Importar Locales
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/locales">Locales</a></li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/locales/upload" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <div class="col-lg-4">
                <label id="l_archivo">Archivo (*)</label>
                <div class="input-group">
                    <input type="file" name="file" class="pull-right">
                </div>
            </div> 
            <div class="clearfix"></div>
            <div class="box-footer col-lg-2 pull-right">
                <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    $('#btn-save').click(function () {
        $('#form1').submit();
    })
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/locales';
    })
</script>
