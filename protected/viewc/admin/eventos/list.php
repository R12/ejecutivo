<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<section class="content-header">
    <h1>
        Definici&oacute;n de Eventos
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>panel/home">Inicio</a></li>
        <li class="active">Definici&oacute;n de Eventos</li>
    </ol>
</section>
<?php 
$login = $_SESSION['login'];
$tipo = $login->tipo;
?>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-body">
                    <div class="mailbox-controls" style="float:right;">
                        <!-- Check all button -->
                        <div class="btn-group">
                            <a href="<?= $patch; ?>admin/eventos/add" id="btn-add" class="btn btn-default btn-md"><i class="fa fa-plus-circle"></i><br/><span>Nuevo</span></a>
                            <a href="<?= $patch; ?>admin/eventos/edit" id="btn-localidad"class="btn btn-default btn-md"><i class="fa fa-edit"></i><br/><span>Editar</span></a>
                            <a href="<?= $patch; ?>admin/eventos/delete" id="btn-delete" class="btn btn-default btn-md"><i class="fa fa-minus-circle"></i><br/><span>Eliminar</spa></a>
                        </div><!-- /.btn-group -->
                    </div>
                    <div class="clearfix"></div>
                    <table id="tabledatas" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Nombre del evento </th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($data["evento"] as $r) {
                                ?>
                                <tr>
                                    <td><input class="minimal" name="item" type="radio" value="<?php echo $r["id"]; ?>" /></td>
                                    <td><?php echo $r["nombre"]; ?></td>
                                    <td><?php echo $r["fecha"]; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>&nbsp;</th>
                              <th>Nombre del evento</th>
                              <th>Fecha</th>
                            </tr>
                        </tfoot>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
          </div>
        </section>
        <script src="<?= $patch ?>global/admin/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= $patch ?>global/admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
            });

            $(function () {
                $("#tabledatas").DataTable({
                "language": {
//                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "No se encontraron resultados"
//                "info": "Showing page _PAGE_ of _PAGES_",
//                "infoEmpty": "No records available",
//                "infoFiltered": "(filtered from _MAX_ total records)"
                }
                });
            });

            $('#btn-edit').click(function (e) {
                item = $('input[name=item]:checked').attr('value');
                if (!item) {
                    alert('Debe seleccionar un item');
                    e.preventDefault();
                }
                else {
                    var action = $(this).attr("href") + "/" + item;
                    $(this).attr("href", action);
                }
            });
            
            $('#btn-localidad').click(function (e) {
                item = $('input[name=item]:checked').attr('value');
                if (!item) {
                    alert('Debe seleccionar un item');
                    e.preventDefault();
                }
                else {
                    var action = $(this).attr("href") + "/" + item;
                    $(this).attr("href", action);
                }
            });

            $('#btn-delete').click(function (e) {
                item = $('input[name=item]:checked').attr('value');
                if (!item) {
                    alert('Debe seleccionar un item');
                    e.preventDefault();
                }
                else {
                    var action = $(this).attr("href") + "/" + item;
                    $(this).attr("href", action);
                }
            });
            
            $('#btn-activate').click(function (e) {
                item = $('input[name=item]:checked').attr('value');
                if (!item) {
                    alert('Debe seleccionar un item');
                    e.preventDefault();
                }
                else {
                    var action = $(this).attr("href") + "/" + item;
                    $(this).attr("href", action);
                }
            });
            $('#btn-borrador').click(function (e) {
                item = $('input[name=item]:checked').attr('value');
                if (!item) {
                    alert('Debe seleccionar un item');
                    e.preventDefault();
                }
                else {
                     window.open('<?php echo $data["rootUrl"] ?>infoproducto?id='+item, "Imprimir", null);
                    //$(this).attr("href", action);
                }
            });
            $('#btn-find').click(function () {
                $('#form1').submit();
            });
        </script>
