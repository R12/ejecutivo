<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $t = $data["tienda"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($t->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> Tiendas
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/tiendas">Tiendas</a></li>
        <li class="active"><?php echo ($t->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Tiendas</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/tiendas/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>
                <div class="col-lg-4">
                    <label id="l_nombre">Nombre (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $t->nombre ?>" id="nombre" name="nombre" maxlength="30">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_link">Web</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-link"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $t->link ?>" id="link" name="link" maxlength="50">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_orden">Orden (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-hashtag"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $t->orden ?>" id="orden" name="orden" maxlength="3">
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_local">Local (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-users"></i>
                        </div>
                        <select class="form-control select2"  id="id_local" name="id_local">
                            <option value="" <?php echo ($t->id_local == '0' ? 'selected="selected"' : ''); ?> >Sin Ra&iacute;z</option>
                            <?php foreach ($data['locales'] as $l) { ?>
                                <option value="<?php echo $l['id']; ?>" <?php echo ($t->id_local == $l['id'] ? 'selected="selected"' : ''); ?> ><?php echo $l['nombre']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_foto">Foto</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file" id="imagen" name="imagen" />
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-6">
                    <div class="col-lg-6">
                        <label id="l_categoria">Categoria (*)</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-users"></i>
                            </div>
                            <select class="form-control select2"  id="id_categoria" name="id_categoria">
                                <option value="" <?php echo ($t->id_categoria == '0' ? 'selected="selected"' : ''); ?> >Sin Ra&iacute;z</option>
                                <?php foreach ($data['categoria'] as $ops) { ?>
                                    <option value="<?php echo $ops['id']; ?>" <?php echo ($t->id_categoria == $ops['id'] ? 'selected="selected"' : ''); ?> ><?php echo $ops['nombre']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label>&nbsp;</label>
                        <div class="input-group">
                            <button class="btn btn-primary" type="button" id="btncategoria">Agregar</button>
                        </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-8">
                        <label>Descripci&oacute;n</label>
                        <div class="input-group">
                            <textarea style="width: 50%;" id="descripcion" name="descripcion" ><?php echo $t->descripcion ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div id="msj" style="color: #D35400;"></div>

                    <table id="tabledatas" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 70%;">Categorias De la tienda</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="itemscategoria">

                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $t->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/editor.js"></script>
<script type="text/javascript">
    initEditor("descripcion");
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        sErrMsg += validateNumber($('#orden').val(), $('#l_orden').html(), true);
        sErrMsg += ($('#id_local').val() === "" ? '- Debe seleccionar un Local.\n' : '');
        sErrMsg += ($('#id_categoria').val() === "" ? '- Debe seleccionar una Categoria.\n' : '');
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/tiendas/validar', {
            nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('La Tienda ' + $('#nombre').val() + ' ya se encuentra registrada ..')
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }


    function cargar_items(e) {
        $.post('<?php echo $data['rootUrl']; ?>admin/tiendas/cargar_categorias',
                {
                },
                function (data) {
                    $('#itemscategoria').html(data);
                });
    }

    $('#items').ready(function () {
        cargar_items();
    });
    function delItem(i) {
        $.post('<?php echo $data['rootUrl']; ?>admin/tiendas/deleteitems', {index: i}, function (data) {
            $('#itemscategoria').html(data);
        });
    }

    function validatecategoria() {
        $.post('<?php echo $data['rootUrl']; ?>admin/tiendas/validarCategoria', {
            id_categoria: $('#id_categoria').val()
        },
                function (data) {
                    if (data === "NO") {
                        alert('Ya esta registrado');
                    } else {
                        insertarcategorias();
                    }
                }
        );
    }

    function insertarcategorias() {
        var nombre_ctg = $('#id_categoria option:selected').text();

        $.post('<?php echo $data['rootUrl']; ?>admin/tiendas/agregar_categoria',
                {
                    id_categoria: $('#id_categoria').val(),
                    nombre_categoria: nombre_ctg

                },
                function (data) {

                    $('#itemscategoria').html(data);
                }
        ), "json";
    }

    // *** Agregar una Categoria ***
    $('#btncategoria').click(function () {
        if (validatecategoria()) {
        }
    });

    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/tiendas';
    });

</script>
