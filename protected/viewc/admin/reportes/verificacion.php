<html>
    <head>
        <title>Validacion de Boleta</title>
    </head>    
    <body>
        <h3>Validaci&oacute;n Boleta:&nbsp;&nbsp;<span style="font-style: italic"><?php echo $data["evento"] ?></span></h3>
        <table>
            <tr>
                <td><strong>Evento : </strong></td>
                <td><?php echo $data["evento"] ?></td>
            </tr>
            <tr>
                <td><strong>Asistente : </strong></td>
                <td><?php echo $data["asistente"] ?></td>
            </tr>
            <tr>
                <td><strong>Cantidad Boleta : </strong></td>
                <td><?php echo $data["cantidad"] ?></td>
            </tr>
            <tr>
                <td><strong>Dia reserva : </strong></td>
                <td><?php echo $data["dia_reserva"] ?></td>
            </tr>
            <tr>
                <td><strong>Hora : </strong></td>
                <td><?php echo $data["hora"] ?></td>
            </tr>
        </table>
    </body>
</html>
