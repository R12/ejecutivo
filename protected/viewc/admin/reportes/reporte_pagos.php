<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<section class="content-header">
    <h1>
        Reportes de reservas de eventos pagos
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>panel/home">Inicio</a></li>
        <li class="active">Definici&oacute;n de eventos pagos</li>
    </ol>
</section>
<?php
$login = $_SESSION['login'];
$tipo = $login->tipo;
?>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-body">
                    <div class="mailbox-controls" style="float:right;">
                        <!-- Check all button -->
                        <div class="btn-group">

                        </div><!-- /.btn-group -->
                    </div>
                    <div class="clearfix"></div>
                    <table id="tabledatas" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nombre del evento </th>
                                <th>Responsable </th>
                                <th>Fecha</th>
                                <th>Ciudad</th>
                                <th>Evento Pago ?</th>
                                <th>Estado</th>
                                <th>Estadistica</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($data["reserva"] as $r) {
                                ?>
                                <tr>

                                    <td><?php echo $r["nombre"]; ?></td>
                                    <td><?php echo $r["cliente"]; ?></td>
                                    <td><?php echo $r["finicial"]; ?></td>
                                    <td><?php echo $r["ciudad"]; ?></td>
                                    <?php
                                    $styl = "";
                                    switch ($r["pago"]) {
                                        case "S":
                                            $styl = "label-warning";
                                            $txt = "PAGO";
                                            break;
                                        case "N":
                                            $styl = "label-default";
                                            $txt = "GRATIS";
                                            break;
                                        default:
                                            $txt = "";
                                            break;
                                    }
                                    echo '<td><span class="label ' . $styl . '">' . $txt . '</span></td>';
                                    ?>
                                    <?php
                                    switch ($r["estado"]) {
                                        case 1:
                                            $styl = "label-warning";
                                            $txt = "BOORADOR";
                                            break;
                                        case 2:
                                            $styl = "label-primary";
                                            $txt = "PENDIENTE";
                                            break;
                                        case 3:
                                            $styl = "label-success";
                                            $txt = "APROBADO";
                                            break;
                                        default:
                                            $txt = "";
                                            break;
                                    }
                                    echo '<td><span class="label ' . $styl . '">' . $txt . '</span></td>';
                                    ?>
                                    <td style="text-align: center;">
                                        <div class="btn-group">
                                            <a href="<?php echo $r["link_reporte"] ?>" target="_blank"  class="btn btn-primary btn-md" ><i class="fa fa-bar-chart"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre del evento</th>
                                <th>Responsable </th>
                                <th>Fecha</th>
                                <th>Ciudad</th>
                                <th>Evento Pago ?</th>
                                <th>Estado</th>
                                <th>Estadistica</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
<script src="<?= $patch ?>global/admin/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?= $patch ?>global/admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    });

    $(function () {
        $("#tabledatas").DataTable({
            "language": {
//                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "No se encontraron resultados"
//                "info": "Showing page _PAGE_ of _PAGES_",
//                "infoEmpty": "No records available",
//                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
    });

    $('#btn-imprimir').click(function (e) {
        item = $('input[name=item]:checked').attr('value');
        if (!item) {
            alert('Debe seleccionar un item');
            e.preventDefault();
        } else {
            window.open('<?php echo $data["rootUrl"] ?>admin/reporte_reservas/imprimir/' + item, "Imprimir", null);
            //$(this).attr("href", action);
        }
    });


    $('#btn-find').click(function () {
        $('#form1').submit();
    });
</script>
