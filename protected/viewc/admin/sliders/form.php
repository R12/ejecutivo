<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $slider = $data["slider"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($slider->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> del Sliders
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/sliders">Sliders Publicitario</a></li>
        <li class="active"><?php echo ($slider->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> del Sliders</li>
    </ol>
</section>
<br/>
<div class="box ">
    <form id="form1" class="form" action="<?= $patch; ?>admin/sliders/save" method="post" name="form1">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>

                <div class="col-lg-4">
                    <label id="l_titulo">Titulo</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $slider->titulo ?>" id="titulo" name="titulo" maxlength="30">
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_descripcion">Descripci&oacute;n</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <textarea cols="80"  name="descripcion" id="descripcion"><?php echo $slider->descripcion ?></textarea>
                        <!--<input type="text" class="form-control pull-right" value="" id="titulo" name="titulo" maxlength="30">-->
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $slider->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="0" />
                </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#titulo').val(), $('#l_titulo').html(), true);
        //sErrMsg += validateText($('#descripcion').val(), $('#l_descripcion').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/sliders/validar', {
            titulo: $('#titulo').val(),
            id: $("#id").val()
        },
        function (data) {
            if (data) {
                alert('La Ciudad ' + $('#nombre').val() + ' ya se encuentra registrada ..')
            } else {
                $('#form1').submit();
            }
        }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/ciudades';
    });

</script>
