<?php $a = $data["documento"]; ?>
<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<section class="content-header">
    <h1>
        <?= ($a->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Documento
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/documentos">Documento</a></li>
        <li class="active"><?= ($a->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Documento</li>
    </ol>
</section>
<br/>
<div class="box ">
    <form id="form1" class="form" action="<?= $patch; ?>admin/documentos/save" method="post" name="form1" enctype="multipart/form-data">
      <div class="box-body">
        <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>
                <div class="col-lg-4">
                    <label id="l_nombre">Nombre:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?= $a->nombre; ?>" id="nombre" name="nombre" maxlength="100">
                    </div><!-- /.input group -->
                </div>
                <div class="col-lg-4">
                    <label id="l_documento">Documento:</label>
                    <div class="input-group">

                        <input type="file" class="pull-right"  id="documento" name="documento" maxlength="10">
                    </div><!-- /.input group -->
                </div>

              <div class="clearfix"></div>
               <div class="box-footer col-lg-2 pull-right">
                   <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                   <button type="button" id="btn-save" class="btn  bg-green pull-right">Guardar</button>
                   <input name="id" type="hidden" id="id" value="<?= $a->id; ?>" />
                   <input name="nombredoc" type="hidden" id="nombredoc" value="<? echo $a->direccion; ?>" />
                   <input id="estado" name="estado" type="hidden" value="1" />
               </div>
           </fieldset>
           </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm(){
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(),$('#l_nombre').html() , true);
        if($('#id').val()==""){
            sErrMsg += validateText($('#documento').val(),$('#l_documento').html() , true);
        }
        if(sErrMsg != "")
        {
            alert(sErrMsg);
            flag = false;
        }
        return flag;
    }
    function validar(){
        $.post( '<?php echo $data['rootUrl']; ?>admin/documentos/validar', {
            codigo: $('#nombre').val(),
            id:$("#id").val()
        },
        function( data ) {
            if (data){
                alert('El Documento ' + $('#nombre').val() + ' ya se encuentra registrado ..')
            } else {
                $('#form1').submit();
            }
        }
    );
    }

    $('#btn-save').click(function(){
        if (validateForm()){
            validar();
        }
    })

    $('#btn-cancel').click(function(){
        window.location = '<?php echo $data['rootUrl']; ?>admin/documentos';
    })

</script>
