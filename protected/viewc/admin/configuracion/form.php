<link href="<?= $patch ?>global/admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $c = $data["configuracion"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($c->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Configuraci&oacute;n
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/configuracion">Configuraci&oacute;n</a></li>
        <li class="active"><?php echo ($c->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Configuraci&oacute;n</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/configuracion/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend>Informaci&oacute;n General</legend>

                <div class="col-lg-4">
                    <label id="l_nombre">Nombre Empresa (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $c->nombre_empresa ?>" id="nombre_empresa" name="nombre_empresa" maxlength="50">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_telefono">Tel&eacute;fono Empresa (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $c->telefono_empresa ?>" id="telefono_empresa" name="telefono_empresa" maxlength="60">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_email">Email Empresa (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $c->email_empresa ?>" id="email_empresa" name="email_empresa" maxlength="60">
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_direccion">Direcci&oacute;n Empresa(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $c->direccion_empresa ?>" id="direccion_empresa" name="direccion_empresa" maxlength="60">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_ciudad">Ciudad (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $c->ciudad_empresa ?>" id="ciudad_empresa" name="ciudad_empresa" maxlength="30">
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_mapa">Mapa Google (*)</label>
                    <div class="input-group">
                        <textarea type="text" cols="50" class="form-control" id="mapa" name="mapa"><?php echo $c->mapa ?></textarea>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box-footer col-lg-2 pull-right">
                    <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                    <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                    <input name="id" type="hidden" id="id" value="<?php echo $c->id; ?>" />
                    <input id="estado" name="estado" type="hidden" value="1" />
                </div>
            </fieldset>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre_empresa').val(), $('#l_nombre').html(), true);
        sErrMsg += validateText($('#telefono_empresa').val(), $('#l_telefono').html(), true);
        sErrMsg += validateEmail($('#email_empresa').val(), $('#l_email').html(), true);
        sErrMsg += validateText($('#direccion_empresa').val(), $('#l_direccion').html(), true);
        sErrMsg += validateText($('#ciudad_empresa').val(), $('#l_ciudad').html(), true);
        sErrMsg += validateText($('#mapa').val(), $('#l_mapa').html(), true);
        //sErrMsg += validateText($('#descripcion').val(), $('#l_descripcion').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }

        return flag;

    }
    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/configuracion/validar', {
            nombre: $('#nombre_empresa').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('La Configuracion ' + $('#nombre').val() + ' ya se encuentra registrada ..')
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    }
    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/configuracion';
    });

</script>
