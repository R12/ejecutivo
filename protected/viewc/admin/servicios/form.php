<?php $s = $data["servicio"]; ?>
<section class="content-header">
    <h1>
        <?php echo ($s->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Servicios
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $patch ?>">Inicio</a></li>
        <li><a href="<?= $patch ?>admin/servicios">Servicios</a></li>
        <li class="active"><?php echo ($s->id == "" ? 'Registro' : 'Actualizaci&oacute;n'); ?> de Eventos</li>
    </ol>
</section>
<br/>
<div class="box">
    <form id="form1" class="form" action="<?= $patch; ?>admin/servicios/save" method="post" name="form1" enctype="multipart/form-data">
        <div class="box-body">
            <fieldset style="width:97%;">
                <legend style="text-align: center;"><strong>Informaci&oacute;n Basica <i class="fa fa-pencil"></i></strong></legend>

                <div class="clearfix"></div><br/>
                <div class="col-lg-4">
                    <label id="l_url">URl(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $s->url ?>" id="url" name="url" maxlength="100" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="l_nombre">Nombre(*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-text-width"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="<?php echo $s->nombre ?>" id="nombre" name="nombre" maxlength="100" />
                    </div>
                </div>
                
                <div class="col-lg-4">
                    <label id="l_foto">Foto Principal (*)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-photo"></i>
                        </div>
                        <input type="file" class="pull-right"  id="imagen" name="imagen" />
                    </div>
                </div>
                <div class="clearfix"></div><br/>
                <div class="col-lg-8">
                    <label>Descripci&oacute;n</label>
                    <div class="input-group">
                        <textarea style="width: 50%;" id="descripcion" name="descripcion" ><?php echo $s->descripcion ?></textarea>
                    </div>
                </div>
            </fieldset>
            <div class="clearfix"></div><br/>

            <div class="clearfix"></div>
            <div class="box-footer col-lg-2 pull-right">
                <button type="button" id="btn-cancel" class="btn bg-grey btn-default">Cancelar</button>
                <button type="button" id="btn-save" class="btn  bg-blue pull-right">Guardar</button>
                <input name="id" type="hidden" id="id" value="<?php echo $s->id; ?>" />
                <input id="estado" name="estado" type="hidden" value="<?php echo $s->estado; ?>" />
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/admin/js/editor.js"></script>
<script type="text/javascript">
    initEditor("descripcion");

    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);

        if ($("#id").val() === "") {
            //sErrMsg += validateText($('#imagen').val(), $('#l_imagenG').html(), true);
            sErrMsg += validateText($('#imagen').val(), $('#l_foto').html(), true);
            //sErrMsg += validateText($('#imagen3').val(), $('#l_imagenP').html(), true);
        }
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
//            $('#ModalInput').modal('show');
//            $('#textError').html(sErrMsg);
            flag = false;
        }
        return flag;
    }


    function validar() {
        $.post('<?php echo $data['rootUrl']; ?>admin/servicios/validar', {
            nombre: $('#nombre').val(),
            id: $("#id").val()
        },
                function (data) {
                    if (data) {
                        alert('EL Servicio ' + $('#nombre').val() + ' ya se encuentra registrado ..')
                    } else {
                        $('#form1').submit();
                    }
                }
        );
    } ;


    $('#btn-save').click(function () {
        if (validateForm()) {
            validar();
        }
    });
    $('#btn-cancel').click(function () {
        window.location = '<?php echo $data['rootUrl']; ?>admin/servicios';
    });
</script>
