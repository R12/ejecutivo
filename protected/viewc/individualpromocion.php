<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">
<div class="wrapper">
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs-v3 img-v1" style="background: url(global/img/breadcrumbs/img3.jpg)">
        <div class="container text-center">
            <h1><?php echo $data["nombre"] ?></h1>
        </div><!--/end container-->
    </div>
    <!--=== End Breadcrumbs v3 ===-->
    <!--=== Container Part ===-->
    <div class="container">
        <div class="content">
            <!-- Magazine Slider -->
            <div class="carousel slide carousel-v2 margin-bottom-40" id="portfolio-carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="full-width img-responsive" src="<?php echo $data["rootUrl"] ?>files/fotos_promociones/<?php echo $data["foto"] ?>" alt="">
                    </div>
                   
                </div>
            </div>
            <div class="row margin-bottom-60">
                <div class="col-sm-12">
                    <div class="headline"><h2>Descripci&oacute;n</h2></div>
                    <p style="text-align: center;"><?php echo $data["descripcion"]; ?></p>
                    <br/>
                    <p style="text-align: center;"><strong>Valido Hasta: <?php echo $data["fecha_fin"] ?></strong></p>
                </div>
            </div>
            <!-- End Magazine Slider -->  
        </div>
    </div>    
    <!--=== End Container Part ===-->
</div><!--/wrapper-->