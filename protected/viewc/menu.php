<!--=== Header v5 ===-->   
<div class="header-v5 header-static">
    <!-- Topbar v3 -->
    <div class="topbar-v3">

    </div>
    <!-- End Topbar v3 -->

    <!-- Navbar -->
    <div class="navbar navbar-default mega-menu" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $data["rootUrl"]; ?>">
                    <img style="width: 225px;" id="logo-header" src="<?php echo $data["rootUrl"] ?>global/img/logo.png" alt="Logo">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
                <?php echo $data['htmlmenu']; ?>
            </div>
        </div>    
    </div>            
    <!-- End Navbar -->
</div>
<!--=== End Header v5 ===-->