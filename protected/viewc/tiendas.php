<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">    
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">
<style>
    .headline-center p{
        font-size: 18px;
    }
</style>
<div class="wrapper">
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs-v3 img-v1" style="background: url(files/fotos_paginas/<?php echo $data["foto"] ?>); background-repeat: no-repeat; background-position: center; background-size: cover;">
        <div class="container" style="text-align: <?php echo $data["align"]; ?>">
            <h1 style="font-family: R12titulo; text-transform: none; font-size: 72px;"><?php echo $data["titulo"]; ?></h1>
        </div><!--/end container-->
    </div>

    <div class="content" style="background-image: url(files/fotos_background/<?php echo $data["background"] ?>); background-position: center;">
        <div class="cube-portfolio container margin-bottom-60">

            <?php if($data["descripcion"]!=""){ ?>
            <div class="headline-center container margin-bottom-40">
                <?php echo $data["descripcion"]; ?>     
            </div>
            <?php } ?>

            <div class="content-xs">
                <div id="filters-container" class="cbp-l-filters-text content-xs">
                    <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> Todos </div> |
                    <?php foreach ($data["categoria"] as $ctg) { ?>
                        <div data-filter=".<?php echo $ctg->id ?>" class="cbp-filter-item"> <?php echo $ctg->nombre; ?> </div> |
                    <?php } ?>

                </div><!--/end Filters Container-->
            </div>

            <div id="grid-container" class="cbp-l-grid-agency">
                <?php foreach ($data["tiendas"] as $t) { ?>
                    <div class="cbp-item <?php echo $t["id_categoria"] ?>">
                        <div class="cbp-caption">
                            <div class="cbp-caption-defaultWrap">
                                <img src="<?php echo $data["rootUrl"] ?>files/fotos_tiendas/logos/<?php echo $t["foto"]; ?>" alt="<?php echo $t["nombre"]; ?>">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <div class="cbp-l-caption-body">
                                        <ul class="link-captions">
                                            <li><a target="_blank" href="<?php echo $t["link"]; ?>"><i class="rounded-x fa fa-link"></i></a></li>
                                            <li><a href="<?php echo $data["rootUrl"] ?>files/fotos_tiendas/logos/<?php echo $t["foto"]; ?>" class="cbp-lightbox" data-title="<?php echo $t["nombre"]; ?>"><i class="rounded-x fa fa-search"></i></a></li>
                                        </ul>
                                        <div class="cbp-l-grid-agency-title"><?php echo $t["nombre"]; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div><!--/end Grid Container-->
        </div>    
    </div>
    <!--=== End Cube-Portfdlio ===-->
</div><!--/wrapper-->
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/js/cube-portfolio/cube-portfolio-4.js"></script>