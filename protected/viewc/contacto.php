<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/pages/page_contact.css">
<div class="wrapper">
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <ul class="pull-right breadcrumb">
                <li><a href="<?php echo $data["rootUrl"] ?>">Inicio</a></li>
                <li class="active">Cont&aacute;ctenos</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!-- Google Map -->
    <div id="map" class="map">
        <?php echo $data["mapa"] ?>
    </div><!---/map-->
    <!-- End Google Map -->

    <!--=== Content Part ===-->
    <div class="container content">		
        <div class="row margin-bottom-30">
            <div class="col-md-8 mb-margin-bottom-30">
                <div class="headline"><h2>Formulario de cont&aacute;cto</h2></div>
                <form id="form1"  name="form1" class="sky-form contact-style">
                    <fieldset class="no-padding">
                        <label id="l_nombre">Nombre<span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-7 col-md-offset-0">
                                <div>
                                    <input type="text" name="nombre" id="nombre" class="form-control">
                                </div>
                            </div>                
                        </div>

                        <label id="l_email">Email <span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-7 col-md-offset-0">
                                <div>
                                    <input type="text" name="email" id="email" class="form-control">
                                </div>
                            </div>                
                        </div>

                        <label id="l_mensaje">Mensaje <span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-11 col-md-offset-0">
                                <div>
                                    <textarea rows="8" name="mensaje" id="mensaje" class="form-control"></textarea>
                                </div>
                            </div>                
                        </div>
                        <div class="clearfix"></div><br/>
                        <p><button type="button" id="enviar" class="btn-u">Enviar</button></p>
                    </fieldset>

                    <div class="message" id="mostrar"></div>
                </form>
            </div><!--/col-md-9-->

            <div class="col-md-4">
                <!-- Business Hours -->
                <div class="headline"><h2>Informaci&oacute;n</h2></div>
                <ul class="list-unstyled margin-bottom-30">
                    <li><strong>Email:</strong><?php echo $data["email_empresa"] ?></li>
                    <li><strong>Tel&eacute;fono:</strong><?php echo $data["telefono_empresa"] ?></li>
                    <li><strong><?php echo $data["direccion_empresa"] ?></strong></li>
                    <li><strong><?php echo $data["ciudad_empresa"] ?></strong></li>
                </ul>
            </div><!--/col-md-3-->
        </div><!--/row-->        
    </div><!--/container-->		
    <!--=== End Content Part ===-->
</div><!--/wrapepr-->
<script type="text/javascript" src="<?php echo $data['rootUrl']; ?>global/admin/js/form.js"></script>
<script type="text/javascript">
    function validateForm() {
        var sErrMsg = "";
        var flag = true;
        sErrMsg += validateText($('#nombre').val(), $('#l_nombre').html(), true);
        sErrMsg += validateEmail($('#email').val(), $('#l_email').html(), true);
        sErrMsg += validateText($('#mensaje').val(), $('#l_mensaje').html(), true);
        if (sErrMsg !== "")
        {
            alert(sErrMsg);
            flag = false;
        }
        return flag;
    }
    $('#enviar').click(function () {
        var msg = '';
        if (validateForm()) {
            $.post('<?php echo $data['rootUrl']; ?>contactenos/send', {nombre: $('#nombre').val(), email: $('#email').val(), mensaje: $('#mensaje').val()}, function (data) {
                //           $('#mostrar').html(data);
                if (data) {
                    $('#nombre').val('');
                    $('#email').val('');
                    $('#mensaje').val('');
                    msg = "<i class='rounded-x fa fa-check'></i><p>Su mensaje fue enviado con &eacute;xito!</p>";
                } else {
                    msg = "<i class='rounded-x fa fa-times'></i><p>Error al enviar!</p>";
                }
                $('#mostrar').html(msg);
            });
        }
    });
</script>