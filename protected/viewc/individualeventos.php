
<link rel="stylesheet" href="<?php echo $data["rootUrl"]; ?>global/plugins/owl-carousel/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="<?php echo $data["rootUrl"]; ?>global/css/pages/portfolio-v1.css">
<div class="wrapper">
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?php echo $data["nombre"] ?></h1>
            <ul class="pull-right breadcrumb">
                <li><a href="<?php echo $data["rootUrl"] ?>">Inicio</a></li>
                <li><a href="<?php echo $data["rootUrl"] ?>eventos">Eventos</a></li>
                <li class="active"><?php echo $data["nombre"] ?></li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content"> 	
    	<div class="row portfolio-item margin-bottom-50"> 
            <!-- Carousel -->
            <div class="col-md-7">
                <div class="carousel slide carousel-v1" id="myCarousel">
                    <div class="carousel-inner">
                        <?php foreach ($data["ftevento"] as $ftevento) { ?>
                        
                        <div class="item active">
                            <img alt="" src="<?php echo $data["rootUrl"]; ?>files/fotos_eventos/img_galeria/<?php echo $ftevento["foto"]; ?>">
                            <div class="carousel-caption">
                                <p><?php echo $ftevento["nombre"]; ?></p>
                            </div>
                        </div>
                        
                        <?php } ?>
                        
                    </div>
                    
                    <div class="carousel-arrow">
                        <a data-slide="prev" href="#myCarousel" class="left carousel-control">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a data-slide="next" href="#myCarousel" class="right carousel-control">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- End Carousel -->
            <!-- Content Info -->        
            <div class="col-md-5">
                <h2><?php echo $data["nombre"]; ?></h2>
                <?php echo $data["descripcion"]; ?>
                <ul class="list-unstyled">
                    <li><i class="fa fa-calendar color-blue"></i>&nbsp;<?php echo $data["fecha"] ?></li>
                </ul>
            </div>
            <!-- End Content Info -->        
        </div><!--/row-->
        <div class="margin-bottom-20 clearfix"></div>    

        <!-- Recent Works -->
        <div class="owl-carousel-v1 owl-work-v1 margin-bottom-40">
            <div class="headline"><h2 class="pull-left">&nbsp;</h2>
                <div class="owl-navigation">
                    <div class="customNavigation">
                        <a class="owl-btn prev-v2"><i class="fa fa-angle-left"></i></a>
                        <a class="owl-btn next-v2"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div><!--/navigation-->
            </div>

            <div class="owl-recent-works-v1">
                <?php foreach ($data["tienda"] as $t) { ?>
                <div class="item">
                    <a href="#">
                        <em class="overflow-hidden">
                            <img class="img-responsive" src="<?php echo $data["rootUrl"] ?>files/fotos_tiendas/logos/<?php echo $t["foto"]; ?>" alt="<?php echo $t["nombre"]; ?>">
                        </em>    
                    </a>    
                </div>
                <?php } ?>
            </div>
        </div>    
        <!-- End Recent Works -->
    </div><!--/container-->	 	
    <!--=== End Content Part ===-->
</div><!--/wrapper-->