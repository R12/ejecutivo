<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/pages/page_404_error.css">
<div class="wrapper">
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            
        </div> 
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
        <!--Error Block-->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="error-v1">
                    <span class="error-v1-title">404</span>
                    <span>¡Eso es un error!</span>
                    <p>La URL solicitada no se encuentra en este servidor</p>
                    <a class="btn-u btn-bordered" href="<?php echo $data["rootUrl"] ?>">Regresar a inicio</a>
                </div>
            </div>
        </div>
        <!--End Error Block-->
    </div>	
    <!--=== End Content Part ===-->
</div><!--/wrapper-->