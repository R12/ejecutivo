<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">    
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">
<style>
    .headline-center p{
        font-size: 18px;
    }
</style>
<div class="wrapper">
    <div class="breadcrumbs-v3 img-v1" style="background: url(files/fotos_paginas/<?php echo $data["foto"] ?>); background-repeat: no-repeat; background-position: center; background-size: cover;">
        <div class="container" style="text-align: <?php echo $data["align"]; ?>">
            <h1 style="font-family: R12titulo; text-transform: none; font-size: 72px;"><?php echo $data["titulo"]; ?></h1>
        </div><!--/end container-->
    </div>
    <div class="content" style="background-image: url(files/fotos_background/<?php echo $data["background"] ?>); background-position: center;">
        <div class="cube-portfolio container margin-bottom-60" >
            <div class="container">
                <div class="title-v1 no-margin-bottom">
                    <?php echo $data["descripcion"]; ?>              
                </div>
            </div>
            <br/>
            <div id="grid-container" class="cbp-l-grid-agency">
                <?php foreach ($data["eventos"] as $e) { ?>
                    <div class="cbp-item graphic">
                        <div class="cbp-caption margin-bottom-20">
                            <div class="cbp-caption-defaultWrap">
                                <img src="<?php echo $data["rootUrl"] ?>files/fotos_eventos/img_principal/<?php echo $e->foto ?>" alt="<?php echo $e->nombre ?>">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <div class="cbp-l-caption-body">
                                        <ul class="link-captions no-bottom-space">
                                            <li><a href="<?php echo $data["rootUrl"] ?>showeventos?id=<?php echo $e->id; ?>"><i class="rounded-x fa fa-link"></i></a></li>
                                            <li><a href="<?php echo $data["rootUrl"] ?>files/fotos_eventos/img_principal/<?php echo $e->foto ?>" class="cbp-lightbox" data-title="<?php echo $e->nombre ?>"><i class="rounded-x fa fa-search"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cbp-title-dark">
                            <div class="cbp-l-grid-agency-title"><?php echo $e->nombre ?></div>
                            <div class="cbp-l-grid-agency-desc"><?php echo $e->fecha ?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div> 
    </div>
    <div class="container content">
        <div class="heading heading-v1 margin-bottom-40">
            <h2 style="font-family: R12titulo; font-size: 32px;">Almacenes</h2>
        </div>

        <ul class="list-inline owl-slider-v2">
            <?php foreach ($data["tienda"] as $t) { ?>
                <li class="item first-child">
                    <img src="<?php echo $data["rootUrl"] ?>files/fotos_tiendas/logos/<?php echo $t["foto"]; ?>" alt="">
                </li>
            <?php } ?>
        </ul><!--/end owl-carousel-->
    </div>
</div><!--/wrapper-->
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/js/cube-portfolio/cube-portfolio-4.js"></script>

