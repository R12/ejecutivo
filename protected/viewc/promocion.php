<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">  
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">
<style>
    .headline-center p{
        font-size: 18px;
    }
</style>
<div class="wrapper">
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs-v3 img-v1" style="background: url(files/fotos_paginas/<?php echo $data["foto"] ?>); background-repeat: no-repeat; background-position: center; background-size: cover;">
        <div class="container" style="text-align: <?php echo $data["align"]; ?>">
            <h1 style="font-family: R12titulo; text-transform: none; font-size: 72px;"><?php echo $data["titulo"]; ?></h1>
        </div><!--/end container-->
    </div>

    <div class="content" style="background-image: url(files/fotos_background/<?php echo $data["background"] ?>); background-position: center;">
        <div class="cube-portfolio container margin-bottom-60">
            
            <div class="headline-center container margin-bottom-40">
                <?php echo $data["descripcion"]; ?>     
            </div>
            
            
            <div id="grid-container" class="cbp-l-grid-agency">
                <?php foreach ($data["promocion"] as $p) { ?>
                    <div class="cbp-item web-design">
                        <div class="cbp-caption">
                            <div class="cbp-caption-defaultWrap">
                                <img src="<?php echo $data["rootUrl"] ?>files/fotos_promociones/<?php echo $p["foto"] ?>" alt="<?php echo $p["nombre"] ?>" />
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <div class="cbp-l-caption-body">
                                        <ul class="link-captions">
                                            <li><a href="<?php echo $data["rootUrl"] ?>showpromociones?id=<?php echo $p["id"] ?>"><i class="rounded-x fa fa-link"></i></a></li>
                                            <li><a href="<?php echo $data["rootUrl"] ?>files/fotos_promociones/<?php echo $p["foto"] ?>" class="cbp-lightbox" data-title="<?php echo $p["nombre"] ?>"><i class="rounded-x fa fa-search"></i></a></li>
                                        </ul>
                                        <div class="cbp-l-grid-agency-title"><?php echo $p["nombre"] ?></div>
                                        <div class="cbp-l-grid-agency-desc"><?php echo $p["tienda"] ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div><!--/end Grid Container-->
        </div> 
    </div>
    <!--=== End Cube-Portfdlio ===-->
</div><!--/wrapper-->
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/js/cube-portfolio/cube-portfolio-2.js"></script>
