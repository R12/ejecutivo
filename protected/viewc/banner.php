<!--=== Slider ===-->
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <?php foreach ($data["banner"] as $b) { ?>
                <!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 3">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo $data["rootUrl"] ?>files/fotos_banner/<?php echo $b["nombreimagen"] ; ?>"  alt="<?php echo $b["nombre"] ; ?>"  data-bgfit="cover" data-bgposition="right top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch3 sft start"
                        data-x="right"
                        data-hoffset="5"
                        data-y="130"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"
                        data-endspeed="300">
                        <strong><?php echo $b["nombre"] ; ?></strong> 
                    </div>
                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch4 sft"
                        data-x="right"
                        data-hoffset="0"
                        data-y="210"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6; font-size: 24px;">
                        <?php echo $b["descripcion"] ; ?>
                    </div>
                </li>
                <!-- END SLIDE -->
                <?php } ?>

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
    <!--=== End Slider ===-->
