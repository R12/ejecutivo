<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">  
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">
<link rel="stylesheet" type="text/css" href="<?php echo $data['rootUrl'] ?>global/plugins/titles/css/jquery.powertip.css"/>
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">

<div class="wrapper">
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs-v3 img-v1" style="background: url(files/fotos_paginas/<?php echo $data["foto"] ?>); background-repeat: no-repeat; background-position: center; background-size: cover;">
        <div class="container" style="text-align: <?php echo $data["align"]; ?>">
            <h1 style="font-family: R12titulo; font-size: 50px;"><?php echo $data["titulo"] ?></h1>
        </div><!--/end container-->
    </div>
    <!--=== End Breadcrumbs v3 ===-->
    <!--=== Cube-Portfdlio ===-->
    <div class="content" style="background-image: url(files/fotos_background/<?php echo $data["background"] ?>); background-position: center;">
        <div class="cube-portfolio container margin-bottom-60" style="margin-top: 60px;">
            <div class="col-lg-4">
                <div class="list-group" style="height: 700px; overflow: scroll;">
                    <?php foreach ($data["locales"] as $t) { ?>
                    <a  class="list-group-item" id="<?php echo $t["nombre"] ?>" datanombre="<?php echo $t["almacen"]."&nbsp;".$t["nombre"]?>">
                        <span class="glyphicon glyphicon-map-marker">&nbsp;</span> 
                                <?php echo $t["almacen"]; ?>
                            <span class="badge"><?php echo $t["nombre"]; ?></span>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="cbp-item web-design">
                <div class="block"><p><a class="tooltips" href="#" id="nalmacen">
                            <span></span></a></p></div>
                <img src="<?php echo $data["rootUrl"] ?>global/img/plano2.jpg" />
                <map name="image-map">
                    <area target="" alt="121" title="121" href="" coords="464,278" shape="rect">
                    <area target="" alt="120" title="120" href="" coords="451,468" shape="rect">
                    <area target="" alt="22-1" title="22-1" href="" coords="518,101" shape="rect">
                    <area target="" alt="21" title="21" href="" coords="502,109" shape="rect">
                    <area target="" alt="2" title="2" href="" coords="502,87" shape="rect">
                    <area target="" alt="20" title="20" href="" coords="486,106" shape="rect">
                    <area target="" alt="3" title="3" href="" coords="486,88" shape="rect">
                    <area target="" alt="19" title="19" href="" coords="470,107" shape="rect">
                    <area target="" alt="4" title="4" href="" coords="470,88" shape="rect">
                    <area target="" alt="5" title="5" href="" coords="454,87" shape="rect">
                    <area target="" alt="6" title="6" href="" coords="438,88" shape="rect">
                    <area target="" alt="7" title="7" href="" coords="422,87" shape="rect">
                    <area target="" alt="8" title="8" href="" coords="406,87" shape="rect">
                    <area target="" alt="9" title="9" href="" coords="390,87" shape="rect">
                    <area target="" alt="14" title="14" href="" coords="390,107" shape="rect">
                    <area target="" alt="15" title="15" href="" coords="406,107" shape="rect">
                    <area target="" alt="16" title="16" href="" coords="422,107" shape="rect">
                    <area target="" alt="17" title="17" href="" coords="438,107" shape="rect">
                    <area target="" alt="18" title="18" href="" coords="455,108" shape="rect">
                    <area target="" alt="19" title="19" href="" coords="470,107" shape="rect">
                    <area target="" alt="20" title="20" href="" coords="487,108" shape="rect">
                    <area target="" alt="21" title="21" href="" coords="502,107" shape="rect">
                    <area target="" alt="10-13" title="10-13" href="" coords="348,100" shape="rect">
                    <area target="" alt="23-31" title="23-31" href="" coords="297,177" shape="rect">
                    <area target="" alt="32" title="32" href="" coords="314,249" shape="rect">
                    <area target="" alt="33" title="33" href="" coords="292,260" shape="rect">
                    <area target="" alt="34" title="34" href="" coords="279,258" shape="rect">
                    <area target="" alt="35" title="35" href="" coords="266,254" shape="rect">
                    <area target="" alt="36" title="36" href="" coords="250,259" shape="rect">
                    <area target="" alt="37" title="37" href="" coords="240,274" shape="rect">
                    <area target="" alt="38" title="38" href="" coords="250,290" shape="rect">
                    <area target="" alt="39" title="39" href="" coords="265,302" shape="rect">
                    <area target="" alt="40" title="40" href="" coords="281,309" shape="rect">
                    <area target="" alt="41" title="41" href="" coords="306,323" shape="rect">
                    <area target="" alt="42" title="42" href="" coords="307,308" shape="rect">
                    <area target="" alt="43" title="43" href="" coords="308,293" shape="rect">
                    <area target="" alt="44" title="44" href="" coords="310,281" shape="rect">
                    <area target="" alt="45" title="45" href="" coords="311,266" shape="rect">
                    <area target="" alt="46" title="46" href="" coords="318,365" shape="rect">
                    <area target="" alt="47" title="47" href="" coords="302,371" shape="rect">
                    <area target="" alt="48" title="48" href="" coords="284,371" shape="rect">
                    <area target="" alt="49" title="49" href="" coords="268,370" shape="rect">
                    <area target="" alt="50" title="50" href="" coords="253,366" shape="rect">
                    <area target="" alt="51" title="51" href="" coords="240,351" shape="rect">
                    <area target="" alt="52" title="52" href="" coords="233,369" shape="rect">
                    <area target="" alt="53" title="53" href="" coords="225,388" shape="rect">
                    <area target="" alt="54" title="54" href="" coords="220,406" shape="rect">
                    <area target="" alt="55" title="55" href="" coords="240,406" shape="rect">
                    <area target="" alt="56" title="56" href="" coords="254,410" shape="rect">
                    <area target="" alt="57" title="57" href="" coords="267,414" shape="rect">
                    <area target="" alt="58" title="58" href="" coords="284,420" shape="rect">
                    <area target="" alt="59" title="59" href="" coords="300,421" shape="rect">
                    <area target="" alt="60" title="60" href="" coords="318,428" shape="rect">
                    <area target="" alt="61" title="61" href="" coords="318,405" shape="rect">
                    <area target="" alt="62" title="62" href="" coords="318,387" shape="rect">
                    <area target="" alt="63" title="63" href="" coords="206,334" shape="rect">
                    <area target="" alt="64" title="64" href="" coords="185,334" shape="rect">
                    <area target="" alt="65" title="65" href="" coords="175,323" shape="rect">
                    <area target="" alt="66" title="66" href="" coords="140,315" shape="rect">
                    <area target="" alt="67" title="67" href="" coords="100,325" shape="rect">
                    <area target="" alt="68" title="68" href="" coords="112,337" shape="rect">
                    <area target="" alt="69" title="69" href="" coords="122,348" shape="rect">
                    <area target="" alt="70" title="70" href="" coords="135,360" shape="rect">
                    <area target="" alt="71" title="71" href="" coords="154,357" shape="rect">
                    <area target="" alt="72" title="72" href="" coords="165,371" shape="rect">
                    <area target="" alt="73" title="73" href="" coords="177,387" shape="rect">
                    <area target="" alt="74" title="74" href="" coords="186,370" shape="rect">
                    <area target="" alt="75" title="75" href="" coords="196,352" shape="rect">
                    <area target="" alt="87" title="87" href="" coords="158,427" shape="rect">
                    <area target="" alt="88" title="88" href="" coords="145,421" shape="rect">
                    <area target="" alt="89" title="89" href="" coords="135,415" shape="rect">
                    <area target="" alt="90" title="90" href="" coords="126,409" shape="rect">
                    <area target="" alt="91" title="91" href="" coords="116,402" shape="rect">
                    <area target="" alt="92" title="92" href="" coords="106,395" shape="rect">
                    <area target="" alt="93" title="93" href="" coords="99,386" shape="rect">
                    <area target="" alt="94" title="94" href="" coords="90,379" shape="rect">
                    <area target="" alt="95" title="95" href="" coords="80,370" shape="rect">
                    <area target="" alt="96" title="96" href="" coords="72,363" shape="rect">
                    <area target="" alt="97" title="97" href="" coords="64,352" shape="rect">
                    <area target="" alt="98" title="98" href="" coords="50,369" shape="rect">
                    <area target="" alt="99" title="99" href="" coords="60,375" shape="rect">
                    <area target="" alt="100" title="100" href="" coords="66,387" shape="rect">
                    <area target="" alt="101" title="101" href="" coords="76,395" shape="rect">
                    <area target="" alt="102" title="102" href="" coords="86,403" shape="rect">
                    <area target="" alt="103" title="103" href="" coords="95,411" shape="rect">
                    <area target="" alt="104" title="104" href="" coords="104,419" shape="rect">
                    <area target="" alt="105" title="105" href="" coords="116,426" shape="rect">
                    <area target="" alt="106" title="106" href="" coords="125,434" shape="rect">
                    <area target="" alt="107" title="107" href="" coords="136,437" shape="rect">
                    <area target="" alt="108" title="108" href="" coords="148,446" shape="rect">
                </map>
            </div>
        </div><!--/end Grid Container-->
    </div> 
</div>
<!--=== End Cube-Portfdlio ===-->
</div><!--/wrapper-->
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/js/cube-portfolio/cube-portfolio-2.js"></script>
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/js/plano.js"></script>
<script>
    $(function () {
        $('.east').powerTip({placement: 'e'});
    });
</script>
