<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="es"> <!--<![endif]-->
    <head>
        <title>Centro Comercial Los Ejecutivos</title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <!-- Web Fonts -->
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>
        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/shop.style.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/plugins.css">

        <!-- CSS Header and Footer -->
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/headers/header-v5.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/footers/footer-v4.css">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/animate.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/line-icons/line-icons.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/owl-carousel/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/revolution-slider/rs-plugin/css/settings.css">
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
        <script type="text/javascript" src="<?php echo $data['rootUrl'] ?>global/plugins/titles/jquery.powertip.min.js"></script>
        <!-- CSS Customization -->
        <link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/custom.css">
    </head>
    <body class="header-fixed">

        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <div class="wrapper">
            <?php include 'menu.php' ?>

            <?php include $data['content'] . ".php"; ?>

            <?php include 'footer.php' ?>
        </div>

        <script src="<?php echo $data["rootUrl"] ?>global/plugins/jquery/jquery-migrate.min.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!-- JS Implementing Plugins -->
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/back-to-top.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/smoothScroll.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/jquery.parallax.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/js/plugins/owl-recent-works.js"></script>

        <!-- JS Customization -->
        <script src="<?php echo $data["rootUrl"] ?>global/js/custom.js"></script>
        <!-- JS Page Level -->
        <script src="<?php echo $data["rootUrl"] ?>global/js/shop.app.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/js/plugins/owl-carousel.js"></script>
        <script src="<?php echo $data["rootUrl"] ?>global/js/plugins/revolution-slider.js"></script>
        <script>
            jQuery(document).ready(function () {
                App.init();
                App.initScrollBar();
                App.initParallaxBg();
                OwlCarousel.initOwlCarousel();
                RevolutionSlider.initRSfullWidth();
                OwlRecentWorks.initOwlRecentWorksV1();
            });
        </script>
        <!--[if lt IE 9]>
            <script src="<?php echo $data["rootUrl"] ?>global/plugins/respond.js"></script>
            <script src="<?php echo $data["rootUrl"] ?>global/plugins/html5shiv.js"></script>
            <script src="<?php echo $data["rootUrl"] ?>global/js/plugins/placeholder-IE-fixes.js"></script>
        <![endif]-->

    </body>
</html>
