<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/css/blocks.css">
<link rel="stylesheet" href="<?php echo $data["rootUrl"] ?>global/plugins/owl-carousel/owl-carousel/owl.carousel.css">
<style>
    p > img {
        margin-right: 20px;
    }
</style>
<div class="wrapper">
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs-v3 img-v1" style="background: url(files/fotos_paginas/<?php echo $data["imagen"] ?>); background-repeat: no-repeat; background-position: center; background-size: cover;">
        <div class="container" style="text-align: <?php echo $data["align"]; ?>">
            <p><?php ?></p>
            <h1 style="font-family: R12titulo; font-size: 50px;"><?php echo $data["titulo"] ?></h1>
        </div><!--/end container-->
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <!--=== Process v1 ===-->
    <div class="process-v1 bg-color-light">
        <div class="container content-sm">
            <div class=" margin-bottom-60">
                <?php echo $data["html"] ?>
            </div>
        </div><!--/end container-->
    </div>
    <!--=== End Process v1 ===-->

    <!--=== Owl Clients v1 ===-->
    <div class="container content">
        <div class="heading heading-v1 margin-bottom-40">
            <h2 style="font-family: R12titulo; font-size: 32px;">Almacenes</h2>
        </div>
        <ul class="list-inline owl-slider-v2">
            <?php foreach ($data["tienda"] as $t) { ?>
                <li class="item first-child">
                    <img src="<?php echo $data["rootUrl"] ?>files/fotos_tiendas/logos/<?php echo $t["foto"]; ?>" alt="<?php echo $t["nombre"]; ?>">
                </li>
            <?php } ?>
        </ul><!--/end owl-carousel-->
    </div>
    <!--=== End Owl Clients v1 ===-->
</div><!--/wrapper-->
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo $data["rootUrl"] ?>global/js/plugins/owl-carousel.js"></script>
