<!--=== Footer v4 ===-->
<div class="footer-v4">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-4 md-margin-bottom-40">
                    <div class="fb-page" data-href="https://facebook.com/CentroComercialSupercentroLosEjecutivos/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://facebook.com/CentroComercialSupercentroLosEjecutivos/" class="fb-xfbml-parse-ignore"><a href="https://facebook.com/CentroComercialSupercentroLosEjecutivos/">Centro Comercial Supercentro Los Ejecutivos</a></blockquote></div>

                </div>
                <!-- End About -->                    

                <!-- End Simple List -->
            </div><!--/end row-->
        </div><!--/end continer-->
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">                     
                    <p>
                        2016 &copy; Desarrollado por <a>Web and net S.A.S</a>. 
                    </p>
                </div>
            </div>
        </div> 
    </div><!--/copyright--> 
</div>
<!--=== End Footer v4 ===-->