<?php include 'banner.php' ?>
<!--=== Product Content ===-->
<div class="content" style="background-image: url(global/img/fondo2.png); background-position: center;">
    <div class="container">
        <div class="heading heading-v1 margin-bottom-20">
            <h2 style="font-family: R12titulo; color: #1E85C2; font-size: 40px; text-transform: none;">Nuestras Promociones</h2>
        </div>
        <!--=== Illustration v3 ===-->
        <div class="row margin-bottom-50">
            <?php foreach ($data["promocion"] as $p) { ?>
                <div class="col-md-4 md-margin-bottom-30">
                    <div class="overflow-h">
                        <a class="illustration-v3" style="background-image: url(<?php echo $data["rootUrl"] ?>files/fotos_promociones/<?php echo $p["foto"] ?>);" href="<?php echo $data["rootUrl"] ?>showpromociones?id=<?php echo $p["id"] ?>">
                            <span class="illustration-bg">
                                <span class="illustration-ads">
                                    <span class="illustration-v3-category">
                                        <span class="product-category"><?php echo $p["tienda"]; ?></span>
                                    </span>    
                                </span>    
                            </span>    
                        </a>
                    </div>    
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!--=== End Product Content ===-->
<!--=== Collection Banner ===-->
<div class="collection-banner">
    <div class="container">
        <div class="col-md-7 md-margin-bottom-50">
            <!-- <h2>Makeup collection</h2>
             <p>Duis in posuere risus. Vivamus sed dignissim tellus. In vehicula justo tempor commodo. <br> Nunc lobortis dapibus neque quis lacinia</p><br>
             <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Shop Now</a>-->
        </div>
        <div class="col-md-5">
            <div class="overflow-h">

                <span class="percent-numb">&nbsp;</span>
                <div class="percent-off">
                    <span class="discount-percent">&nbsp;</span>
                    <span class="discount-off">&nbsp;</span>
                </div>
                <div class="new-offers ">
                    <p>&nbsp;</p>
                    <span>&nbsp;</span>
                </div>    
            </div>    
        </div>
    </div>
</div>
<!--=== End Collection Banner ===-->
<!--=== Sponsors ===-->
<div class="container content">
    <div class="heading heading-v1 margin-bottom-40">
        <h2 style="font-family: R12titulo; font-size: 32px;">Almacenes</h2>
    </div>

    <ul class="list-inline owl-slider-v2">
        <?php foreach ($data["tienda"] as $t) { ?>
            <li class="item first-child">
                <img src="<?php echo $data["rootUrl"] ?>files/fotos_tiendas/logos/<?php echo $t["foto"]; ?>" alt="">
            </li>
        <?php } ?>
    </ul><!--/end owl-carousel-->
</div>
<!--=== End Sponsors ===-->