<?php
// rutas para MainController
$route['*']['/'] = array('MainController', 'index');
$route['*']['/home'] = array('MainController', 'index');
$route['*']['/error'] = array('ErrorController', 'index');
$route['*']['/imagen/:pindex'] = array('MainController', 'imagen');
$route['*']['root']['/:pagina'] = array('MainController', 'showPageService');
$route['*']['/nosotros'] = array('MainController', 'showPage');

$route['*']['/eventos'] = array('MainController', 'getAllEventos');
$route['*']['/showeventos'] = array('MainController', 'ShowEventos');

$route['*']['/promociones'] = array('MainController', 'getPromociones');
$route['*']['/showpromociones'] = array('MainController', 'ShowPromociones');

$route['*']['/tiendas'] = array('MainController', 'getTiendas');

$route['*']['/contacto'] = array('MainController', 'getContacto');
$route['*']['/contactenos/send'] = array('MainController', 'send');

$route['*']['/locales'] = array('MainController', 'getLocales');

// rutas para AdminController
$route['*']['/admin'] = array('admin/AdminController', 'index');
$route['*']['/admin/login'] = array('admin/AdminController', 'login');
$route['*']['/admin/logout'] = array('admin/AdminController', 'logout');
$route['*']['/panel/logout'] = array('admin/AdminController', 'logout');
$route['*']['/panel/home'] = array('admin/AdminController', 'home');
//rutas para PaginasController
$route['*']['/admin/paginas'] = array('admin/PaginasController', 'index');
$route['*']['/admin/paginas/save'] = array('admin/PaginasController', 'save');
$route['*']['/admin/paginas/add'] = array('admin/PaginasController', 'add');
$route['*']['/admin/paginas/edit/:pindex'] = array('admin/PaginasController', 'edit');
// rutas para ParametrosController
$route['*']['/admin/parametros'] = array('admin/ParametrosController', 'index');
$route['*']['/admin/parametros/save'] = array('admin/ParametrosController', 'save');
//Roles
$route['*']['/admin/roles'] = array('admin/RolesController', 'index');
$route['*']['/admin/roles/add'] = array('admin/RolesController', 'add');
$route['*']['/admin/roles/edit/:pindex'] = array('admin/RolesController', 'edit');
$route['*']['/admin/roles/save'] = array('admin/RolesController', 'save');
//usuarios
$route['*']['/admin/usuarios'] = array('admin/UsuariosController', 'index');
$route['*']['/admin/usuarios/add'] = array('admin/UsuariosController', 'add');
$route['*']['/admin/usuarios/edit/:pindex'] = array('admin/UsuariosController', 'edit');
$route['*']['/admin/usuarios/save'] = array('admin/UsuariosController', 'save');
$route['*']['/admin/usuarios/delete/:pindex'] = array('admin/UsuariosController', 'deactivate');
$route['*']['/admin/usuarios/page/:filter/:texto/:number'] = array('admin/UsuariosController', 'index');
$route['*']['/admin/usuarios/validar'] = array('admin/UsuariosController', 'validar');

//Banners
$route['*']['/admin/banners'] = array('admin/BannersController', 'index');
$route['*']['/admin/banners/add'] = array('admin/BannersController', 'add');
$route['*']['/admin/banners/edit/:pindex'] = array('admin/BannersController', 'edit');
$route['*']['/admin/banners/save'] = array('admin/BannersController', 'save');
$route['*']['/admin/banners/delete/:pindex'] = array('admin/BannersController', 'deactivate');
$route['*']['/admin/banners/page/:filter/:texto/:number'] = array('admin/BannersController', 'index');
$route['*']['/admin/banners/validar'] = array('admin/BannersController', 'validar');


//cambio de contrasena
$route['*']['/panel/cambio'] = array('admin/CambiarController', 'index');
$route['*']['/panel/cambiar'] = array('admin/CambiarController', 'update');
$route['*']['/admin/cambiar/validar'] = array('admin/CambiarController', 'validar');

//Categoria de Categoria de eventos
$route['*']['/admin/categoriaeventos'] = array('admin/CategoriaEventosController', 'index');
$route['*']['/admin/categoriaeventos/add'] = array('admin/CategoriaEventosController', 'add');
$route['*']['/admin/categoriaeventos/edit/:pindex'] = array('admin/CategoriaEventosController', 'edit');
$route['*']['/admin/categoriaeventos/save'] = array('admin/CategoriaEventosController', 'save');
$route['*']['/admin/categoriaeventos/delete/:pindex'] = array('admin/CategoriaEventosController', 'deactivate');
$route['*']['/admin/categoriaeventos/page/:filter/:texto/:number'] = array('admin/CategoriaEventosController', 'index');
$route['*']['/admin/categoriaeventos/validar'] = array('admin/CategoriaEventosController', 'validar');

//Sliders
$route['*']['/admin/sliders'] = array('admin/SlidersController', 'index');
$route['*']['/admin/sliders/add'] = array('admin/SlidersController', 'add');
$route['*']['/admin/sliders/edit/:pindex'] = array('admin/SlidersController', 'edit');
$route['*']['/admin/sliders/save'] = array('admin/SlidersController', 'save');
$route['*']['/admin/sliders/delete/:pindex'] = array('admin/SlidersController', 'deactivate');
$route['*']['/admin/sliders/page/:filter/:texto/:number'] = array('admin/SlidersController', 'index');
$route['*']['/admin/sliders/validar'] = array('admin/SlidersController', 'validar');



//Controladores de clientes
$route['*']['/admin/actualizarperfil/:pindex'] = array('admin/PerfilController', 'updatePerfil');
$route['*']['/admin/clientes/saveperfil'] = array('admin/PerfilController', 'SaveupdatePerfil');
$route['*']['/admin/actualizarperfil/validar'] = array('admin/PerfilController', 'validar');

$route['*']['/admin/clientes'] = array('admin/ClientesController', 'index');
$route['*']['/admin/clientes/pendientes'] = array('admin/ClientesController', 'index');
$route['*']['/admin/clientes/add'] = array('admin/ClientesController', 'add');
$route['*']['/admin/clientes/edit/:pindex'] = array('admin/ClientesController', 'edit');
$route['*']['/admin/clientes/save'] = array('admin/ClientesController', 'save');
$route['*']['/admin/clientes/delete/:pindex'] = array('admin/ClientesController', 'deactivate');

$route['*']['/admin/clientes/activate/:pindex'] = array('admin/ClientesController', 'activate');

$route['*']['/admin/clientes/page/:filter/:texto/:number'] = array('admin/ClientesController', 'index');
$route['*']['/admin/clientes/validar'] = array('admin/ClientesController', 'validar');

//Eventos
$route['*']['/admin/eventos'] = array('admin/EventosController', 'index');
$route['*']['/admin/clear'] = array('admin/EventosController', 'cancel');
$route['*']['/admin/eventos/add'] = array('admin/EventosController', 'add');
$route['*']['/admin/eventos/edit/:pindex'] = array('admin/EventosController', 'edit');

$route['*']['/admin/eventos/save'] = array('admin/EventosController', 'save');
$route['*']['/admin/eventos/delete/:pindex'] = array('admin/EventosController', 'deactivate');
$route['*']['/admin/eventos/activate/:pindex'] = array('admin/EventosController', 'activate');
$route['*']['/admin/eventos/page/:filter/:texto/:number'] = array('admin/EventosController', 'index');
$route['*']['/admin/eventos/validar'] = array('admin/EventosController', 'validar');

// Fotos Eventos
$route['*']['/admin/fotoevento'] = array('admin/FotosEventosController', 'index');
$route['*']['/admin/fotoevento/add'] = array('admin/FotosEventosController', 'add');
$route['*']['/admin/fotoevento/save'] = array('admin/FotosEventosController', 'save');
$route['*']['/admin/fotoevento/edit/:pindex'] = array('admin/FotosEventosController', 'edit');
$route['*']['/admin/fotoevento/delete/:pindex'] = array('admin/FotosEventosController', 'deactivate');
$route['*']['/admin/fotoevento/verimagen'] = array('admin/FotosEventosController', 'verimagen');
$route['*']['/admin/fotoevento/validar'] = array('admin/FotosEventosController', 'validar');
$route['*']['/admin/fotoevento/page/:filter/:texto/:number'] = array('admin/FotosEventosController', 'index');

// Tiendas
$route['*']['/admin/tiendas'] = array('admin/TiendasController', 'index');
$route['*']['/admin/tiendas/add'] = array('admin/TiendasController', 'add');
$route['*']['/admin/tiendas/save'] = array('admin/TiendasController', 'save');
$route['*']['/admin/tiendas/edit/:pindex'] = array('admin/TiendasController', 'edit');
$route['*']['/admin/tiendas/delete/:pindex'] = array('admin/TiendasController', 'deactivate');
$route['*']['/admin/tiendas/verimagen'] = array('admin/TiendasController', 'verimagen');
$route['*']['/admin/tiendas/validar'] = array('admin/TiendasController', 'validar');
$route['*']['/admin/tiendas/page/:filter/:texto/:number'] = array('admin/TiendasController', 'index');

$route['*']['/admin/tiendas/validarCategoria'] = array('admin/TiendasController', 'Validarcategorias');
$route['*']['/admin/tiendas/agregar_categoria'] = array('admin/TiendasController', 'agregar_categoria');
$route['*']['/admin/tiendas/cargar_categorias'] = array('admin/TiendasController', 'cargar_categoria');
$route['*']['/admin/tiendas/deleteitems'] = array('admin/TiendasController', 'deleteitem');

// Categorias Tiendas
$route['*']['/admin/ctgtienda'] = array('admin/CategoriaTiendasController', 'index');
$route['*']['/admin/ctgtienda/add'] = array('admin/CategoriaTiendasController', 'add');
$route['*']['/admin/ctgtienda/save'] = array('admin/CategoriaTiendasController', 'save');
$route['*']['/admin/ctgtienda/edit/:pindex'] = array('admin/CategoriaTiendasController', 'edit');
$route['*']['/admin/ctgtienda/delete/:pindex'] = array('admin/CategoriaTiendasController', 'deactivate');
$route['*']['/admin/ctgtienda/verimagen'] = array('admin/CategoriaTiendasController', 'verimagen');
$route['*']['/admin/ctgtienda/validar'] = array('admin/CategoriaTiendasController', 'validar');
$route['*']['/admin/ctgtienda/page/:filter/:texto/:number'] = array('admin/CategoriaTiendasController', 'index');

// Locales
$route['*']['/admin/locales'] = array('admin/LocalesController', 'index');
$route['*']['/admin/locales/add'] = array('admin/LocalesController', 'add');
$route['*']['/admin/locales/save'] = array('admin/LocalesController', 'save');
$route['*']['/admin/locales/edit/:pindex'] = array('admin/LocalesController', 'edit');
$route['*']['/admin/locales/importar'] = array('admin/LocalesController', 'import');
$route['*']['/admin/locales/upload'] = array('admin/LocalesController', 'upload');
$route['*']['/admin/locales/delete/:pindex'] = array('admin/LocalesController', 'deactivate');
$route['*']['/admin/locales/verimagen'] = array('admin/LocalesController', 'verimagen');
$route['*']['/admin/locales/validar'] = array('admin/LocalesController', 'validar');
$route['*']['/admin/locales/page/:filter/:texto/:number'] = array('admin/LocalesController', 'index');


//Direcciones
$route['*']['/admin/direcciones'] = array('admin/DireccionesController', 'index');
$route['*']['/admin/direcciones/add'] = array('admin/DireccionesController', 'add');
$route['*']['/admin/direcciones/save'] = array('admin/DireccionesController', 'save');
$route['*']['/admin/direcciones/edit'] = array('admin/DireccionesController', 'edit');
$route['*']['/admin/direcciones/validar'] = array('admin/DireccionesController', 'validar');
$route['*']['/admin/direcciones/delete'] = array('admin/DireccionesController', 'delete');
$route['*']['/admin/direcciones/page/:filter/:texto/:number'] = array('admin/DireccionesController', 'index');

//Promociones
$route['*']['/admin/promociones'] = array('admin/PromocionesController', 'index');
$route['*']['/admin/promociones/add'] = array('admin/PromocionesController', 'add');
$route['*']['/admin/promociones/save'] = array('admin/PromocionesController', 'save');
$route['*']['/admin/promociones/edit/:pindex'] = array('admin/PromocionesController', 'edit');
$route['*']['/admin/promociones/validar'] = array('admin/PromocionesController', 'validar');
$route['*']['/admin/promociones/delete'] = array('admin/PromocionesController', 'delete');
$route['*']['/admin/promociones/page/:filter/:texto/:number'] = array('admin/PromocionesController', 'index');

//Configuracion
$route['*']['/admin/configuracion'] = array('admin/ConfiguracionController', 'index');
$route['*']['/admin/configuracion/add'] = array('admin/ConfiguracionController', 'add');
$route['*']['/admin/configuracion/save'] = array('admin/ConfiguracionController', 'save');
$route['*']['/admin/configuracion/edit/:pindex'] = array('admin/ConfiguracionController', 'edit');
$route['*']['/admin/configuracion/validar'] = array('admin/ConfiguracionController', 'validar');
$route['*']['/admin/configuracion/delete'] = array('admin/ConfiguracionController', 'delete');
$route['*']['/admin/configuracion/page/:filter/:texto/:number'] = array('admin/ConfiguracionController', 'index');


//Servicios
$route['*']['/admin/servicios'] = array('admin/ServiciosController', 'index');
$route['*']['/admin/servicios/add'] = array('admin/ServiciosController', 'add');
$route['*']['/admin/servicios/edit/:pindex'] = array('admin/ServiciosController', 'edit');
$route['*']['/admin/servicios/save'] = array('admin/ServiciosController', 'save');
$route['*']['/admin/servicios/delete/:pindex'] = array('admin/ServiciosController', 'deactivate');
$route['*']['/admin/servicios/validar'] = array('admin/ServiciosController', 'validar');



?>
