<?php

//rules for create Post form
return array(
        'telefono' => array(
           array( 'required', 'El campo teléfono es requerido' )
        ),
    'nombre' => array(
           array( 'required', 'El campo Nombre es requerido' )
        ),
    'apellidos' => array(
           array( 'required', 'El campo Apellidos es requerido' )
        ),
    'usuario' => array(
           array( 'required', 'El campo Usuario es requerido' )
        ),
    'password' => array(
           array( 'required', 'El campo Contraseña es requerido' )
        ),
    'rpassword' => array(
           array( 'required', 'El campo Repetir Contraseña es requerido' )
        ),
        'email' => array(
           array('email','El campo correo electrónico no es un email valido'),
           array('required','El campo correo electrónico es requerido')
        ),

    'residencia' => array(
           array( 'required', 'El campo Residencia  es requerido' )
        ),
    
     'sexo' => array(
           array( 'required', 'El campo Sexo  es requerido' )
        ),
    
    'fechaN' => array(
           array('required','El campo Fecha de Nacimiento es requerido')
        )
    );
?>