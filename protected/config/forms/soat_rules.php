<?php

//rules for create Post form
return array(
        'ciudad' => array(
           array( 'required', 'El campo ciudad es requerido' ),
        ),
        'telefono' => array(
           array( 'required', 'El campo teléfono es requerido' )
        ),
        'email' => array(
           array('email','El campo correo electrónico no es un email valido'),
           array('required','El campo correo electrónico es requerido')
        ),

    'titulo' => array(
           array('required','El campo Titulo es requerido')
        ),
    'nombre' => array(
           array('required','El campo Nombre es requerido')
        ),
    'identificacion' => array(
           array('required','El campo Identificacion es requerido')
        ),
    'celular' => array(
           array('required','El campo Celular es requerido')
        ),
    'placa' => array(
           array('required','El campo Placa es requerido')
        ),
    'fecha' => array(
           array('required','El campo Fecha de Vencimiento es requerido')
        )
    );
?>