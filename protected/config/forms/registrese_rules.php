<?php

//rules for create Post form
return array(
        'nombre' => array(
           array( 'required', 'El campo nombre es requerido.' )
        ),

        'direccion' => array(
           array( 'required', 'El campo direccion es requerido' ),
        ),

        'telefono' => array(
           array( 'required', 'El campo teléfono es requerido' )
        ),
        
        'identificacion' => array (
            array ('required', 'El Campo Identificacion es requerido')
        ),
    
        'apellidos' => array(
            array ('required', 'El Campo Apellidos es requerido')
        ),
    
        'nickname' => array(
            array ('required', 'El Campo Nickname es requerido')
        ),
    
        'tipo_identificacion' => array(
            array ('required', 'El Campo Tipo Identificacion es requerido')
        ),

        'email' => array(
           array('email','El campo correo electrónico no es un email valido'),
           array('required','El campo correo electrónico es requerido')
        )
    );
?>