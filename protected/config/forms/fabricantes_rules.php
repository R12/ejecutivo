<?php

//rules for create Post form
return array(
               
        'nombre' => array(
           array( 'required', 'El campo nombre es requerido.' )
        ),

        'url' => array(
           array('required', 'El campo url es requerido' ),
           array('url', 'La url digitada es invalida') 
        ),
    
        'url1' => array(
           array( 'optional' ),
           array('url', 'La url digitada es invalida') 
        )      
    );
?>