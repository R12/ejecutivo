<?php

//rules for create Post form
return array(
    
        'codigo' => array(
           array( 'required', 'El campo codigo es requerido.' )
        ),
    
        'nombre' => array(
           array( 'required', 'El campo nombre es requerido.' )
        ),

        'descripcion' => array(
           array( 'required', 'El campo descripcion es requerido' ),
        ),
    
        'url' => array(
           array( 'optional' ),
           array('url', 'La url digitada es invalida') 
        )      
    );
?>