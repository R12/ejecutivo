<?php
$dbmap['Solicitudes']['has_many']['ItemsSolicitudes'] = array("foreign_key"=>"id_solicitud");
$dbmap['ItemsSolicitudes']['belongs_to']['Solicitudes'] = array("foreign_key"=>"id");

$dbmap['Despachos']['has_many']['ItemsDespachos'] = array("foreign_key"=>"id_despacho");
$dbmap['ItemsDespachos']['belongs_to']['Despachos'] = array("foreign_key"=>"id");

$dbmap['Roles']['has_many']['RolesOpciones'] = array("foreign_key"=>"role_id");
$dbmap['RolesOpciones']['belongs_to']['Roles'] = array("foreign_key"=>"id");

$dbconfig['prod'] = array('localhost', 'ejecutivosdb', 'root', '1425', 'mysql', false, 'collate'=>'utf8_unicode_ci', 'charset'=>'utf8');
$dbconfig['dev'] = array('localhost', 'ejecutivosdb', 'root', '1425','mysql', false, 'collate'=>'utf8_unicode_ci', 'charset'=>'utf8');

?>
