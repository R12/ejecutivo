<?php

/**
 * Description of Entrada
 *
 * @author Manuel Cuevas
 */
class Solicitud extends FPDF {

    private $user = "";

    public function __construct() {
        parent::__construct('P', 'mm', 'Letter');
    }

    function Body($id) {

        $sql = "SELECT informe,codigo,emision,version,nota FROM informes WHERE id = '8'";
        $informe = Doo::db()->query($sql)->fetchAll();
        $datos=$informe[0];
        $this->SetFont('Arial', 'B',15);
        $this->Cell(196,5, utf8_decode($datos['informe']),0,0,'C');
        $this->Ln(12);
        $this->SetFont('Arial','B',8);
        $solicitud = Doo::db()->query("SELECT CONCAT(c.nombre,' ',c.apellidos) AS cliente FROM solicitudes s INNER JOIN clientes c ON s.id_cliente=c.id WHERE s.id='$id'")->fetchAll();
        $this->Cell(60,5,  utf8_decode('SEÑORES'), 0, 1, 'L');
        $this->Cell(60,5,  utf8_decode($solicitud[0]['cliente']), 0, 1, 'L');
        $this->Cell(60,5,  utf8_decode('SELECCIÓN ABREVIADA – SUBASTA INVERSA _012  DE 2012'), 0, 1, 'L');
        
        $this->Ln(6);
        
        $this->Cell(10,5,   utf8_decode('ITEMS'), 1, 0, 'C');
        $this->Cell(54,5,   utf8_decode('DESCRIPCION'),1,0, 'C');
        $this->Cell(22,5,   utf8_decode('CANTIDAD'),1,0, 'C');
        $this->Cell(22,5,   utf8_decode ('Valor sin iva'),1,0, 'C');
        $this->Cell(22,5,   utf8_decode('%IVA'),1,0, 'C');
        $this->Cell(22,5,   utf8_decode('Valor del iva'),1,0, 'C');
        $this->Cell(22,5,   utf8_decode('Valor U. iva'),1,0, 'C');
        $this->Cell(22,5,   utf8_decode('TOTAL'),1,0, 'C');
        $this->Ln(5);
        $this->SetFont('Arial','',8);
        $solicitud = Doo::db()->query("SELECT descripcion,cantidad,valor,iva,((valor*iva)*cantidad) AS val_iva,(valor*(1+iva))iva_inc,(cantidad*valor) AS total FROM items_solicitudes WHERE id_solicitud='$id'")->fetchAll();
        $i=0;
        $svalor=0;
        $sv_iva=0;
        foreach ($solicitud as $sl){
            $i++;
            $this->Cell(10,5,  $i,1,0, 'C');
            $this->Cell(54,5,  utf8_decode($sl['descripcion']),1,0, 'L');
            $this->Cell(22,5, number_format($sl['cantidad'],2),1,0, 'R');
            $valor=$sl['valor'];
            $v_iva=$sl['val_iva'];
            
            $total=$sl['total'];
            $svalor=$svalor+$total;
            
            $sv_iva=$sv_iva+$v_iva;
            $this->Cell(22,5,  '$'.number_format($valor,2),1,0, 'R');
            $this->Cell(22,5,  number_format($sl['iva']*100,2).'%',1,0, 'R');
            $this->Cell(22,5,  '$'.number_format($v_iva,2),1,0, 'R');
            $this->Cell(22,5,  '$'.number_format($sl['iva_inc'],2),1,0, 'R');
            $this->Cell(22,5,  '$'.number_format($total,2),1,0, 'R');
            $this->Ln(5);
        }
        $this->SetFont('Arial','B',8);
        $this->Cell(174,5,   utf8_decode('VALOR TOTAL ANTES DE IVA'),1,0, 'R');
        $this->SetFont('Arial','',8);
        $this->Cell(22,5,'$'.number_format($svalor,2),1,1, 'R');
        $this->SetFont('Arial','B',8);
        $this->Cell(174,5,   utf8_decode('VALOR DEL IVA'),1,0, 'R');
        $this->SetFont('Arial','',8);
        $this->Cell(22,5,'$'.number_format($sv_iva,2),1,1, 'R');
        $this->SetFont('Arial','B',8);
        $this->Cell(174,5,   utf8_decode('VALOR TOTAL IVA INCLUIDO'),1,0, 'R');
        $this->SetFont('Arial','',8);
        $this->Cell(22,5,'$'.number_format($svalor+$sv_iva,2),1,1, 'R');
        $this->Ln(6);
        $this->MultiCell(180, 2, utf8_decode('TIEMPO DE ENTREGA: 24 HORAS HABILES DE ACUERDO A LA SOLICITUD DEL CLIENTE'));
        $this->Ln(3);
        $this->MultiCell(180, 2, utf8_decode('URGENCIAS: 12 HORAS HABILES DESPUES DE SOLICITADO EL PEDIDO'));
        $this->Ln(6);
        $this->MultiCell(196, 4,utf8_decode('(a) Este será el valor tenido en cuenta para las ofertas iniciales de precio al momento de la apertura de las propuestas económicas, pero en todo caso el valor total con IVA incluido, no podrá superar el valor del presupuesto oficial. Nota: El precio o precios ofertados deberán incluir todos los costos relacionados con legalización, impuestos, administración, variaciones monetarias, derechos laborales y prestaciones sociales, pérdidas, y corresponderá dentro del área comercial del oferente cerciorarse y calcular todas estas variables, por lo tanto el ITM no será responsable por reclamaciones que tengan como causa los elementos antes descritos. Los precios ofrecidos serán fijos no reajustables y deberán  ser en cifras enteras sin decimales, en moneda colombiana Para la verificación de la menor oferta inicial de precio, se tomará la relación entre los precios unitarios y los totales por medio de operaciones aritméticas, en caso de encontrarse diferencia, se tomará como base el precio unitario y con base en este se corregirá y calculará el valor total de la oferta, el cual será dado a conocer a los participantes en la subasta. El proponente deberá anexar un archivo de Excel con la información de los valores ofrecidos en el sobre de propuesta económica. Nota: Los proponentes deberán cotizar la totalidad de los ítems, so pena de incurrir en rechazo de la propuesta. '),0,'J');
       $this->Line($this->GetX(), $this->GetY()+30,$this->GetX()+50, $this->GetY()+30);
       $this->Line($this->GetX()+146, $this->GetY()+30,$this->GetX()+196, $this->GetY()+30);

       
       $this->SetY($this->GetY()+26);
       $this->Cell(50,4,'SISCAD S.A.S',0,0, 'C');
       $this->SetY($this->GetY()+5);
       $this->Cell(50,4,'NOMBRE DEL PROPONENTE',0,0, 'C');
       
       $this->SetX($this->GetX()+96);
       $this->Cell(50,4,'FIRMA DEL REPRESENTANTE LEGAL O',0,0, 'C');
       $this->Ln(3);
       $this->Cell(50,4,'CC. o NIT 806,005,495-9',0,0, 'C');
        $this->SetX($this->GetX()+96);
       $this->Cell(50,4,'PERSONA NATURAL',0,0, 'C');
    }

    function Footer() {
        //Posición: a 1,5 cm del final
//        $this->SetY(-15);
//        $this->SetFont('Arial', '',7);
//        $this->Cell(200,4,  utf8_decode('Oficina San Fernando, calle 31 N° 81 - 116, Teléfono: 6618205 - 60018113 - 6618112, Fax: 6618205, A.A. 2987'), 0, 0, 'C');
//        $this->Ln(5);
//        $this->Cell(200, 1,  utf8_decode('E-mail: herreraduranltda@hotmail.com NIT: 890.404.029-7, Cartagena de Indias - Colombia'), 0, 0, 'C');
        //Arial italic 8
//        $this->SetFont('Arial', '', 8);
        //Número de página
//        $this->Cell(150, 1, 'Usuario: ' . $this->getUser(), 0, 0, 'L');
//        $this->Cell(40, 1, 'Pagina ' . $this->PageNo() . ' de {nb}', 0, 0, 'R');
    }

    public function setUser($usr) {
        $this->user = $usr;
    }

    public function getUser() {
        return $this->user;
    }

}

?>
