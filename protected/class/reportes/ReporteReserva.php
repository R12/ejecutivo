<?php

/**
 * Description of Entrada
 *
 * @author web
 */
include "Barcode39.php"; 
class ReporteReserva extends FPDF {


    
    
    public function __construct() {
        parent::__construct('P', 'mm', 'Letter');
    }

    function Body($id) {
        $this->SetTitle(utf8_decode("Reporte de Reservas"));
        Doo::db()->query("SET lc_time_names = 'es_ES';");
        $sql = "SELECT e.nombre AS evento, e.aforo,r.nombre,r.email,r.telefonos,r.cantidad_persona,DATE_FORMAT(r.dia_reserva,'%M %d %Y ') AS dia_reserva,r.hora
        FROM reservas r
        INNER JOIN eventos e 
        ON (r.id_evento=e.id)
        WHERE r.id_evento = '$id'";
        $informe = Doo::db()->query($sql)->fetchAll();
        
        $evento = $informe[0]['evento'];
        $aforo = $informe[0]['aforo'];
        
        $this->SetFont('Arial', 'B',10);
        
         $this->Cell(200,5,utf8_decode($evento),0,0,'C');
        
        $this->Ln(15);
        $this->SetFont('Arial', 'B',8);
        $this->Cell(50,5,utf8_decode("Nombre"),1,0,'C');
        $this->Cell(40,5,utf8_decode("Email"),1,0,'C');
        $this->Cell(30,5,utf8_decode("Telefono"),1,0,'C');
        $this->Cell(25,5,utf8_decode("N° Persona"),1,0,'C');
        $this->Cell(30,5,utf8_decode("Dia Reserva"),1,0,'C');
        $this->Cell(25,5,utf8_decode("Hora Reserva"),1,0,'C');
        $this->ln(5);
        $nreserva = 0;
        // set object

        foreach ($informe as $i){
            $this->SetFont('Arial', '',8);
            $this->cell(50,5,  $i['nombre'],1,0,'L');
            $this->cell(40,5,  $i['email'],1,0,'L');
            $this->cell(30,5,  $i['telefonos'],1,0,'L');
            $this->cell(25,5,  $i['cantidad_persona'],1,0,'L');
            $this->cell(30,5,  $i['dia_reserva'],1,0,'L');
            $this->cell(25,5,  $i['hora'],1,0,'L');
            $this->ln(5);   
            $nreserva+=$i['cantidad_persona'];
        }
        
        $disponible =  ($aforo-$nreserva);
        
        $this->Ln(10);
        $this->SetFont('Arial', 'B',10);
        $this->Cell(190,5,utf8_decode("Total Aforo:"),0,0,'R');
        
        $this->SetTextColor(0,0,0);
        $this->Cell(10,5,utf8_decode($aforo),0,0,'R','true');
        $this->Ln(5);
        
        $this->SetTextColor(0,0,0);
        $this->Cell(190,5,utf8_decode("Total Reservado:"),0,0,'R');
        $this->SetTextColor(255,0,0);
        $this->Cell(10,5,utf8_decode($nreserva),0,0,'R');
        $this->Ln(5);
        
        $this->SetTextColor(0,0,0);
        $this->Cell(190,5,utf8_decode("Disponible:"),0,0,'R');
        $this->SetTextColor(0,201,83);
        $this->Cell(10,5,utf8_decode($disponible),0,0,'R');

        
        
        $this->SetFont('Arial', 'B',15);
        //$this->Cell(196,5, utf8_decode($datos['informe']),0,0,'C');
        $this->Ln(12);
   
    }

    function Footer() {
        //Posición: a 1,5 cm del final
//        $this->SetY(-15);
//        $this->SetFont('Arial', '',7);
//        $this->Cell(200,4,  utf8_decode('Oficina San Fernando, calle 31 N° 81 - 116, Teléfono: 6618205 - 60018113 - 6618112, Fax: 6618205, A.A. 2987'), 0, 0, 'C');
//        $this->Ln(5);
//        $this->Cell(200, 1,  utf8_decode('E-mail: herreraduranltda@hotmail.com NIT: 890.404.029-7, Cartagena de Indias - Colombia'), 0, 0, 'C');
        //Arial italic 8
//        $this->SetFont('Arial', '', 8);
        //Número de página
//        $this->Cell(150, 1, 'Usuario: ' . $this->getUser(), 0, 0, 'L');
//        $this->Cell(40, 1, 'Pagina ' . $this->PageNo() . ' de {nb}', 0, 0, 'R');
    }


}

?>
