<?php

class OrdenQr extends FPDF {

    public function __construct() {
        parent::__construct('P', 'mm', 'Letter');
        //parent::__construct('L','mm','Letter');
        $this->SetTitle('Reservas');
    }
    
    
    function Body($data) {

        $r = $data["reservas"];
        $this->SetFont('Arial', 'B', 20);
        //232X54
        //$this->Cell(80, 35, $this->Image('global/img/logo-min.jpg', $this->GetX(), $this->GetY(), 80, 35), 1, 0, 'C');
        $this->Cell(160,35,utf8_decode($r["evento"]),0,0,'C');
        $this->Cell(36, 35, $this->Image('global/docs/eventos/qrcodes/QR_' . $r["id"] . '.png', $this->GetX(), $this->GetY(), 36, 35), 0, 0, 'C');
        //$this->Cell(80, 35, $this->Image('global/img/logo.png', $this->GetX(), $this->GetY(), 80, 35), 1, 1, 'C');
        $this->Ln(35);
        
        
        $this->SetFont('Arial', '', 10);
        $this->Cell(65.3, 6, utf8_decode("Asistente:"), "TLR", 'L');
        $this->Cell(65.3, 6, utf8_decode("Fecha:"), "TLR", 'L');
        $this->Cell(65.3, 6, utf8_decode("Hora:"), "TLR", 'L');
        $this->Ln(6);
        $this->SetFont('Arial', 'B', 11);
        $this->Cell(65.3, 6, utf8_decode($r["asistente"]), "LR", 'L');
        $this->Cell(65.3, 6, utf8_decode($r["dia_reserva"]), "LR", 'L');
        $this->Cell(65.3, 6, utf8_decode($r["hora"]), "LR", 'L');
        $this->SetFont('Arial', '', 11);
        $this->Ln(6);
        $this->Cell(65.3, 12, utf8_decode($r["email"]), "LR", 'L');
        $this->Cell(65.3, 6, utf8_decode(""), "LRB", 'L');
        $this->Cell(65.3, 6, utf8_decode(""), "LRB", 'L');
        $this->Ln(6);
        $this->SetFont('Arial', '', 11);
        $this->Cell(65.3, 12, utf8_decode(""), "LR", 'L');
        $this->Cell(65.3, 12, utf8_decode("Tipo de Entrada"), "R", 'L');
        $this->Cell(65.3, 12, utf8_decode("Cantidad Boleta"), "R", 'L');
        $this->Ln(8);
        $this->SetFont('Arial', 'B', 11);
        $this->Cell(65.3, 6, utf8_decode("Dirección:"), 0, 'L');
        $this->Cell(65.3, 12, utf8_decode("Entrada General"), "LR", 'L');
        $this->Cell(65.3, 12, utf8_decode($r["cantidad"]), "R", 'L');
        $this->Ln(4);
        $this->SetFont('Arial', '', 11);
        $this->Cell(65.3, 12, utf8_decode($r["lugar"]), "LR", 'L');
        $this->Ln(6);
        $this->Cell(65.3, 14, utf8_decode($r["direccion"]), "LRB", 'L');
        $this->Cell(65.3, 14, utf8_decode(""), "LRB", 'L');
        $this->Cell(65.3, 14, utf8_decode(""), "LRB", 'L');
        $this->Ln(6);
        $this->SetY(100);
        $this->SetX(140);
        $this->Cell(36, 35, $this->Image('global/docs/eventos/codigobarra/barcode_' . $r["id"] . '.gif', $this->GetX(), $this->GetY(), 67, 20), 0, 0, 'C');
              
    }
}
?>
