<?php
Doo::loadCore('db/DooModel');

class UsuariosBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 10.
     */
    public $usuario;

    /**
     * @var varchar Max length is 40.
     */
    public $nombre;

    /**
     * @var varchar Max length is 40.
     */
    public $password;

    /**
     * @var char Max length is 2.
     */
    public $role;

    /**
     * @var char Max length is 1.
     */
    public $tipo;

    /**
     * @var char Max length is 1.
     */
    public $estado;

    public $_table = 'usuarios';
    public $_primarykey = 'id';
    public $_fields = array('id','usuario','nombre','password','role','tipo','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'usuario' => array(
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 40 ),
                        array( 'notnull' ),
                ),

                'password' => array(
                        array( 'maxlength', 40 ),
                        array( 'notnull' ),
                ),

                'role' => array(
                        array( 'maxlength', 2 ),
                        array( 'notnull' ),
                ),

                'tipo' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'estado' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                )
            );
    }

}