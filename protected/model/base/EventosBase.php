<?php
Doo::loadCore('db/DooModel');

class EventosBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 100.
     */
    public $nombre;

    /**
     * @var varchar Max length is 400.
     */
    public $descripcion;

    /**
     * @var date
     */
    public $fecha;

    /**
     * @var varchar Max length is 100.
     */
    public $foto;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'eventos';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','descripcion','fecha','foto','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 100 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'maxlength', 400 ),
                        array( 'optional' ),
                ),

                'fecha' => array(
                        array( 'date' ),
                        array( 'optional' ),
                ),

                'foto' => array(
                        array( 'maxlength', 100 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}