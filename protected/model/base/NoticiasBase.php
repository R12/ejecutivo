<?php
Doo::loadCore('db/DooModel');

class NoticiasBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $id;

    /**
     * @var varchar Max length is 60.
     */
    public $titulo;

    /**
     * @var text
     */
    public $introduccion;

    /**
     * @var char Max length is 1.
     */
    public $tipo;

    /**
     * @var int Max length is 2.
     */
    public $orden;

    /**
     * @var date
     */
    public $fecha_crec;

    /**
     * @var date
     */
    public $fecha_exp;

    /**
     * @var longblob
     */
    public $contenido;

    /**
     * @var text
     */
    public $revista;

    /**
     * @var varchar Max length is 30.
     */
    public $icon;

    /**
     * @var varchar Max length is 30.
     */
    public $color_icon;

    /**
     * @var longblob
     */
    public $imagen;

    /**
     * @var varchar Max length is 30.
     */
    public $id_usuario;

    public $_table = 'noticias';
    public $_primarykey = 'id';
    public $_fields = array('id','titulo','introduccion','tipo','orden','fecha_crec','fecha_exp','contenido','revista','icon','color_icon','imagen','id_usuario');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'titulo' => array(
                        array( 'maxlength', 60 ),
                        array( 'notnull' ),
                ),

                'introduccion' => array(
                        array( 'optional' ),
                ),

                'tipo' => array(
                        array( 'maxlength', 1 ),
                        array( 'optional' ),
                ),

                'orden' => array(
                        array( 'integer' ),
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                ),

                'fecha_crec' => array(
                        array( 'date' ),
                        array( 'optional' ),
                ),

                'fecha_exp' => array(
                        array( 'date' ),
                        array( 'optional' ),
                ),

                'contenido' => array(
                        array( 'optional' ),
                ),

                'revista' => array(
                        array( 'optional' ),
                ),

                'icon' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'color_icon' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'imagen' => array(
                        array( 'optional' ),
                ),

                'id_usuario' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                )
            );
    }

}