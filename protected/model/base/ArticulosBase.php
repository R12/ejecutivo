<?php
Doo::loadCore('db/DooModel');

class ArticulosBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 100.
     */
    public $titulo;

    /**
     * @var text
     */
    public $intro;

    /**
     * @var char Max length is 10.
     */
    public $fecha;

    /**
     * @var text
     */
    public $descripcion;

    /**
     * @var char Max length is 1.
     */
    public $publicado;

    /**
     * @var char Max length is 1.
     */
    public $destacada;

    /**
     * @var char Max length is 1.
     */
    public $categoria;

    /**
     * @var varchar Max length is 100.
     */
    public $imagen;

    public $_table = 'articulos';
    public $_primarykey = 'id';
    public $_fields = array('id','titulo','intro','fecha','descripcion','publicado','destacada','categoria','imagen');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'titulo' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                ),

                'intro' => array(
                        array( 'notnull' ),
                ),

                'fecha' => array(
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'descripcion' => array(
                        array( 'notnull' ),
                ),

                'publicado' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'destacada' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'categoria' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'imagen' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                )
            );
    }

}