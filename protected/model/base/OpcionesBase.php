<?php
Doo::loadCore('db/DooModel');

class OpcionesBase extends DooModel{

    /**
     * @var varchar Max length is 15.
     */
    public $codigo;

    /**
     * @var varchar Max length is 180.
     */
    public $menuitem;

    /**
     * @var varchar Max length is 15.
     */
    public $depende;

    /**
     * @var char Max length is 3.
     */
    public $submenu;

    /**
     * @var varchar Max length is 300.
     */
    public $url;

    /**
     * @var varchar Max length is 300.
     */
    public $toolbar;

    /**
     * @var int Max length is 10.
     */
    public $orden;

    public $_table = 'opciones';
    public $_primarykey = 'codigo';
    public $_fields = array('codigo','menuitem','depende','submenu','url','toolbar','orden');

    public function getVRules() {
        return array(
                'codigo' => array(
                        array( 'maxlength', 15 ),
                        array( 'notnull' ),
                ),

                'menuitem' => array(
                        array( 'maxlength', 180 ),
                        array( 'notnull' ),
                ),

                'depende' => array(
                        array( 'maxlength', 15 ),
                        array( 'notnull' ),
                ),

                'submenu' => array(
                        array( 'maxlength', 3 ),
                        array( 'notnull' ),
                ),

                'url' => array(
                        array( 'maxlength', 300 ),
                        array( 'notnull' ),
                ),

                'toolbar' => array(
                        array( 'maxlength', 300 ),
                        array( 'notnull' ),
                ),

                'orden' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                )
            );
    }

}