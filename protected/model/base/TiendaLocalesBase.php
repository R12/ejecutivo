<?php
Doo::loadCore('db/DooModel');

class TiendaLocalesBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.
     */
    public $id_tienda;

    /**
     * @var int Max length is 10.
     */
    public $id_local;

    public $_table = 'tienda_locales';
    public $_primarykey = 'id';
    public $_fields = array('id','id_tienda','id_local');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'id_tienda' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'id_local' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                )
            );
    }

}