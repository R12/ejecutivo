<?php
Doo::loadCore('db/DooModel');

class ConfiguracionBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 50.
     */
    public $nombre_empresa;

    /**
     * @var varchar Max length is 60.
     */
    public $telefono_empresa;

    /**
     * @var varchar Max length is 60.
     */
    public $cel_empresa;

    /**
     * @var varchar Max length is 60.
     */
    public $email_empresa;

    /**
     * @var varchar Max length is 60.
     */
    public $direccion_empresa;

    /**
     * @var varchar Max length is 50.
     */
    public $ciudad_empresa;

    /**
     * @var text
     */
    public $mapa;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'configuracion';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre_empresa','telefono_empresa','cel_empresa','email_empresa','direccion_empresa','ciudad_empresa','mapa','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre_empresa' => array(
                        array( 'maxlength', 50 ),
                        array( 'optional' ),
                ),

                'telefono_empresa' => array(
                        array( 'maxlength', 60 ),
                        array( 'optional' ),
                ),

                'cel_empresa' => array(
                        array( 'maxlength', 60 ),
                        array( 'optional' ),
                ),

                'email_empresa' => array(
                        array( 'maxlength', 60 ),
                        array( 'optional' ),
                ),

                'direccion_empresa' => array(
                        array( 'maxlength', 60 ),
                        array( 'optional' ),
                ),

                'ciudad_empresa' => array(
                        array( 'maxlength', 50 ),
                        array( 'optional' ),
                ),

                'mapa' => array(
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}