<?php
Doo::loadCore('db/DooModel');

class PromocionesBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 20.
     */
    public $nombre;

    /**
     * @var varchar Max length is 500.
     */
    public $descripcion;

    /**
     * @var varchar Max length is 30.
     */
    public $foto;

    /**
     * @var int Max length is 10.
     */
    public $id_tienda;

    /**
     * @var date
     */
    public $fecha_inicio;

    /**
     * @var date
     */
    public $fecha_fin;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'promociones';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','descripcion','foto','id_tienda','fecha_inicio','fecha_fin','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 20 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'maxlength', 500 ),
                        array( 'optional' ),
                ),

                'foto' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'id_tienda' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'fecha_inicio' => array(
                        array( 'date' ),
                        array( 'optional' ),
                ),

                'fecha_fin' => array(
                        array( 'date' ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}