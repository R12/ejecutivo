<?php
Doo::loadCore('db/DooModel');

class CategoriasTiendasBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 40.
     */
    public $nombre;

    /**
     * @var varchar Max length is 100.
     */
    public $descripcion;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'categorias_tiendas';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','descripcion','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 40 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'maxlength', 100 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}