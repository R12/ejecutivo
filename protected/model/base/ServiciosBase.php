<?php
Doo::loadCore('db/DooModel');

class ServiciosBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 30.
     */
    public $url;

    /**
     * @var varchar Max length is 30.
     */
    public $nombre;

    /**
     * @var text
     */
    public $descripcion;

    /**
     * @var varchar Max length is 30.
     */
    public $imagen;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'servicios';
    public $_primarykey = 'id';
    public $_fields = array('id','url','nombre','descripcion','imagen','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'url' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'optional' ),
                ),

                'imagen' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}