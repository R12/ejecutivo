<?php
Doo::loadCore('db/DooModel');

class RolesOpcionesBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $role_id;

    /**
     * @var varchar Max length is 15.
     */
    public $opcion;

    /**
     * @var char Max length is 3.
     */
    public $acceso;

    public $_table = 'roles_opciones';
    public $_primarykey = 'opcion';
    public $_fields = array('role_id','opcion','acceso');

    public function getVRules() {
        return array(
                'role_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'opcion' => array(
                        array( 'maxlength', 15 ),
                        array( 'notnull' ),
                ),

                'acceso' => array(
                        array( 'maxlength', 3 ),
                        array( 'notnull' ),
                )
            );
    }

}