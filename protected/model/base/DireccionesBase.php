<?php
Doo::loadCore('db/DooModel');

class DireccionesBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.
     */
    public $alto;

    /**
     * @var int Max length is 10.
     */
    public $dependencia;

    /**
     * @var char Max length is 1.
     */
    public $submenu;

    /**
     * @var varchar Max length is 200.
     */
    public $etiqueta;

    /**
     * @var varchar Max length is 100.
     */
    public $url;

    /**
     * @var char Max length is 1.
     */
    public $istop;

    /**
     * @var char Max length is 1.
     */
    public $isfotter;

    /**
     * @var char Max length is 1.
     */
    public $iscolunm;

    /**
     * @var int Max length is 1.
     */
    public $posicionf;

    /**
     * @var int Max length is 1.
     */
    public $posicionc;

    /**
     * @var int Max length is 10.
     */
    public $orderfotter;

    /**
     * @var int Max length is 10.
     */
    public $ordercolumn;

    /**
     * @var int Max length is 10.
     */
    public $ordercolumn2;

    /**
     * @var char Max length is 1.
     */
    public $destino;

    public $_table = 'direcciones';
    public $_primarykey = 'id';
    public $_fields = array('id','alto','dependencia','submenu','etiqueta','url','istop','isfotter','iscolunm','posicionf','posicionc','orderfotter','ordercolumn','ordercolumn2','destino');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'alto' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'dependencia' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'submenu' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'etiqueta' => array(
                        array( 'maxlength', 200 ),
                        array( 'notnull' ),
                ),

                'url' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                ),

                'istop' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'isfotter' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'iscolunm' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'posicionf' => array(
                        array( 'integer' ),
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'posicionc' => array(
                        array( 'integer' ),
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                ),

                'orderfotter' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'ordercolumn' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'ordercolumn2' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'notnull' ),
                ),

                'destino' => array(
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                )
            );
    }

}