<?php
Doo::loadCore('db/DooModel');

class FotosEventosBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 30.
     */
    public $nombre;

    /**
     * @var varchar Max length is 30.
     */
    public $foto;

    /**
     * @var varchar Max length is 300.
     */
    public $descripcion;

    /**
     * @var int Max length is 10.
     */
    public $id_evento;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'fotos_eventos';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','foto','descripcion','id_evento','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'foto' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'maxlength', 300 ),
                        array( 'optional' ),
                ),

                'id_evento' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}