<?php
Doo::loadCore('db/DooModel');

class TiendasBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 30.
     */
    public $nombre;

    /**
     * @var text
     */
    public $descripcion;

    /**
     * @var varchar Max length is 50.
     */
    public $link;

    /**
     * @var int Max length is 3.
     */
    public $orden;

    /**
     * @var varchar Max length is 30.
     */
    public $foto;

    /**
     * @var int Max length is 10.
     */
    public $id_categoria;

    /**
     * @var int Max length is 10.
     */
    public $id_local;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'tiendas';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','descripcion','link','orden','foto','id_categoria','id_local','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'optional' ),
                ),

                'link' => array(
                        array( 'maxlength', 50 ),
                        array( 'optional' ),
                ),

                'orden' => array(
                        array( 'integer' ),
                        array( 'maxlength', 3 ),
                        array( 'optional' ),
                ),

                'foto' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'id_categoria' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'id_local' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}