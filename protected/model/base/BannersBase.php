<?php
Doo::loadCore('db/DooModel');

class BannersBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $id;

    /**
     * @var varchar Max length is 60.
     */
    public $nombre;

    /**
     * @var int Max length is 10.
     */
    public $pagina;

    /**
     * @var varchar Max length is 200.
     */
    public $nombreimagen;

    /**
     * @var varchar Max length is 30.
     */
    public $titulo;

    /**
     * @var varchar Max length is 800.
     */
    public $descripcion;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'banners';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','pagina','nombreimagen','titulo','descripcion','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 60 ),
                        array( 'notnull' ),
                ),

                'pagina' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombreimagen' => array(
                        array( 'maxlength', 200 ),
                        array( 'notnull' ),
                ),

                'titulo' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'maxlength', 800 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}