<?php
Doo::loadCore('db/DooModel');

class PagesBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 20.
     */
    public $page;

    /**
     * @var varchar Max length is 100.
     */
    public $titulo;

    /**
     * @var varchar Max length is 100.
     */
    public $titulo2;

    /**
     * @var varchar Max length is 10.
     */
    public $align;

    /**
     * @var varchar Max length is 30.
     */
    public $foto;

    /**
     * @var varchar Max length is 30.
     */
    public $background;

    /**
     * @var longblob
     */
    public $descripcion;

    /**
     * @var longblob
     */
    public $english;

    public $_table = 'pages';
    public $_primarykey = 'id';
    public $_fields = array('id','page','titulo','titulo2','align','foto','background','descripcion','english');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'page' => array(
                        array( 'maxlength', 20 ),
                        array( 'notnull' ),
                ),

                'titulo' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                ),

                'titulo2' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                ),

                'align' => array(
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'foto' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'background' => array(
                        array( 'maxlength', 30 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'notnull' ),
                ),

                'english' => array(
                        array( 'notnull' ),
                )
            );
    }

}