<?php
Doo::loadCore('db/DooModel');

class ParametrosBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 100.
     */
    public $email;

    public $_table = 'parametros';
    public $_primarykey = 'id';
    public $_fields = array('id','email');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'email' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                )
            );
    }

}