<?php
Doo::loadCore('db/DooModel');

class LocalesBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 80.
     */
    public $nombre;

    /**
     * @var varchar Max length is 100.
     */
    public $almacen;

    /**
     * @var varchar Max length is 500.
     */
    public $descripcion;

    /**
     * @var char Max length is 3.
     */
    public $piso;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'locales';
    public $_primarykey = 'id';
    public $_fields = array('id','nombre','almacen','descripcion','piso','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'nombre' => array(
                        array( 'maxlength', 80 ),
                        array( 'optional' ),
                ),

                'almacen' => array(
                        array( 'maxlength', 100 ),
                        array( 'optional' ),
                ),

                'descripcion' => array(
                        array( 'maxlength', 500 ),
                        array( 'optional' ),
                ),

                'piso' => array(
                        array( 'maxlength', 3 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}