<?php
Doo::loadCore('db/DooModel');

class TiendaCategoriaBase extends DooModel{

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.
     */
    public $id_categoria;

    /**
     * @var int Max length is 10.
     */
    public $id_tienda;

    /**
     * @var char Max length is 2.
     */
    public $estado;

    public $_table = 'tienda_categoria';
    public $_primarykey = 'id';
    public $_fields = array('id','id_categoria','id_tienda','estado');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'id_categoria' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'id_tienda' => array(
                        array( 'integer' ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'estado' => array(
                        array( 'maxlength', 2 ),
                        array( 'optional' ),
                )
            );
    }

}