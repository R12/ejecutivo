<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>404 Error Page | Unify - Responsive Website Template</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="<?php echo Doo::conf()->APP_URL; ?>global/css/style.css">
    <link rel="stylesheet" href="<?php echo Doo::conf()->APP_URL; ?>global/css/pages/page_404_error.css">
</head> 

<body>    

<div class="wrapper">
    <!--=== Content Part ===-->
    <div class="container content">		
        <!--Error Block-->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="error-v1">
                    <span class="error-v1-title">404</span>
                     <span>¡Eso es un error!</span>
                    <p>La URL solicitada no se encuentra en este servidor</p>
                    <a class="btn-u btn-bordered" href="<?php echo Doo::conf()->APP_URL; ?>">Regresar a inicio</a>
                </div>
            </div>
        </div>
        <!--End Error Block-->
    </div>	
    <!--=== End Content Part ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?php echo Doo::conf()->APP_URL; ?>global/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Doo::conf()->APP_URL; ?>global/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?php echo Doo::conf()->APP_URL; ?>global/plugins/bootstrap/js/bootstrap.min.js"></script> 
</body>
</html> 