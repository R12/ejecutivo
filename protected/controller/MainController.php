<?php

/**
 * MainController
 * @author WEB
 */
class MainController extends DooController {

    public $data;

    function getInputINT($key) {
        $val = filter_input(INPUT_GET, $key, FILTER_SANITIZE_NUMBER_INT);
        if (is_null($val)) {
            return false;
        } else if (!$val) {
            return false;
        } else {
            return $val;
        }
    }

    public function index() {
        $this->tiendas();
        $this->banner();
        $this->buildMenu();
        $promocion = Doo::db()->query("SELECT p.id,p.nombre,p.fecha_inicio,p.foto,t.nombre AS tienda 
        FROM promociones p
        INNER JOIN tiendas t
        ON(p.id_tienda=t.id)
        WHERE p.estado = 1 AND p.fecha_fin >= CURDATE() LIMIT 3")->fetchAll();
        $this->data['promocion'] = $promocion;
        $this->data['content'] = 'inicio';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }

    public function Menufooter() {
        $sqlmenufooter = Doo::db()->query("SELECT page,titulo FROM pages")->fetchAll();
        $this->data['menu_footer'] = $sqlmenufooter;
    }

    public function showPageService() {
        $page = $this->params['pagina'];
        Doo::loadModel("Servicios");
        $p = new Servicios();
        $p->url = $page;
        $p = Doo::db()->find($p, array('limit' => 1));
        if ($p == false) {
            return array(Doo::conf()->APP_URL . "/error", 404);
        }
        $this->Menu();
        $this->Menufooter();
        $this->buildMenu();
        $this->tiendas();
        $this->data['titulo'] = $p->nombre;
        $this->data['html'] = $p->descripcion;
        $this->data['imagen'] = $p->imagen;
        $this->data['content'] = 'page_service';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }
    
    public function showPage() {
        $page = $this->params['pagina'];
        Doo::loadModel("Pages");
        $p = new Pages();
        $p->url = $page;
        $p = Doo::db()->find($p, array('limit' => 1));
        if ($p == false) {
            return array(Doo::conf()->APP_URL . "/error", 404);
        }
        $this->Menu();
        $this->Menufooter();
        $this->buildMenu();
        $this->tiendas();
        $this->data['titulo'] = $p->titulo;
        $this->data['html'] = $p->descripcion;
        $this->data['imagen'] = $p->foto;
        $this->data['align'] = $p->align;
        
        $this->data['content'] = 'page';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }
    
    

    public function categorias($id) {
        $ctg = Doo::db()->query("SELECT id,nombre FROM categoria_eventos WHERE estado = 1 AND dependencia = 0")->fetchAll();
        $this->data['categorias'] = $ctg;

        $imctg = Doo::db()->query("SELECT id,nombre,id_usuario,fotopagina FROM eventos WHERE  estado = 3  AND seccion4='S'  AND ciudad = $id ")->fetchAll();
        $this->data['imgcategorias'] = $imctg;
    }

    public function getAllEventos() {
        Doo::db()->query("SET lc_time_names = 'es_ES'");
        $evento = Doo::db()->find('Eventos', array("select" => "id,nombre,DATE_FORMAT(fecha,'%M %d %Y') AS fecha,foto", "where" => "estado=1 ", "ORDER" => "fecha", "asc" => "fecha"));
        $this->data['eventos'] = $evento;
        
        $pages = Doo::db()->query("SELECT * from pages where page='eventos'")->fetchAll();
        $this->data["titulo"]=$pages[0]["titulo"];
        $this->data["descripcion"]=$pages[0]["descripcion"];
        $this->data["foto"]=$pages[0]["foto"];
        $this->data["align"]=$pages[0]["align"];
        $this->data["background"]=$pages[0]["background"];
        
        $this->tiendas();
        $this->buildMenu();
        $this->data['content'] = 'eventos';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }

    public function getTiendas() {
        Doo::db()->query("SET lc_time_names = 'es_ES'");

        $ctgtienda = Doo::db()->find('CategoriasTiendas', array("select" => "id,nombre", "where" => "estado=1 ", "ORDER" => "nombre", "asc" => "nombre"));
        $this->data['categoria'] = $ctgtienda;

        $sql = Doo::db()->query("SELECT t.id,t.nombre,t.link,t.foto, GROUP_CONCAT(c.id_categoria SEPARATOR ' ' ) AS id_categoria
        FROM tiendas t 
        INNER JOIN `tienda_categoria` c
        ON (c.id_tienda=t.`id`)
        WHERE t.estado = 1
        GROUP BY t.id")->fetchAll();
        $this->data["tiendas"] = $sql;
        
        $pages = Doo::db()->query("SELECT * from pages where page='tiendas'")->fetchAll();
        $this->data["titulo"]=$pages[0]["titulo"];
        $this->data["descripcion"]=$pages[0]["descripcion"];
        $this->data["foto"]=$pages[0]["foto"];
        $this->data["align"]=$pages[0]["align"];
        $this->data["background"]=$pages[0]["background"];
        
        $this->buildMenu();
        $this->data['content'] = 'tiendas';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }

    public function getPromociones() {
        $this->promociones();
        $this->tiendas();
        $this->buildMenu();
        
        $pages = Doo::db()->query("SELECT * from pages where page='promocion'")->fetchAll();
        $this->data["titulo"]=$pages[0]["titulo"];
        $this->data["descripcion"]=$pages[0]["descripcion"];
        $this->data["foto"]=$pages[0]["foto"];
        $this->data["align"]=$pages[0]["align"];
        $this->data["background"]=$pages[0]["background"];
        
        $this->data['content'] = 'promocion';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }

    public function getLocales() {
        $this->promociones();
//        $this->tiendas();
        $tiendas = Doo::db()->query("SELECT l.nombre AS LOCAL, t.nombre FROM locales l 
        INNER JOIN tiendas t
        ON (t.`id_local`=l.`id`)
        WHERE t.`estado` = 1 ")->fetchAll();
        $this->data["tienda"] = $tiendas;
        
        $locales = Doo::db()->query("SELECT * from locales WHERE estado = 1 ");
        $this->data["locales"] = $locales;
        
        $this->buildMenu();
        
        $pages = Doo::db()->query("SELECT * from pages where page='locales'")->fetchAll();
        $this->data["titulo"]=$pages[0]["titulo"];
        $this->data["descripcion"]=$pages[0]["descripcion"];
        $this->data["foto"]=$pages[0]["foto"];
        $this->data["align"]=$pages[0]["align"];
        $this->data["background"]=$pages[0]["background"];
        
        $this->data['content'] = 'locales';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }

    public function promociones() {
        Doo::db()->query("SET lc_time_names = 'es_ES'");
        $promocion = Doo::db()->query("SELECT p.id,p.nombre,p.fecha_inicio,p.foto,t.nombre AS tienda 
        FROM promociones p
        INNER JOIN tiendas t
        ON(p.id_tienda=t.id)
        WHERE p.estado = 1 AND p.fecha_fin >= CURDATE()")->fetchAll();
        $this->data['promocion'] = $promocion;
    }

    public function ShowEventos() {
        Doo::db()->query("SET lc_time_names = 'es_ES'");
        $id = $this->getInputINT("id");
        if (!$id) {
            return Doo::conf()->APP_URL;
        } else {

            $evento = Doo::db()->query("SELECT nombre,descripcion,DATE_FORMAT(fecha,'%M %d %Y') AS fecha,foto FROM "
                            . "eventos WHERE id = $id AND  estado = 1")->fetchAll();
            $this->data['nombre'] = $evento[0]["nombre"];
            $this->data['descripcion'] = $evento[0]["descripcion"];
            $this->data['foto'] = $evento[0]["foto"];
            $this->data['fecha'] = $evento[0]["fecha"];

            $ftevento = Doo::db()->query("SELECT f.id,f.nombre,f.foto FROM
        fotos_eventos f
        INNER JOIN eventos e
        ON (f.`id_evento`=e.`id`)
        WHERE e.`id` = $id");
            $this->data['ftevento'] = $ftevento;
            $this->tiendas();
            $this->buildMenu();
            $this->data['content'] = 'individualeventos';
            $this->data['rootUrl'] = Doo::conf()->APP_URL;
            $this->renderc('home', $this->data, true);
        }
    }

    public function ShowPromociones() {
        Doo::db()->query("SET lc_time_names = 'es_ES'");
        $id = $this->getInputINT("id");
        if (!$id) {
            return Doo::conf()->APP_URL;
        } else {

            $promocion = Doo::db()->query("SELECT p.id,p.nombre,p.foto,p.descripcion,
        DATE_FORMAT(p.fecha_fin,'%M %d %Y') AS fecha_fin,t.nombre AS tienda 
        FROM promociones p 
        INNER JOIN tiendas t
        ON (p.id_tienda=t.id)
        WHERE p.id=$id AND p.estado=1 AND p.fecha_fin >= CURDATE()")->fetchAll();
            //$this->data['promocion'] = $promocion;
            if ($promocion == null) {
                $this->data['content'] = '404';
            } else {

                $this->data['nombre'] = $promocion[0]["nombre"];
                $this->data['foto'] = $promocion[0]["foto"];
                $this->data['fecha_fin'] = $promocion[0]["fecha_fin"];
                $this->data['descripcion'] = $promocion[0]["descripcion"];
                $this->data['content'] = 'individualpromocion';
            }
            $this->tiendas();
            $this->buildMenu();

            $this->data['rootUrl'] = Doo::conf()->APP_URL;
            $this->renderc('home', $this->data, true);
        }
    }

    public function banner() {
        $banner = Doo::db()->query("SELECT * FROM banners WHERE estado = 1")->fetchAll();
        $this->data["banner"] = $banner;
    }

    public function tiendas() {
        $tiendas = Doo::db()->query("SELECT nombre,foto FROM tiendas WHERE estado = 1 ")->fetchAll();
        $this->data["tienda"] = $tiendas;
    }

    public function imagen() {
        $param = $this->params['pindex'];
        $id = $_REQUEST['id'];
        $rs = Doo::db()->find($param, array("select" => "imagen",
            "where" => "id = ?",
            "limit" => 1,
            "param" => array($id)));
        $imagen = $rs->imagen;

        if ($imagen == "") {
            echo "";
        } else {
            Header("Content-type: image/jpg ");
            echo $imagen;
        }
    }

    private function buildMenu() {
        $sql = "SELECT id,etiqueta,url,destino,submenu,alto FROM direcciones WHERE istop='S' AND dependencia='0' ORDER BY ordercolumn2";
        $rs = Doo::db()->query($sql);
        $parentMenu = $rs->fetchAll();
        $this->data["htmlmenu"] = '<ul class="nav navbar-nav">';
        $this->buildChildMenu($parentMenu, false);
        $this->data["htmlmenu"].= '</ul>';
    }

    private function buildChildMenu($parentMenu, $sub) {
        foreach ($parentMenu as $row) {
            $id = $row["id"];
            $submenu = $row["submenu"];
            $etiqueta = $row["etiqueta"];
            $alto = $row["alto"];
            $url = $row["url"];
            $destino = $row["destino"];
            if ($submenu == 'S') {
                $this->data["htmlmenu"].= '<li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">' . $etiqueta . '</a>';
                $sql = "SELECT id,etiqueta,url,destino,submenu,alto FROM direcciones WHERE istop='S' AND dependencia='$id' ORDER BY ordercolumn2";
                $rs = Doo::db()->query($sql);
                $childMenu = $rs->fetchAll();
                $this->data["htmlmenu"].= '<ul class="dropdown-menu">';
                $this->buildChildMenu($childMenu, true);
                $this->data["htmlmenu"].= '</ul ></li><li></li>';
                $this->data['scriptactivePrincipal'] = "$('#M$id').addClass('active');";
            } else {
                $this->data["htmlmenu"].= '<li class="page-scroll" id="HL' . $id . '">';
                if ($destino == 'E') {
                    //style="font-size: 10px;' . ($alto > 0 ? 'height:' . $alto . 'px;' : '') . '"
                    $this->data["htmlmenu"].= '<a  target="_black" href="' . $url . '">' . $etiqueta . '</a>';
                } else {
                    $this->data["htmlmenu"].= '<a  href="' . Doo::conf()->APP_URL . $row["url"] . '">' . $etiqueta . '</a>';
                }
                $this->data["htmlmenu"].= '</li><li>';
                if (!$sub) {
                    $this->data["htmlmenu"].= '</li>';
                }
            }
        }
    }

    public function getContacto() {
        $this->tiendas();
        $this->buildMenu();
        $sql = Doo::db()->query("SELECT * FROM configuracion WHERE estado = 1")->fetchAll();
        $this->data["nombre_empresa"] = $sql[0]["nombre_empresa"];
        $this->data["email_empresa"] = $sql[0]["email_empresa"];
        $this->data["telefono_empresa"] = $sql[0]["telefono_empresa"];
        $this->data["direccion_empresa"] = $sql[0]["direccion_empresa"];
        $this->data["ciudad_empresa"] = $sql[0]["ciudad_empresa"];
        $this->data["mapa"] = $sql[0]["mapa"];

        $this->data['content'] = 'contacto';
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->renderc('home', $this->data, true);
    }

    public function send() {
        Doo::loadHelper('DooMailer');
        $mail = new DooMailer();
        $html = "<p>
                  <b>Nombre:</b><span>" . $_POST["nombre"] . "</span><br/>
                  <b>Email:</b><span>" . $_POST["email"] . "</span><br/><br/>
                  <b>Mensaje:</b><span>" . $_POST["mensaje"] . "</span><br/>
                  </p><br/>
                  <footer><i>Correo enviado desde la pagina de CC. Los Ejecutivos</i></footer>";
        $param = $this->db()->find("Parametros", array("select" => "email", "limit" => 1));
        $mail->addTo($param->email);
        $mail->setSubject('Comentarios-Los-Ejecutivos-Web');
        $mail->setBodyHtml($html);
        $mail->setFrom($_POST['email']);
        $mail->send();
        echo true;
    }

}

?>
