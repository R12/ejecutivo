<?php

/**
 * Description of ClientesController
 *
 * @author web.
 */
class FotosEventosController extends DooController {

    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }

        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["22"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

  public function index() {
        Doo::loadHelper('DooPager');
        if (!isset($_POST["filtro"])) {
            if (!isset($this->params['filter'])) {
                $filtro = "fe.id";
            } else {
                $filtro = $this->params['filter'];
            }
        }else{
            $filtro = $_POST["filtro"];
        }

        if (!isset($_POST["texto"])) {
            if (!isset($this->params['texto'])) {
                $texto = "";
            } else {
                $texto = $this->params['texto'];
            }
        }else{
            $texto = $_POST["texto"];
        }

        $where = "fe.estado=1 AND $filtro like '$texto%'";


         $rs = Doo::db()->query("SELECT COUNT(*) AS total FROM fotos_eventos fe INNER JOIN eventos e
               ON (fe.id_evento=e.id) where ".$where);
         $r = $rs->fetchAll();
         $total=$r[0]["total"];
         if ($total == 0){
           $total = 1;
           }
        $pager = new DooPager(Doo::conf()->APP_URL."admin/fotoevento/page/$filtro/$texto", $total, 10, 5);

        if(isset($this->params['number']))
            $pager->paginate(intval($this->params['number']));
        else
        $pager->paginate(1);
        $sql="SELECT fe.id,fe.nombre,e.nombre AS evento,fe.foto  FROM fotos_eventos fe INNER JOIN eventos e
        ON (fe.id_evento=e.id) where  ".$where;
        $this->data['fotoevento'] = Doo::db()->query($sql." ORDER BY fe.id desc LIMIT $pager->limit")->fetchAll();
        $this->data['pager'] = $pager->output;
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'fotos_eventos/list.php';
        $this->data['filtro'] = $filtro;
        $this->data['texto'] = $texto;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("FotosEventos");
        $fotoevento = new FotosEventos();
        $this->data['fotoevento'] = $fotoevento;
        $this->data['evento'] = Doo::db()->find("Eventos",array("select"=>"id,nombre"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'fotos_eventos/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function edit() {
        $id = $this->params["pindex"];
        $fotoevento = Doo::db()->find("FotosEventos", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['fotoevento'] = $fotoevento;
        $this->data['evento'] = Doo::db()->find("Eventos",array("select"=>"id,nombre"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'fotos_eventos/form.php';
        $this->renderc('admin/index', $this->data);
    }


     public function save() {
        Doo::loadModel("FotosEventos");
        Doo::loadHelper('DooGdImage');
        $fotoevento = new FotosEventos($_POST);
        try {
            $imagen = "";

            if ($fotoevento->id == "") {

                $fotoevento->id = Null;
                $fotoevento->nombre = $_POST['nombre'];
                $fotoevento->descripcion = $_POST['descripcion'];
                $fotoevento->id_evento = $_POST['id_evento'];
                $fotoevento->estado = '1';

                $gd = new DooGdImage(Doo::conf()->IMG_EV. 'img_galeria/');
                $type = $gd->getUploadFormat('imagen');
                if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                    $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                } else {
                    $imagen = "";
                    throw new Exception('Formato de la Imagen no Valido!');
                }
                $fotoevento->foto = $imagen;
                Doo::db()->insert($fotoevento);
            } else {

                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_EV. 'img_galeria/');
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                $include1 = "";
                if ($imagen != "") {
                    $include1 = ", foto='$imagen'";
                }
                $sql = "update fotos_eventos set nombre = '$fotoevento->nombre', descripcion = '$fotoevento->descripcion', id_evento = '$fotoevento->id_evento', estado='1' $include1  where id = $fotoevento->id";
                Doo::db()->query($sql);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return Doo::conf()->APP_URL . "admin/fotoevento";
     }
    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE fotos_eventos SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/fotoevento";
    }

    public function validar() {
        $nomb = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from fotos_eventos where nombre = '$nomb' and id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }

}

?>
