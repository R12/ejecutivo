<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class TiendasController extends DooController {

    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["14"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

    public function index() {
        $ctgevento = Doo::db()->query("SELECT t.id,t.nombre AS tienda,t.link, l.nombre AS LOCAL FROM tiendas t "
                . " INNER JOIN locales l ON (t.id_local=l.id) WHERE t.estado=1");
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'tiendas/list.php';
        $this->data['tienda'] = $ctgevento;
        $this->clean();
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Tiendas");
        $tienda = new Tiendas();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['tienda'] = $tienda;
        $this->data['categoria'] = Doo::db()->query("SELECT id,nombre FROM categorias_tiendas WHERE estado=1 ORDER BY nombre")->fetchAll();
        $this->data['locales'] = Doo::db()->query("SELECT id,nombre FROM locales WHERE estado=1 ORDER BY nombre")->fetchAll();
        $this->data['content'] = 'tiendas/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
        Doo::loadModel("Tiendas");
        Doo::loadHelper('DooGdImage');
        $tienda = new Tiendas($_POST);
        try {
            $imagen = "";
            if ($tienda->id == "") {
                $tienda->id = Null;
                $tienda->nombre = filter_input(INPUT_POST, "nombre");
                $tienda->descripcion = filter_input(INPUT_POST, "descripcion");
                $tienda->link = filter_input(INPUT_POST, "link");
                $tienda->orden = filter_input(INPUT_POST, "orden");
                $tienda->id_categoria = filter_input(INPUT_POST, "id_categoria");
                $tienda->id_local = filter_input(INPUT_POST, "id_local");
                $tienda->estado = '1';
                /* Insertando la Imagen Para el Banner */
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_TIENDA . 'logos/');
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                $tienda->foto = $imagen;
                //Doo::db()->insert($tienda);
                $tienda->id = Doo::db()->insert($tienda);
            } else {
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_TIENDA . 'logos/');
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }

                $include1 = "";
                if ($imagen != "") {
                    $include1 = ", foto='$imagen'";
                }
                $sql = "UPDATE tiendas  SET id = '$tienda->id', nombre = '$tienda->nombre', descripcion = '$tienda->descripcion', "
                        . "link = '$tienda->link',orden='$tienda->orden',id_categoria='$tienda->id_categoria',id_local='$tienda->id_local',estado = 1 $include1   WHERE id = $tienda->id";
                Doo::db()->query($sql);
            }

            if (isset($_SESSION["listado_categoria"])) {
                $array = unserialize($_SESSION["listado_categoria"]);
                Doo::loadModel("TiendaCategoria");

                foreach ($array as $a) {
                    if ($a['id'] == '') {
                        $s = new TiendaCategoria();
                        $s->id = null;
                        $s->id_tienda = $tienda->id;
                        $s->id_categoria = $a["id_categoria"];
                        $s->estado = '1';
                        Doo::db()->insert($s);
                    }
//                    else {
//                        $localidad = $a["localidad"];
//                        Doo::db()->query("UPDATE localidades SET nombre=$localidad WHERE id = ?", array($a['id']));
//                    }
                }
            }
            if (isset($_SESSION["listado_categoria_del"])) {
                $itemsBorrar = unserialize($_SESSION["listado_categoria_del"]);
                foreach ($itemsBorrar as $i) {
                    Doo::db()->query("DELETE FROM tienda_categoria  WHERE id  = ?", array($i['id']));

                    print_r($i);
                }
                $_SESSION["listado_categoria_del"] = null;
            }
        } catch (PDOException $e) {
            // echo $e->getMessage();
        }
        $this->clean();
        return Doo::conf()->APP_URL . "admin/tiendas";
    }

    public function edit() {
        $id = $this->params["pindex"];
        $tienda = Doo::db()->find("Tiendas", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['tienda'] = $tienda;
        $this->data['categoria'] = Doo::db()->query("SELECT id,nombre FROM categorias_tiendas WHERE estado=1 ORDER BY nombre")->fetchAll();
        $this->data['locales'] = Doo::db()->query("SELECT id,nombre FROM locales WHERE estado=1 ORDER BY nombre")->fetchAll();


        $items = Doo::db()->query("SELECT tc.*,c.nombre AS nombre_categoria FROM tienda_categoria tc
        INNER JOIN `categorias_tiendas` c 
        ON (tc.id_categoria=c.id)
        WHERE tc.id_tienda = $id AND tc.estado=1 ")->fetchAll();
        $array = array();
        foreach ($items as $i) {
            $id = $i["id"];
            $id_tienda = $i["id_tienda"];
            $id_categoria = $i["id_categoria"];
            $nombre_categoria = $i["nombre_categoria"];
            $array[] = array("id" => $id, "id_tienda" => $id_tienda, "nombre_categoria" => $nombre_categoria , "id_categoria" => $id_categoria);
        }
        $_SESSION["listado_categoria"] = serialize($array);

        $this->data['content'] = 'admin/tiendas/items_categorias';
        $this->data['content'] = 'tiendas/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE tiendas SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/tiendas";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from tiendas where nombre = '$nombre' AND id <> '$id' AND estado = 1")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }

    public function cargar_categoria() {
        if (isset($_SESSION["listado_categoria"])) {
            $array = unserialize($_SESSION["listado_categoria"]);
        } else {
            $array = array();
        }
        $this->data['categorias'] = $array;
        $this->renderc("admin/tiendas/items_categorias", $this->data, true);
    }

    /* Validamos que las categorias no se repitan */

    public function Validarcategorias() {
        $id_categoria = filter_input(INPUT_POST, "id_categoria");
        $r = "OK";
        if (isset($_SESSION["listado_categoria"])) {
            $array = unserialize($_SESSION["listado_categoria"]);
            foreach ($array as $a) {
                if (($a['id_categoria'] == $id_categoria)) {
                    $r = "NO";
                }
            }
        }
        echo $r;
    }

    public function agregar_categoria() {
        if (isset($_SESSION["listado_categoria"])) {
            $array = unserialize($_SESSION["listado_categoria"]);
        } else {
            $array = array();
        }

        $id_categoria = filter_input(INPUT_POST, "id_categoria");
        $nombre_categoria = filter_input(INPUT_POST, "nombre_categoria");
        $array[] = array("id" => '', "id_categoria" => $id_categoria, "nombre_categoria" => $nombre_categoria ,"id_tienda" => '');
        $_SESSION["listado_categoria"] = serialize($array);
        $this->data['categorias'] = $array;
        $this->renderc("admin/tiendas/items_categorias", $this->data, true);
    }

    function deleteitem() {
        if (isset($_SESSION["listado_categoria"])) {
            $array = unserialize($_SESSION["listado_categoria"]);
        }

        $ext = array_splice($array, $_POST['index'] - 1, 1);
        $itemsBorrar = array();
        if (isset($_SESSION["listado_categoria_del"])) {
            $itemsBorrar = unserialize($_SESSION["listado_categoria_del"]);
            $itemsBorrar[] = $ext[0];
        } else {
            $itemsBorrar[] = $ext[0];
        }

        $_SESSION["listado_categoria_del"] = serialize($itemsBorrar);
        $_SESSION["listado_categoria"] = serialize($array);
        $this->data['categorias'] = $array;
        $this->renderc("admin/tiendas/items_categorias", $this->data, true);
    }

    public function clean() {
        if (isset($_SESSION["listado_categoria"])) {
            $_SESSION["listado_categoria"] = null;
        }
        if (isset($_SESSION["listado_categoria_del"])) {
            $_SESSION["listado_categoria_del"] = null;
        }
    }

    /* Validamos que las ubicaciones con precion no se repitan */

    public function validarFechapago() {
        $idfechapago = filter_input(INPUT_POST, "idfechapago");
        $idlocalidad = filter_input(INPUT_POST, "idlocalidad");
        $r = "OK";
        if (isset($_SESSION["list_localidad"])) {
            $array = unserialize($_SESSION["list_localidad"]);
            foreach ($array as $a) {
                if (($a['idfec_pago'] == $idfechapago) && ($a["id_localidad"] == $idlocalidad)) {
                    $r = "NO";
                }
            }
        }
        echo $r;
    }

}

?>
