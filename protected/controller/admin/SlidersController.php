<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class SlidersController extends DooController {

      public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
                if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["42"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }

    public function index() {
        $sliders = Doo::db()->find("Sliders",array("where"=>"estado=1","asc"=>"titulo"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'sliders/list.php';
        $this->data['slider'] = $sliders;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Sliders");
        $sliders = new Sliders();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['slider'] = $sliders;
        $this->data['content'] = 'sliders/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
       Doo::loadModel("Sliders");
       $sliders = new Sliders($_POST);
       $sliders->estado="1";
       if ($sliders->id == "") {
           $sliders->id = Null;
       }
       if ($sliders->id == Null) {
           Doo::db()->Insert($sliders);
       } else {
           Doo::db()->Update($sliders);
       }
       return Doo::conf()->APP_URL . "admin/sliders";
   }

    public function edit() {
        $id = $this->params["pindex"];
        $sliders = Doo::db()->find("Sliders", array('where' => 'id = ?', 'limit' => 1,'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['slider'] = $sliders;
        $this->data['content'] = 'sliders/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE sliders SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/sliders";
    }

    public function validar() {
        $titulo = $_POST["titulo"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from sliders where titulo = '$titulo' AND id <> '$id'")->rowCount();
        if ($count > 0)
            echo true;
        else
            echo false;
    }
}
?>
