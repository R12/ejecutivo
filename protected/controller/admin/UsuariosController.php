<?php

/**
 * Description of UsuariosController
 *
 * @author Manuel Cuevas.
 */
class UsuariosController extends DooController {

         public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }

        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["22"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }

    public function index() {
        Doo::loadHelper('DooPager');
        if (!isset($_POST["filtro"])) {
            if (!isset($this->params['filter'])) {
                $filtro = "u.nombre";
            } else {
                $filtro = $this->params['filter'];
            }
        }else{
            $filtro = $_POST["filtro"];
        }

        if (!isset($_POST["texto"])) {
            if (!isset($this->params['texto'])) {
                $texto = "";
            } else {
                $texto = $this->params['texto'];
            }
        }else{
            $texto = $_POST["texto"];
        }

        $where = "u.estado=1 AND $filtro like '$texto%'";
        $rs    = Doo::db()->query("SELECT count(*) as total FROM usuarios u INNER JOIN roles r ON(u.role=r.id) where ".$where);
         $r = $rs->fetchAll();
         $total=$r[0]["total"];
         if ($total == 0){
           $total = 1;
           }
        $pager = new DooPager(Doo::conf()->APP_URL."usuarios/page/$filtro/$texto", $total, 10, 5);
        if(isset($this->params['number']))
            $pager->paginate(intval($this->params['number']));
        else
        $pager->paginate(1);
        $sql="SELECT u.id,usuario,nombre,r.descripcion AS rol FROM usuarios u INNER JOIN roles r ON(u.role=r.id) where ".$where;
        $usuarios = Doo::db()->query($sql." ORDER BY u.id desc LIMIT $pager->limit");
        $this->data['usuarios'] = $usuarios;
        $this->data['pager'] = $pager->output;
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'usuarios/usuarios.php';
        $this->data['filtro'] = $filtro;
        $this->data['texto'] = $texto;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Usuarios");
        $usuario = new Usuarios();
        $roles = Doo::db()->find("Roles",array("select"=>"id,role","desc"=>"id"));
        $this->data['roles'] = $roles;
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['usuario'] = $usuario;
        $this->data['content'] = 'usuarios/frm_usuarios.php';
        $this->renderc('admin/index', $this->data);
    }

    public function edit() {
        $id = $this->params["pindex"];
        $usuario = Doo::db()->find("Usuarios", array("select"=>"id,usuario,nombre,role",'where' => 'id = ?', 'limit' => 1,'param' => array($id)));
        $roles = Doo::db()->find("Roles",array("select"=>"id,role","desc"=>"id"));
        $this->data['roles'] = $roles;
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['usuario'] = $usuario;
        $this->data['content'] = 'usuarios/frm_usuarios.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
        Doo::loadModel("Usuarios");
        $usuario = new Usuarios($_POST);
        if ($usuario->id == "") {
            $usuario->id = Null;
        }
        if ($usuario->id == Null) {
            Doo::db()->query("INSERT INTO usuarios (usuario,id_cliente,identificacion,nombre,password,role,estado) VALUES ('$usuario->usuario','0','$usuario->identificacion','$usuario->nombre',MD5('$usuario->password'),'$usuario->role','1')");
        } else {
            Doo::db()->query("UPDATE usuarios SET usuario='$usuario->usuario',identificacion='$usuario->identificacion',nombre='$usuario->nombre',role='$usuario->role' where id='$usuario->id'");
        }
        return Doo::conf()->APP_URL . "admin/usuarios";
    }
    public  function generarCodigo($longitud) {
                        $key = '';
                        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
                        $max = strlen($pattern) - 1;
                        for ($i = 0; $i < $longitud; $i++)
                            $key .= $pattern{mt_rand(0, $max)};
                        return $key;
                    }
                    
    public function recuperar() {
        $email = $_POST["email"];
        $newpas = $this->generarCodigo(8); 
        $rs = Doo::db()->query("UPDATE usuarios SET PASSWORD = MD5('$newpas') WHERE usuario='$email'");
    }
    



    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE usuarios SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/usuarios";
    }

         public function validar() {
        $usuario = $_POST["usuario"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from usuarios where usuario = '$usuario' and id <> '$id'")->rowCount();
        if ($count > 0)
            echo true;
        else
            echo false;
    }
}

?>
