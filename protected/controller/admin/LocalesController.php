<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class LocalesController extends DooController {

    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["16"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

    public function index() {
        $locales = Doo::db()->find("Locales", array("where" => "estado!=0", "asc" => "nombre"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'locales/list.php';
        $this->data['locales'] = $locales;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Locales");
        $locales = new Locales();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['locales'] = $locales;
        $this->data['content'] = 'locales/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
        Doo::loadModel("Locales");
        $locales = new Locales($_POST);
        $locales->estado = "1";
        if ($locales->id == "") {
            $locales->id = Null;
        }
        if ($locales->id == Null) {
            Doo::db()->Insert($locales);
        } else {
            Doo::db()->Update($locales);
        }
        return Doo::conf()->APP_URL . "admin/locales";
    }

    public function edit() {
        $id = $this->params["pindex"];
        $locales = Doo::db()->find("Locales", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['locales'] = $locales;
        $this->data['content'] = 'locales/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE locales SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/locales";
    }

    function import() {
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'locales/import.php';
        $this->renderc('admin/index', $this->data);
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from locales where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }

    public function upload() {
        try {
            $nombre_archivo = $_FILES["file"]["name"];  //Nombre del archivo
            $tamano_archivo = $_FILES["file"]["size"];  //Tamano de archivo
            $archivo = $_FILES["file"]["tmp_name"];
            if ($nombre_archivo != "") {
                set_time_limit(0);
                $lines = file($archivo);
                $i = 0;
                Doo::db()->query("DELETE FROM locales");
                while (isset($lines[$i])) {
                    $datos = explode("\t", $lines[$i]);
                    $sql = "INSERT INTO `locales` (nombre,almacen) VALUES ('$datos[0]','$datos[1]')";

                    $i++;
                    Doo::db()->query($sql);
                }
                return Doo::conf()->APP_URL . "admin/locales";
            } else {
                die("no se especifico archivo");
            }
        } catch (SQLException $e) {
            echo $e;
        }
    }

}

?>
