<?php

/**
 * Description of EventosController
 *
 * @author WEB
 */
class EventosController extends DooController {

    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["21"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

    public function index() {
        $evento = Doo::db()->query("SELECT * FROM eventos WHERE estado = 1 ");
        //print_r($evento);
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'eventos/list.php';
        $this->data['evento'] = $evento;
        $this->clean();
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Eventos");
        $evento = new Eventos();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['evento'] = $evento;
        $this->data['content'] = 'eventos/form.php';
        $this->renderc('admin/index', $this->data);
    }




    public function save() {
        Doo::loadModel("Eventos");
        Doo::loadHelper('DooGdImage');
        $evento = new Eventos($_POST);
        try {
            $imagen = "";
            if ($evento->id == "") {
                $evento->id = Null;
                $evento->nombre = filter_input(INPUT_POST, "nombre");
                $evento->descripcion = filter_input(INPUT_POST, "descripcion");
                $evento->fecha = filter_input(INPUT_POST, "fecha");
                $evento->estado = '1';
                /* Insertando la Imagen Para el Banner */
                if ($_FILES["imagen"]["name"] != "") {
                $gd = new DooGdImage(Doo::conf()->IMG_EV . 'img_principal/');
                $type = $gd->getUploadFormat('imagen');
                if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                    $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                } else {
                    $imagen = "";
                    throw new Exception('Formato de la Imagen no Valido!');
                }
                }
               $evento->foto = $imagen;
               Doo::db()->insert($evento);
                
            } else {
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_EV . 'img_principal/');
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }

                $include1 = "";
                if ($imagen != "") {
                    $include1 = ", foto='$imagen'";
                }
                $sql = "UPDATE eventos  SET nombre = '$evento->nombre', descripcion = '$evento->descripcion', fecha = '$evento->fecha',estado = 1 $include1   WHERE id = $evento->id";
                Doo::db()->query($sql);
            }

           
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->clean();
        return Doo::conf()->APP_URL . "admin/eventos";
    }



    public function activate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE eventos SET estado=3 WHERE id=?", array($id));
        $this->avisoEventoCliente($id);
        return Doo::conf()->APP_URL . "admin/eventos";
    }

    public function edit() {
        $id = $this->params["pindex"];
        $evento = Doo::db()->find("Eventos", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['evento'] = $evento;
        $this->data['content'] = 'eventos/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE eventos SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/eventos";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from eventos where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }


}

?>
