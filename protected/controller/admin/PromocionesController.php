<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class PromocionesController extends DooController {

      public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
                if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["17"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }

    public function index() {
        $promocion = Doo::db()->query("SELECT p.id,p.nombre,p.descripcion,t.nombre AS tienda FROM promociones p 
        INNER JOIN tiendas t
        ON (p.id_tienda=t.id)
        WHERE p.estado=1");
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'promociones/list.php';
        $this->data['promocion'] = $promocion;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Promociones");
        $promocion = new Promociones();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['promocion'] = $promocion;
        $this->data['tienda'] = Doo::db()->query("SELECT id,nombre FROM tiendas WHERE estado=1 ORDER BY nombre")->fetchAll();
        $this->data['content'] = 'promociones/form.php';
        $this->renderc('admin/index', $this->data);
    }

     public function save() {
        Doo::loadModel("Promociones");
        Doo::loadHelper('DooGdImage');
        $promocion = new Promociones($_POST);
        try {
            $imagen = "";
            if ($promocion->id == "") {
                $promocion->id = Null;
                $promocion->nombre = filter_input(INPUT_POST, "nombre");
                $promocion->descripcion = filter_input(INPUT_POST, "descripcion");
                $promocion->id_tienda = filter_input(INPUT_POST, "id_tienda");
                $promocion->fecha_inicio = filter_input(INPUT_POST, "fecha_inicio");
                $promocion->fecha_fin = filter_input(INPUT_POST, "fecha_fin");
                $promocion->estado = '1';
                /* Insertando la Imagen Para el Banner */
                if ($_FILES["imagen"]["name"] != "") {
                $gd = new DooGdImage(Doo::conf()->IMG_PROMOCION);
                $type = $gd->getUploadFormat('imagen');
                if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                    $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                } else {
                    $imagen = "";
                    throw new Exception('Formato de la Imagen no Valido!');
                }
                }
               $promocion->foto = $imagen;
               Doo::db()->insert($promocion);
                
            } else {
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_PROMOCION);
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }

                $include1 = "";
                if ($imagen != "") {
                    $include1 = ", foto='$imagen'";
                }
                $sql = "UPDATE promociones  SET nombre = '$promocion->nombre', descripcion = '$promocion->descripcion', "
                        . "id_tienda = '$promocion->id_tienda',fecha_inicio='$promocion->fecha_inicio',"
                        . "fecha_fin='$promocion->fecha_fin',estado = 1 $include1   WHERE id = $promocion->id";
                Doo::db()->query($sql);
            }

           
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->clean();
        return Doo::conf()->APP_URL . "admin/promociones";
    }

    public function edit() {
        $id = $this->params["pindex"];
        $promocion = Doo::db()->find("Promociones", array('where' => 'id = ?', 'limit' => 1,'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['promocion'] = $promocion;
        $this->data['tienda'] = Doo::db()->query("SELECT id,nombre FROM tiendas WHERE estado=1 ORDER BY nombre")->fetchAll();
        $this->data['content'] = 'promociones/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE promociones SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/promociones";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from promociones where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }
}
?>
