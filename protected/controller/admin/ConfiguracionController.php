<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class ConfiguracionController extends DooController {

      public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
                if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["31"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }

    public function index() {
        $configuracion = Doo::db()->find("Configuracion",array("where"=>"estado=1","asc"=>"nombre_empresa"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'configuracion/list.php';
        $this->data['configuracion'] = $configuracion;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Configuracion");
        $configuracion = new Configuracion();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['configuracion'] = $configuracion;
        $this->data['content'] = 'configuracion/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
       Doo::loadModel("Configuracion");
       $configuracion = new Configuracion($_POST);
       $configuracion->estado="1";
       if ($configuracion->id == "") {
           $configuracion->id = Null;
       }
       if ($configuracion->id == Null) {
           Doo::db()->Insert($configuracion);
       } else {
           Doo::db()->Update($configuracion);
       }
       return Doo::conf()->APP_URL . "admin/configuracion";
   }

    public function edit() {
        $id = $this->params["pindex"];
        $configuracion = Doo::db()->find("Configuracion", array('where' => 'id = ?', 'limit' => 1,'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['configuracion'] = $configuracion;
        $this->data['content'] = 'configuracion/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE configuracion SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/configuracion";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from configuracion where nombre_empresa = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }
}
?>
