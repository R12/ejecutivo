<?php
Doo::loadController('admin/AdminController');
class ParametrosController extends AdminController {

    public $data;
 
    public function index(){
       $this->data['rootUrl'] = Doo::conf()->APP_URL;
       $this->data['content'] = "parametros/form.php";
       $param = $this->db()->find("Parametros", array("limit" => 1));
       $this->data["email"] = $param->email;
       $this->data["id"] = $param->id;
        
       $this->renderc('admin/index', $this->data, true);
    }

    public function save(){

        Doo::loadHelper('DooValidator');
        Doo::loadModel("Parametros");

        $p = new Parametros($_POST);
        $validator = new DooValidator;
        $validator->checkMode = DooValidator:: CHECK_ALL_ONE;
       
        $new = false;

        if ($_POST['id'] == "") {
            $p->id = Null;
            $new = true;
        }

        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        
        if ($error = $validator->validate($_POST, 'parametros_rules')) {
            $this->data['errors'] = $error;
            $this->data['content'] = "parametros/form.php";
            $this->data["email"] = $p->email;
            $this->data["id"]    = $p->id;
            $this->view()->renderc('admin/index', $this->data);
        } else {

            if ($new)
                Doo::db()->insert($p);
            else
                Doo::db()->update($p);

            return Doo::conf()->APP_URL . "panel/home";
        }


    }

}

?>
