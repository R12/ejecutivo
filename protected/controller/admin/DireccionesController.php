<?php
 
class DireccionesController extends DooController {

   public $data;
   
      public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
                if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["13"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }
   
    public function index(){
        Doo::loadHelper('DooPager');
        
          if (!isset($_POST["filtro"])) {
            if (!isset($this->params['filter'])) {
                $filtro = "etiqueta";
            } else {
                $filtro = $this->params['filter'];
            }
        }else{
            $filtro = $_POST["filtro"];
        }
        
        if (!isset($_POST["texto"])) {
            if (!isset($this->params['texto'])) {
                $texto = "";
            } else {
                $texto = $this->params['texto'];
            }
        }else{
            $texto = $_POST["texto"];
        }
        
        $where = "$filtro like '$texto%'"; 
         $rs   = Doo::db()->find("Direcciones", array("select"=>"COUNT(id) AS total", "where" => $where, "limit"=>1));
         $total = $rs->total;
         if ($total == 0)
           $total = 1;
        $pager = new DooPager(Doo::conf()->APP_URL."admin/direcciones/page/$filtro/$texto", $total, 10, 5);
        if(isset($this->params['number']))
            $pager->paginate(intval($this->params['number']));
        else
            $pager->paginate(1);
        $this->data['banners'] = Doo::db()->find("Direcciones", array("where" => $where, "limit"=>$pager->limit));
        $this->data['pager'] = $pager->output;
        $this->data['filtro'] = $filtro;
        $this->data['texto']  = $texto;
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'direcciones/list.php';
        $this->renderc('admin/index', $this->data);

    }

    public function add(){
        Doo::loadModel("Direcciones");
        $dirc = new Direcciones();
        $this->data['rootUrl']   = Doo::conf()->APP_URL;
        $this->data['direccion'] = $dirc;
        $this->data['opciones'] = Doo::db()->query("SELECT id,etiqueta FROM direcciones WHERE submenu='S' ORDER BY etiqueta")->fetchAll();
        $this->data['content'] = 'direcciones/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function edit(){

        Doo::loadModel("Direcciones");
        $dirc = new Direcciones();
        $dirc->id = $_REQUEST['item'];
        $this->data['rootUrl']   = Doo::conf()->APP_URL;
        $this->data['opciones'] = Doo::db()->query("SELECT id,etiqueta FROM direcciones WHERE submenu='S' ORDER BY etiqueta")->fetchAll();
        $this->data['direccion'] = Doo::db()->find($dirc, array('limit' => 1));
        $this->data['content'] = 'direcciones/form.php';
        $this->renderc('admin/index', $this->data); 
     }
    
      public function save(){
        Doo::loadModel("Direcciones");
        $dirc = new Direcciones($_POST);
        
        if(isset($_POST['istop'])){
            $dirc->istop='S';
        }else{
            $dirc->istop='N';
        }
        if(isset($_POST['isfotter'])){
            $dirc->isfotter='S';
        }else{
            $dirc->isfotter='N';
        }

        if(isset($_POST['iscolunm'])){
            $dirc->iscolunm='S';
        }else{
            $dirc->iscolunm='N';
        }
        if(isset($_POST['submenu'])){
            $dirc->submenu='S';
        }else{
            $dirc->submenu='N';
        }
        if($dirc->id == ""){
            $dirc->id = NULL;    
        }
        if ($dirc->id == NULL) {
          Doo::db()->insert($dirc);
        }else {
           Doo::db()->update($dirc);
        }
        return Doo::conf()->APP_URL."admin/direcciones";
    }
    
     
    public function delete(){
        $id = $_REQUEST['item'];
        Doo::loadModel("Direcciones");
        $dirc = new Direcciones();
        $dirc->id = $id;
        Doo::db()->delete($dirc);
        return Doo::conf()->APP_URL."admin/direcciones";
    }
    
      public function validar() {
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from direcciones where id='$id'")->rowCount();        
        if ($count > 0)
            echo true;
        else
            echo false;
    }    
}
?>
