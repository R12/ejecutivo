<?php

/**
 * Description of EventosController
 *
 * @author WEB
 */
class ServiciosController extends DooController {

    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["19"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

    public function index() {
        $servicio = Doo::db()->query("SELECT * FROM servicios WHERE estado = 1 ");
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'servicios/list.php';
        $this->data['servicio'] = $servicio;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Servicios");
        $servicio = new Servicios();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['servicio'] = $servicio;
        $this->data['content'] = 'servicios/form.php';
        $this->renderc('admin/index', $this->data);
    }




    public function save() {
        Doo::loadModel("Servicios");
        Doo::loadHelper('DooGdImage');
        $servicio = new Servicios($_POST);
        try {
            $imagen = "";
            if ($servicio->id == "") {
                $servicio->id = Null;
                $servicio->nombre = filter_input(INPUT_POST, "nombre");
                $servicio->descripcion = filter_input(INPUT_POST, "descripcion");
                $servicio->estado = '1';
                /* Insertando la Imagen Para el Banner */
                if ($_FILES["imagen"]["name"] != "") {
                $gd = new DooGdImage(Doo::conf()->IMG_SV);
                $type = $gd->getUploadFormat('imagen');
                if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                    $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                } else {
                    $imagen = "";
                    throw new Exception('Formato de la Imagen no Valido!');
                }
                }
               $servicio->imagen = $imagen;
               Doo::db()->insert($servicio);
                
            } else {
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_SV);
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }

                $include1 = "";
                if ($imagen != "") {
                    $include1 = ", imagen='$imagen'";
                }
                $sql = "UPDATE servicios  SET nombre = '$servicio->nombre', descripcion = '$servicio->descripcion',estado = 1 $include1   WHERE id = $servicio->id";
                Doo::db()->query($sql);
            }

           
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->clean();
        return Doo::conf()->APP_URL . "admin/servicios";
    }


    public function edit() {
        $id = $this->params["pindex"];
        $servicio = Doo::db()->find("Servicios", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['servicio'] = $servicio;
        $this->data['content'] = 'servicios/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE servicios SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/servicios";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from servicios where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }


}

?>
