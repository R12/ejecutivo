<?php

class AdminController extends DooController {

    public $data;
    private $permisos;
    private $accesos;

    public function home() {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL . 'admin';
        } else {
           

            $this->data['rootUrl'] = Doo::conf()->APP_URL;
            $this->data['content'] = 'home.php';
            $this->renderc('admin/index', $this->data);
        }
    }

    public function clients_home() {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL . 'clientes';
        } else {
            $this->data['rootUrl'] = Doo::conf()->APP_URL;
            $this->data['content'] = 'home.php';
            $this->data["title"] = "Inicio de Sesi&oacute;n";
            $this->renderc('admin/index', $this->data);
        }
    }

    public function index() {
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->view()->renderc('admin/login', $this->data);
    }

    public function inicio_sision() {
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data["title"] = "Inicio de Sesi&oacute;n";
        $this->view()->renderc('admin/login', $this->data);
    }

    public function login() {
        if (isset($_POST['email']) && isset($_POST['password'])) {
            if (!empty($_POST['email']) && !empty($_POST['password'])) {
                $user = trim($_POST['email']);
                $pass = md5(trim($_POST['password']));
                $u = $this->db()->find("Usuarios", array("where" => "usuario = ? and password = ?", "limit" => 1, "param" => array($user, $pass)));
                $this->data['rootUrl'] = Doo::conf()->APP_URL;
                if ($u == Null) { // o $u == false
                    $this->data['error'] = "Acceso denegado";
                    $this->renderc('admin/login', $this->data);
                } else {
                    unset($_SESSION['login']);
                    $this->buildMenu($u->role);
                    $login = new stdclass();
                    $login->usuario = $u->usuario;
                    $login->nombre = $u->nombre;
                    $login->id = $u->id;
                    $login->tipo = $u->tipo;
                    $login->menu = $this->data["htmlmenu"];
                    $login->toolbar = $this->data["toolbar"];
                    $_SESSION['login'] = $login;
                    $_SESSION['permisos'] = $this->permisos;
                    $_SESSION['accesos'] = $this->accesos;
                    return Doo::conf()->APP_URL . "panel/home";
                }
            } else {
                return Doo::conf()->APP_URL . 'admin';
            }
        } else {
            return Doo::conf()->APP_URL . 'admin';
        }
    }

    public function loginclient() {
        if (isset($_POST['email']) && isset($_POST['password'])) {
            if (!empty($_POST['email']) && !empty($_POST['password'])) {
                $user = trim($_POST['email']);
                $pass = md5(trim($_POST['password']));
                $u = $this->db()->find("Usuarios", array("where" => "usuario = ? and password = ?", "limit" => 1, "param" => array($user, $pass)));
                $this->data['rootUrl'] = Doo::conf()->APP_URL;
                if ($u == Null) { // o $u == false
                    $this->data['error'] = "Acceso denegado";
                    $this->data["title"] = "Inicio de Sesi&oacute;n";
                    $this->renderc('admin/login', $this->data);
                } else {
                  unset($_SESSION['login']);
                  $this->buildMenu($u->role);
                  $login = new stdclass();
                  $login->usuario = $u->usuario;
                  $login->nombre = $u->nombre;
                  $login->id = $u->id;
                  $login->id_cliente = $u->id_cliente;
                  $login->tipo = $u->tipo;
                  $login->menu = $this->data["htmlmenu"];
                  $login->toolbar = $this->data["toolbar"];
                  $_SESSION['login'] = $login;
                  $_SESSION['permisos'] = $this->permisos;
                  $_SESSION['accesos'] = $this->accesos;
                  return Doo::conf()->APP_URL . "panel/home";
                }
            } else {
                return Doo::conf()->APP_URL . 'admin';
            }
        } else {
            return Doo::conf()->APP_URL . 'admin';
        }
    }

    public function logout() {
        unset($_SESSION['usuario']);
        session_destroy();
        return Doo::conf()->APP_URL;
    }

    public function logoutclient() {
        unset($_SESSION['usuario']);
        session_destroy();
        return Doo::conf()->APP_URL . "clientes";
    }

    private function buildMenu($role) {

        $this->data["role"] = $role;

        $sql = "select o.codigo,o.codigo, o.menuitem, o.depende, o.submenu, o.url, r.opcion, o.toolbar,r.acceso
                from opciones o
                inner join roles_opciones r on (o.codigo = r.opcion and r.role_id = '$role')
                where depende = '' ORDER BY orden";
        //and estado = 1

        $rs = Doo::db()->query($sql);
        $parentMenu = $rs->fetchAll();

        $this->data["toolbar"] = "";
        $this->data["permisos"] = array();
        $this->data["accesos"] = array();

        $this->data["htmlmenu"] = '<ul class="sidebar-menu">';
        $this->buildChildMenu($parentMenu);
        $this->data["htmlmenu"].= '</ul>';
        //$this->data["htmlmenu"].= '<br class="clear" />';
    }

    private function buildChildMenu($parentMenu) {

        $role = $this->data["role"];

        foreach ($parentMenu as $row):

            $submenu = $row["submenu"];
            $depende = $row["depende"];
            $codigo = $row["codigo"];
            $opcion = $row["opcion"];
            $toolbar = $row["toolbar"];

            if (strlen($opcion) == Null) {
                $a = 0;
                $access = "N";
            } else {
                $a = 1;
                $access = $row["acceso"];
            }
            $this->permisos[$row["codigo"]] = $a;
            $this->accesos[$row["codigo"]] = $access;

            if ($submenu == 'S') {

                $this->data["htmlmenu"].= '<li class="treeview">';
                $this->data["htmlmenu"].= '<a href="#"><i class="' . $toolbar . '"></i> <span>' . ($row["menuitem"]) . '</span> <i class="fa fa-angle-left pull-right"></i></a>';

                $sql = "select o.codigo,o.codigo, o.menuitem, o.depende, o.submenu, o.url, r.opcion,o.toolbar,r.acceso
                        from opciones o
                        inner join roles_opciones r on (o.codigo = r.opcion and r.role_id = '$role')
                        where depende = '$codigo'  ORDER BY orden";
                //and estado = 1

                $rs = Doo::db()->query($sql);
                $childMenu = $rs->fetchAll();

                $this->data["htmlmenu"].= '<ul class="treeview-menu">';
                $this->buildChildMenu($childMenu);
                $this->data["htmlmenu"].= '</ul>';
            } else {

                if (strlen($opcion) == Null) {
                    $this->data["htmlmenu"].= '<li class="treeview">';
                    $this->data["htmlmenu"].= '<a class="disabled" href="javascript:void(0);"><i class="fa fa-pie-chart"></i><span>' . $row["menuitem"] . '</span><i class="fa fa-angle-left pull-right"></i></a>';
                } else {
                    $this->data["htmlmenu"].= '<li class="treeview">';
                    $this->data["htmlmenu"].= '<a href="' . Doo::conf()->APP_URL . $row["url"] . '">' . ($row["menuitem"]) . '</a>';
                }

                if ($toolbar != "" & strlen($opcion) != Null) {
                    $this->data["toolbar"].='<div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="' . $row["toolbar"] . '"></i></span>
                    <div class="info-box-content">
                     <span class="info-box-text"><a href="' . Doo::conf()->APP_URL . $row["url"] . '">' . $row["menuitem"] . '</a></span>

                    </div>
                    </div>
                    </div>';
                }
            }
            $this->data["htmlmenu"].= '</li>';

        endforeach;
    }

}

?>
