<?php

/**
 * Description of ClientesController
 *
 * @author WEB
 */
class ClientesController extends DooController {
    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["12"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

    public function index() {
        if (isset($_GET["pendientes"])) {
            $estado = "=1";
        } else if (isset($_GET["activos"])) {
            $estado = "=2";
        } else {
            $estado = " <> 0";
        }
        $cliente = Doo::db()->find("Clientes", array("where" => "estado $estado", "asc" => "nombre"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'clientes/list.php';
        $this->data['cliente'] = $cliente;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Clientes");
        $cliente = new Clientes();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['cliente'] = $cliente;
        $this->data['content'] = 'clientes/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
        Doo::loadModel("Clientes");
        $cliente = new Clientes($_POST);
        $cliente->estado = "1";
        if ($cliente->id == "") {
            $cliente->id = Null;
        }
        if ($cliente->id == Null) {
            Doo::db()->Insert($cliente);
        } else {
            Doo::db()->Update($cliente);
        }
        return Doo::conf()->APP_URL . "admin/clientes";
    }

    public function edit() {
        $id = $this->params["pindex"];
        $cliente = Doo::db()->find("Clientes", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['cliente'] = $cliente;
        $this->data['content'] = 'clientes/form.php';
        $this->renderc('admin/index', $this->data);
    }
    
    public function updatePerfil() {
        $id = $this->params["pindex"];
        $cliente = Doo::db()->find("Clientes", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['cliente'] = $cliente;
        $this->data['content'] = 'usuarios/formcliente.php';
        $this->renderc('admin/index', $this->data);
    }
    
    public function SaveupdatePerfil() {
        Doo::loadModel("Clientes");
        $cliente = new Clientes($_POST);
        $cliente->estado = "1";
        if ($cliente->id == "") {
            $cliente->id = Null;
        }
        if ($cliente->id == Null) {
            Doo::db()->Insert($cliente);
        } else {
            Doo::db()->Update($cliente);
        }
        return Doo::conf()->APP_URL . "panel/home";
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE clientes SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/clientes";
    }

    public function activate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE clientes SET estado=2 WHERE id=?", array($id));

        $r = Doo::db()->query("select id,nombre,nit,email from clientes where id = $id ")->fetchAll();

        $nombre = $r[0]['nombre'];
        $email = $r[0]['email'];
        $nit = $r[0]['nit'];

        $msj = "<html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>Vive tu ticket</title>

    <style type='text/css'>
        .ReadMsgBody {width: 100%; background-color: #ffffff;}
        .ExternalClass {width: 100%; background-color: #ffffff;}
        body     {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Arial, Helvetica, sans-serif}
        table {border-collapse: collapse;}

        @media only screen and (max-width: 640px)  {
                        body[yahoo] .deviceWidth {width:440px!important; padding:0;}
                        body[yahoo] .center {text-align: center!important;}
                }

        @media only screen and (max-width: 479px) {
                        body[yahoo] .deviceWidth {width:280px!important; padding:0;}
                        body[yahoo] .center {text-align: center!important;}
                }
    </style>
    </head>
    <body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' yahoo='fix' style='font-family: Arial, Helvetica, sans-serif'>
    <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
        <tr>
            <td width='100%' valign='top' bgcolor='#ffffff' style='padding-top:20px'>
            <table width='700' bgcolor='#fff' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
                <tr bgcolor='#f7f7f7'>
                    <td style='padding: 6px 0px 0px'>
                        <table width='650' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
                            <tr>
                                <td width='100%' >
                                    <table  border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth'>
                                        <tr>
                                            <td class='center' style='padding: 20px 0px 10px 0px'>
                                                <a href='#'><img src='http://vivetuticket.com/global/img/logot.png'></a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table  border='0' cellpadding='0' cellspacing='0' align='right' class='deviceWidth'>
                                        <tr>
                                            <td  class='center' style='font-size: 13px; color: #272727; font-weight: light; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 0px 10px 0px;'>
                                                <a href='#' style='text-decoration: none; color: #3b3b3b;'>T&eacute;rminos y Condiciones</a>
                                                &nbsp; &nbsp;
                                                <a href='#' style='text-decoration: none; color: #3b3b3b;'>Guia R&aacute;pida</a>
                                                &nbsp; &nbsp;
                                                <a href='http://webandnet.us/ticket/' style='text-decoration: none; color: #3b3b3b;'>Soporte</a>
                                                &nbsp; &nbsp;
                                           </td>
                                        </tr>
                                    </table><!--End nav-->
                                </td>
                            </tr>
                        </table>
                   </td>
                </tr>
            </table>
                <table width='700' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
                    <tr>
                        <td width='100%' valign='top' style='padding: 0px ' class='center'>
                            <a href='#'><img class='deviceWidth' width='700' hight='350' src='http://webandnet.us/ticket/global/img/imgbackground.jpg'></a>
                        </td>
                    </tr>
                </table>
                <table width='700' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
                    <tr>
                        <td width='100%' style=' padding: 20px 0;' align='center' bgcolor='#f7f7f7'>
                            <table   border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
                                <tr>
                                    <td valign='top' class='left' style='font-size: 16px; color: #303030; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0 20px 10px 20px;'>
                                         " . $nombre . "
                                    </td>
                                </tr>
                                <tr>
                                    <td valign='top' class='left' style='font-size: 16px; color: #303030;  text-align: center; font-family: sans-serif; line-height: 25px; vertical-align: middle; padding: 0 20px 10px 20px;'>
                                         Bienvenido a <b>Vive tu ticket</b>, el lugar para crear y compartir tus eventos, de forma gratuita.
                                    </td>
                                </tr>
                                <tr>
                                    <td valign='top' class='left' style='font-size: 20px; color: #303030; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0 20px 10px 20px;'>
                                      <br/><br/>
                                         Accede ya a tu cuenta..!
                                    </td>
                                </tr>
                                <tr>
                                    <td valign='top' class='left' style='font-size: 16px; color: #303030;  text-align: center; font-family: sans-serif; line-height: 25px; vertical-align: middle; padding: 0 20px 10px 20px;'>
                                         <b>Ingresa al link: </b> <a style='text-decoration: none'; href='http://vivetuticket.com/login'>http://vivetuticket.com/login</a>
                                    </td>
                                </tr>
                               
                                <tr>
                                  <td class='center' style='font-size: 12px; color: #303030; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 50px 0px 50px; '>
                                      *Recuerda por seguridad Cambiar la contrase&ntilde;a
                                  </td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width='700'  border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
                      <tr>
                        <td class='center' style='font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 50px 0px 50px; '>
                            Copyright © Vive tu ticket 2016
                        </td>
                    </tr>
                </table>
                <div style='height:15px'>&nbsp;</div>
            </td>
        </tr>
    </table>
    </body>
    </html>";
        Doo::loadHelper('DooMailer');
        $mail = new DooMailer();
        $mail->addTo($email);
        $mail->setSubject('[Vive Tu Ticket- Activaci&oacute;n]');
        $mail->setBodyHtml($msj);
        $mail->setFrom('vivetuticket@correo.com');
        $mail->send();

        return Doo::conf()->APP_URL . "admin/clientes";
    }

    public function validar() {
        $nombre = filter_input(INPUT_POST, "nombre");
        $id = filter_input(INPUT_POST, "id");
        $count = Doo::db()->query("select * from clientes where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }

}

?>
