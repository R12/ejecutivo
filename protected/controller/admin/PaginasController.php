<?php

/**
 * Description of PaginasController
 *
 * @author Ivgallo 
 */
//Doo::loadController('admin/AdminController');
class PaginasController extends DooController {

    public $data;

    public function beforeRun($resource, $action) {
        if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }

        if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        } else {
            if ($_SESSION["permisos"]["12"] != 1) {
                $_SESSION["msg_error"] = "No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL . "panel/home";
            }
        }
    }

    public function index() {
        $this->data['paginas'] = $this->db()->find("Pages", array("desc" => "id", "select" => 'id, page, titulo', "asArray" => true));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'paginas/list.php';
        $this->renderc('admin/index', $this->data);
    }

    public function add() {
        Doo::loadModel("Pages");
        $p = new Pages();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'paginas/form.php';
        $this->data['page'] = $p;
        $this->renderc('admin/index', $this->data);
    }

    public function edit() {
        $id = $this->params["pindex"];
        $pg = Doo::db()->find("Pages", array('where' => 'id = ?', 'limit' => 1, 'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['page'] = $pg;
        $this->data['content'] = 'paginas/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
        Doo::loadModel("Pages");
        Doo::loadHelper('DooGdImage');
        $pages = new Pages($_POST);

        try {
            $imagen = "";
            $imagen2 = "";
            if ($pages->id == "") {
                $pages->id = Null;
                $pages->page = filter_input(INPUT_POST, "page");
                $pages->titulo = filter_input(INPUT_POST, "titulo");
                $pages->descripcion = filter_input(INPUT_POST, "descripcion");
                $pages->english = "";
                $pages->titulo2 = "";

                if (isset($_POST["align"])) {
                    if ($_POST["align"] == "C") {
                        $pages->align = "center";
                    } else if ($_POST["align"] == "L") {
                        $pages->align = "left";
                    } else if ($_POST["align"] == "R") {
                        $pages->align = "right";
                    }
                } else {
                    $pages->align = "center";
                }

                /* Insertando la Imagen Para el Banner */
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_PG);
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                
                /* Insertando la Imagen Para el Background */
                if ($_FILES["imagen"]["imagen2"] != "") {
                    $gd2 = new DooGdImage(Doo::conf()->IMG_BG);
                    $type2 = $gd2->getUploadFormat('imagen');
                    if ($type2 == "jpeg" || $type2 == "jpg" || $type2 == "png") {
                        $imagen2 = $gd2->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen2 = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                $pages->background = $imagen2;
                $pages->foto = $imagen;
                Doo::db()->insert($pages);
            } else {

                if (isset($_POST["align"])) {
                    if ($_POST["align"] == "C") {
                        $pages->align = "center";
                    } else if ($_POST["align"] == "L") {
                        $pages->align = "left";
                    } else if ($_POST["align"] == "R") {
                        $pages->align = "right";
                    }
                } else {
                    $pages->align = "center";
                }

                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_PG);
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                
                if ($_FILES["imagen2"]["name"] != "") {
                    $gd2 = new DooGdImage(Doo::conf()->IMG_BG);
                    $type2 = $gd2->getUploadFormat('imagen2');
                    if ($type2 == "jpeg" || $type2 == "jpg" || $type2 == "png") {
                        $imagen2 = $gd2->uploadImage('imagen2', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen2 = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }

                $include1 = "";
                $include2 = "";
                
                if ($imagen != "") {
                    $include1 = ", foto='$imagen'";
                }
                if($imagen2 !=""){
                    $include2 = ", background='$imagen2'";
                }
                
                $sql = "UPDATE pages  SET page = '$pages->page', titulo = '$pages->titulo', align = '$pages->align', descripcion = '$pages->descripcion' $include1 $include2  WHERE id = $pages->id";
                Doo::db()->query($sql);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->clean();
        return Doo::conf()->APP_URL . "admin/paginas";
    }

    public function save23() {
        Doo::loadHelper('DooValidator');
        Doo::loadModel("Pages");
        $p = new Pages();
        $validator = new DooValidator;
        $validator->checkMode = DooValidator:: CHECK_ALL_ONE;
        $p->page = $_POST['page'];
        $p->titulo = $_POST['titulo'];
        $p->descripcion = stripcslashes($_POST['descripcion']);
        $p->titulo2 = "";
        $p->english = "";
        $id = $_POST['id'];
        if ($error = $validator->validate($_POST, 'pagina_rules')) {
            $this->data['rootUrl'] = Doo::conf()->APP_URL;
            $this->data['errors'] = $error;
            $this->data['content'] = "paginas/form.php";
            $this->data['page'] = $p;
            $this->renderc('admin/index', $this->data, true);
        } else {
            if ($id == "") {
                Doo::db()->insert($p);
            } else {
                $p->id = $id;
                Doo::db()->update($p);
            }
            return Doo::conf()->APP_URL . "admin/paginas";
        }
    }

}

?>
