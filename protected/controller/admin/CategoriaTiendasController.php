<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class CategoriaTiendasController extends DooController {

      public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
                if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["15"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }

    public function index() {
        $ctgevento = Doo::db()->find("CategoriasTiendas",array("where"=>"estado=1","asc"=>"nombre"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'categoria_tienda/list.php';
        $this->data['ctgetienda'] = $ctgevento;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("CategoriasTiendas");
        $ctgtienda = new CategoriasTiendas();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['ctgetienda'] = $ctgtienda;
        $this->data['content'] = 'categoria_tienda/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function save() {
       Doo::loadModel("CategoriasTiendas");
       $ctgetienda = new CategoriasTiendas($_POST);
       $ctgetienda->estado="1";
       if ($ctgetienda->id == "") {
           $ctgetienda->id = Null;
       }
       if ($ctgetienda->id == Null) {
           Doo::db()->Insert($ctgetienda);
       } else {
           Doo::db()->Update($ctgetienda);
       }
       return Doo::conf()->APP_URL . "admin/ctgtienda";
   }

    public function edit() {
        $id = $this->params["pindex"];
        $ctgetienda = Doo::db()->find("CategoriasTiendas", array('where' => 'id = ?', 'limit' => 1,'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['ctgetienda'] = $ctgetienda;
        $this->data['content'] = 'categoria_tienda/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE categorias_tiendas SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/ctgtienda";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from categorias_tiendas where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }
}
?>
