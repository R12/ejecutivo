<?php

/**
 * Description of RolesController
 *
 * @author WEB
 */
class BannersController extends DooController {

      public function beforeRun($resource, $action) {
       if (!isset($_SESSION['login'])) {
            return Doo::conf()->APP_URL;
        }
                if (!isset($_SESSION['permisos'])) {
            return Doo::conf()->APP_URL;
        }else{
            if($_SESSION["permisos"]["18"]!=1){
                $_SESSION["msg_error"]="No tiene Permiso para esta Opci&oacute;n";
                return Doo::conf()->APP_URL."panel/home";
            }
        }
    }

    public function index() {
        $banner = Doo::db()->find("Banners",array("where"=>"estado=1","asc"=>"id"));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['content'] = 'banner/list.php';
        $this->data['banner'] = $banner;
        $this->renderc('admin/index', $this->data, true);
    }

    public function add() {
        Doo::loadModel("Banners");
        $banner = new Banners();
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['banner'] = $banner;
        $this->data['content'] = 'banner/form.php';
        $this->renderc('admin/index', $this->data);
    }

   public function save() {
    Doo::loadModel("Banners");
    Doo::loadHelper('DooGdImage');
    $banner = new Banners($_POST);
    try {
      $imagen = "";
      if ($banner->id == "") {
          $banner->id=NULL;
          $banner->estado = '1';
                /* Insertando la Imagen Para el Banner */
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_BANNER);
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                $banner->nombreimagen = $imagen;
                Doo::db()->insert($banner);
            } else {
                if ($_FILES["imagen"]["name"] != "") {
                    $gd = new DooGdImage(Doo::conf()->IMG_BANNER);
                    $type = $gd->getUploadFormat('imagen');
                    if ($type == "jpeg" || $type == "jpg" || $type == "png") {
                        $imagen = $gd->uploadImage('imagen', 'img_' . date('Ymdhis'));
                    } else {
                        $imagen = "";
                        throw new Exception('Formato de la Imagen no Valido!');
                    }
                }
                $include1 = "";
                if ($imagen != "") {
                    $include1 = ", nombreimagen='$imagen'";
                }
                // Actualizacion de Banner
        $sql = "update banners set nombre = '$banner->nombre', descripcion = '$banner->descripcion',estado='1' $include1  where id = $banner->id";
        Doo::db()->query($sql);
      }
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
   return Doo::conf()->APP_URL . "admin/banners";
  }

    public function edit() {
        $id = $this->params["pindex"];
        $banner = Doo::db()->find("Banners", array('where' => 'id = ?', 'limit' => 1,'param' => array($id)));
        $this->data['rootUrl'] = Doo::conf()->APP_URL;
        $this->data['banner'] = $banner;
        $this->data['content'] = 'banner/form.php';
        $this->renderc('admin/index', $this->data);
    }

    public function deactivate() {
        $id = $this->params["pindex"];
        Doo::db()->query("UPDATE bannes SET estado=0 WHERE id=?", array($id));
        return Doo::conf()->APP_URL . "admin/banners";
    }

    public function validar() {
        $nombre = $_POST["nombre"];
        $id = $_POST["id"];
        $count = Doo::db()->query("select * from banners where nombre = '$nombre' AND id <> '$id'")->rowCount();
        if ($count > 0) {
            echo true;
        } else {
            echo false;
        }
    }
}
?>
